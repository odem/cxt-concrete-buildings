const buildingImages = (function(window, document, library, imgCarousel) {
  const buildingImagesArgs = {
      indicatorAttr: { image: 'data-image' },
      displayImageAttr: { first: 'data-firstShown' },
    },
    arrowSelectedLinearShift = (carousel, direction) => {
      let images = null,
        firstDisplayImage = null,
        newFirstImage = null,
        getNewImage = function() {};

      if (
        !imgCarousel.isCarouselValid(carousel) ||
        !library.isString(direction)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to linear shift images from arrow selection.'
        );
      }

      images = imgCarousel.getImages(carousel);
      direction = direction.toLowerCase();

      if (images.length && (direction === 'left' || direction === 'right')) {
        firstDisplayImage = library.getCollectionElement(images, {
          attribute: buildingImagesArgs.displayImageAttr.first,
        });

        getNewImage =
          direction === 'left'
            ? library.getNextElementSibling
            : library.getPreviousElementSibling;

        newFirstImage = getNewImage(firstDisplayImage.element);

        if (newFirstImage) {
          imgCarousel.horzLinearImageShift(carousel, newFirstImage);
        }
      }
    },
    imageSelectedLinearShift = (carousel, selectedIndicator) => {
      let imageId = '',
        newFirstImage = null;

      if (
        !imgCarousel.isCarouselValid(carousel) ||
        !imgCarousel.isIndicatorValid(selectedIndicator)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to linear shift images from image selection.'
        );
      }

      imageId = selectedIndicator.getAttribute(
        buildingImagesArgs.indicatorAttr.image
      );
      newFirstImage = document.getElementById(imageId);

      if (newFirstImage) {
        imgCarousel.horzLinearImageShift(carousel, newFirstImage);
      }
    },
    setCarousels = () => {
      const carousels = imgCarousel.getCarousels(document);
      let currCarousel = null,
        indicators = null,
        indicator = null,
        leftArrow = null,
        rightArrow = null;

      if (carousels && carousels.length) {
        for (let i = 0; i < carousels.length; i++) {
          currCarousel = carousels[i];
          indicators = imgCarousel.getIndicators(currCarousel);
          rightArrow = imgCarousel.getArrow(currCarousel, 'right');
          leftArrow = imgCarousel.getArrow(currCarousel, 'left');

          imgCarousel.setLinearCarousel(currCarousel);

          if (document.addEventListener) {
            for (let i = 0; i < indicators.length; i++) {
              indicator = indicators[i];

              indicator.addEventListener(
                'click',
                imageSelectedLinearShift.bind(null, currCarousel, indicator),
                false
              );
            }
          }

          if (document.addEventListener && rightArrow) {
            rightArrow.addEventListener(
              'click',
              arrowSelectedLinearShift.bind(null, currCarousel, 'left'),
              false
            );
          }

          if (document.addEventListener && leftArrow) {
            leftArrow.addEventListener(
              'click',
              arrowSelectedLinearShift.bind(null, currCarousel, 'right'),
              false
            );
          }

          if (window.addEventListener) {
            window.addEventListener(
              'orientationchange',
              imgCarousel.setLinearCarousel.bind(null, currCarousel),
              false
            );
            window.addEventListener(
              'resize',
              imgCarousel.setLinearCarousel.bind(null, currCarousel),
              false
            );
          }
        }
      }
    };
  return {
    setCarousels,
  };
})(window, document, library, imgCarousel);
