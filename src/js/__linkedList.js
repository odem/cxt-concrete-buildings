const linkedList = (function(library, linkedListNode) {
  const state = { head: null, length: 0 },
    methods = {
      clear: function() {
        this.head = null;
        this.length = 0;
      },
      contains: function(data) {
        return this.indexOf(data) === -1 ? false : true;
      },
      getSize: function() {
        return this.length;
      },
      indexOf: function(data) {
        let index = 0,
          currNode = this.head;

        while (currNode != null) {
          if (currNode.data === data) {
            return index;
          }

          currNode = currNode.next;
          index++;
        }

        return -1;
      },
      isEmpty: function() {
        return this.length === 0;
      },
      print: function(separator) {
        const defaultSeparator = ' -> ';

        if (separator === undefined) {
          separator = defaultSeparator;
        }

        if (!library.isString(separator)) {
          throw new TypeError(
            library.errorMsg().argsEvaluationFail + ' to print linked list.'
          );
        }

        console.log('list: ' + this.toString(separator));
      },
      toString: function(separator) {
        const defaultSeparator = ',';
        let current = this.head,
          list = '';

        if (separator === undefined) {
          separator = defaultSeparator;
        }

        if (!library.isString(separator)) {
          throw new TypeError(
            library.errorMsg().argsEvaluationFail +
              ' to convert linked list data to string.'
          );
        }

        for (let i = 0; i < this.length - 1; i++) {
          list += current.data.toString() + separator;
          current = current.next;
        }

        if (this.length) {
          list += current.data.toString();
        }

        return list;
      },
    },
    singleLinkedMethods = {
      add: function(index, data) {
        let currNode = null;

        if (!library.isInt(index)) {
          throw new TypeError(
            'index must be an integer to add node at a specific linked list spot.'
          );
        }

        if (index < 0 || index > this.length) {
          throw new RangeError('index is outside linked list range.');
        }

        this.length++;

        if (this.head === null) {
          this.head = linkedListNode.createSingleLinked(data);
        } else {
          if (index === 0) {
            this.head = linkedListNode.createSingleLinked(data, this.head);
          } else {
            currNode = this.head;

            for (let i = 0; i < index - 1; i++) {
              currNode = currNode.next;
            }

            currNode.next = linkedListNode.createSingleLinked(
              data,
              currNode.next
            );
          }
        }
      },
      remove: function(index) {
        let currNode = this.head;

        if (!library.isInt(index)) {
          throw new TypeError(
            'index must be an integer to remove node at specific spot in linked list.'
          );
        }

        if (index < 0 || index >= this.length) {
          throw new RangeError('index is outside linked list range.');
        }

        this.length--;

        if (index === 0) {
          this.head = currNode.next;
        } else {
          for (let i = 0; i < index - 1; i++) {
            currNode = currNode.next;
          }

          currNode.next = currNode.next.next;
        }
      },
      removeNode: function(data) {
        let currNode = this.head;

        if (currNode != null && currNode.data === data) {
          this.head = currNode.next;
          this.length--;
        }

        while (
          currNode != null &&
          currNode.next != null &&
          currNode.next.data != data
        ) {
          currNode = currNode.next;
        }

        if (currNode != null && currNode.next != null) {
          currNode.next = currNode.next.next;
          this.length--;
        }
      },
      set: function(index, data) {
        let currNode = this.head;

        if (!library.isInt(index)) {
          throw new TypeError(
            'index must be an integer to add data at a specific linked list spot.'
          );
        }

        if (index < 0 || index >= this.length) {
          throw new RangeError('index is outside linked list range.');
        }

        if (currNode != null) {
          for (let i = 0; i < index; i++) {
            currNode = currNode.next;
          }

          currNode.data = data;
        }
      },
    },
    doubleLinkedMethods = {
      add: function(index, data) {},
      addFirst: function(data) {},
      addLast: function(data) {},
      clear: function() {},
      contains: function(data) {},
      getSize: function() {},
      indexOf: function(data) {},
      isEmpty: function() {},
      print: function() {},
      remove: function(index) {},
      removeFirst: function() {},
      removeLast: function() {},
      removeNode: function(data) {},
      set: function(index, data) {},
    };

  return {
    create: function(type) {
      const protoMethodTypes = {
        singlelinked: singleLinkedMethods,
        doublelinked: doubleLinkedMethods,
      };
      let protoMethods = null,
        list = null;

      if (!library.isString(type)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to create linked list.'
        );
      }

      protoMethods = protoMethodTypes[type.toLowerCase()];

      if (protoMethods === undefined) {
        throw new RangeError(
          'linked list type must "singlelinked" or "doublelinked" (case-insensitive).'
        );
      }

      list = library.composeObject({}, state, methods, protoMethods);

      return {
        add: function(index, data) {
          return list.add(index, data);
        },
        addFirst: function(data) {
          return list.add(0, data);
        },
        addLast: function(data) {
          return list.add(list.length, data);
        },
        clear: function() {
          return list.clear();
        },
        contains: function(data) {
          return list.contains(data);
        },
        getSize: function() {
          return list.getSize();
        },
        indexOf: function(data) {
          return list.indexOf(data);
        },
        isEmpty: function() {
          return list.isEmpty();
        },
        print: function(separator) {
          return list.print(separator);
        },
        remove: function(index) {
          return list.remove(index);
        },
        removeFirst: function() {
          return list.remove(0);
        },
        removeLast: function() {
          return list.remove(list.length - 1);
        },
        removeNode: function(data) {
          return list.removeNode(data);
        },
        set: function(index, data) {
          return list.set(index, data);
        },
        toString: function(separator) {
          return list.toString(separator);
        },
      };
    },
  };
})(library, linkedListNode);
