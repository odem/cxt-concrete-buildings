const imgCarousel = (function(window, document, library) {
  const imgCarouselArgs = {
      class: {
        carousel: 'js-imgCarousel',
        imagesViewport: 'js-imgCarousel-imagesViewport',
        imagesContainer: 'js-imgCarousel-imagesContainer',
        image: 'js-imgCarousel-img',
        indicatorsContainer: 'js-imgCarousel-imgIndicatorContainer',
        indicator: 'js-imgCarousel-imgIndicator',
      },
      indicatorAttr: { image: 'data-image' },
      imageAttr: { id: 'id' },
      displayImageAttr: { first: 'data-firstshown', last: 'data-lastshown' },
    },
    arrowDirections = {
      left: {
        classname: 'js-imgCarousel-arrowLeft',
        dependentSibling: library.getPreviousElementSibling,
      },
      right: {
        classname: 'js-imgCarousel-arrowRight',
        dependentSibling: library.getNextElementSibling,
      },
      up: {
        classname: 'js-imgCarousel-arrowUp',
        dependentSibling: library.getPreviousElementSibling,
      },
      down: {
        classname: 'js-imgCarousel-arrowDown',
        dependentSibling: library.getNextElementSibling,
      },
    },
    getCarousels = node => {
      return library.getElements(imgCarouselArgs.class.carousel, node);
    },
    getImagesViewport = node => {
      return library.getFirstElement(
        imgCarouselArgs.class.imagesViewport,
        node
      );
    },
    getImages = node => {
      return library.getElements(imgCarouselArgs.class.image, node);
    },
    getIndicatorsContainer = node => {
      return library.getFirstElement(
        imgCarouselArgs.class.indicatorsContainer,
        node
      );
    },
    getIndicators = node => {
      return library.getElements(imgCarouselArgs.class.indicator, node);
    },
    isIndicatorValid = indicator => {
      return (
        library.isValidRadioBtn(indicator) &&
        library.elementHasReqAttrs(indicator, imgCarouselArgs.indicatorAttr)
      );
    },
    isCarouselValid = carousel => {
      return library.isElementNode(carousel);
    },
    isImageValid = image => {
      return (
        library.isElementNode(image) &&
        library.elementHasReqAttrs(image, imgCarouselArgs.imageAttr)
      );
    },
    isImagesContainerValid = container => {
      return library.isElementNode(container);
    },
    getSelectedIndicator = (indicators, value) => {
      return library.getCollectionElement(indicators, {
        attribute: imgCarouselArgs.indicatorAttr.image,
        attributeValue: value,
      });
    },
    getArrow = (node, direction) => {
      let arrowClassname = '';

      if (!library.isElementNode(node) || !library.isString(direction)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to get image carousel arrow.'
        );
      }

      if (library.objectHasOwnProperty(arrowDirections, direction)) {
        arrowClassname = arrowDirections[direction]['classname'];

        return node.getElementsByClassName(arrowClassname)[0];
      }
      return null;
    },
    getFirstImgIndex = images => {
      let numImages = 0,
        imagesShift = 0,
        image = 0,
        totalImageWidth = 0,
        firstDisplayIndex = -1;

      if (!library.isArrayLike(images)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to get first image index of image carousel.'
        );
      }

      numImages = images.length;

      if (numImages) {
        image = images[0];
        totalImageWidth = library.getElementTotalWidth(image);

        if (totalImageWidth > 0) {
          imagesShift =
            parseInt(library.getCSSProperty(image.parentElement, 'left'), 10) ||
            0;

          firstDisplayIndex++;

          while (imagesShift < 0) {
            firstDisplayIndex++;
            imagesShift += totalImageWidth;
          }
        }
      }

      return firstDisplayIndex;
    },
    getLastImgIndexByDisplay = (viewport, elements, startIndex) => {
      let numElements = 0,
        elementsShift = 0,
        element = 0,
        totalElementWidth = 0,
        maxNumDisplayElements = 0,
        lastIndex = -1,
        lastDisplayIndex = -1,
        lastPossibleDisplayIndex = -1;

      if (
        !library.isElementNode(viewport) ||
        !library.isArrayLike(elements) ||
        !library.isPositiveInt(startIndex)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to get last image index based on displayed image carousel.'
        );
      }

      numElements = elements.length;

      if (numElements && library.isIndexValid(startIndex, elements)) {
        element = elements[0];
        maxNumDisplayElements = library.getMaxNumDisplayElements(
          viewport,
          element
        );

        if (maxNumDisplayElements > 0) {
          totalElementWidth = library.getElementTotalWidth(element);
          elementsShift =
            parseInt(
              library.getCSSProperty(element.parentElement, 'left'),
              10
            ) || 0;

          while (elementsShift >= totalElementWidth) {
            maxNumDisplayElements--;
            elementsShift -= totalElementWidth;
          }

          lastIndex = numElements - 1;
          lastPossibleDisplayIndex = startIndex + maxNumDisplayElements - 1;
          lastDisplayIndex =
            lastIndex > lastPossibleDisplayIndex
              ? lastPossibleDisplayIndex
              : lastIndex;
        }
      }

      return lastDisplayIndex;
    },
    getLastImgIndexByIndex = (collection, refIndex, shiftAmount) => {
      let index = -1,
        numItems = 0,
        maxIndexValue = 0;

      if (
        !library.isArrayLike(collection) ||
        !library.isInt(refIndex) ||
        !library.isInt(shiftAmount)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to get last image index based on index of image carousel.'
        );
      }

      numItems = collection.length;
      maxIndexValue = numItems - 1;

      if (numItems && library.isIndexValid(refIndex, collection)) {
        index = refIndex + shiftAmount;
      }

      return index > maxIndexValue ? maxIndexValue : index;
    },
    getLastDisplayImage = (images, options) => {
      let index = -1,
        prevLastImage = null,
        shift = 0,
        viewport = undefined,
        startIndex = -1;

      if (!library.isArrayLike(images) || !library.isObject(options)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to get last display image of carousel.'
        );
      }

      shift = library.objectHasOwnProperty(options, 'shift')
        ? options.shift
        : undefined;
      viewport = library.objectHasOwnProperty(options, 'viewport')
        ? options.viewport
        : undefined;
      startIndex = library.objectHasOwnProperty(options, 'startIndex')
        ? options.startIndex
        : -1;

      prevLastImage = library.getCollectionElement(images, {
        attribute: imgCarouselArgs.displayImageAttr.last,
      });

      if (prevLastImage && shift != undefined) {
        index = getLastImgIndexByIndex(images, prevLastImage.index, shift);
      }

      if (viewport && library.isIndexValid(images, startIndex)) {
        index = getLastImgIndexByDisplay(viewport, images, startIndex);
      }

      return library.isIndexValid(index, images) ? images[index] : null;
    },
    setImagesContainerWidth = (container, image) => {
      const maxNumImagesShown = 5,
        widthMultiplier = 2;

      if (isImagesContainerValid(container) && isImageValid(image)) {
        container.style.width =
          image.offsetWidth * maxNumImagesShown * widthMultiplier + 'px';
      }
    },
    setFirstImageAttribute = (images, image) => {
      if (!library.isArrayLike(images) || !library.isElementNode(image)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to set first image attribute(s) of the image carousel.'
        );
      }

      library.setRadialAttribute({
        elements: images,
        targetElement: image,
        attribute: imgCarouselArgs.displayImageAttr.first,
        attributeValue: 'true',
      });
    },
    setLastImageAttribute = (images, image) => {
      if (!library.isArrayLike(images) || !library.isElementNode(image)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to set last image attribute(s) of the image carousel.'
        );
      }

      library.setRadialAttribute({
        elements: images,
        targetElement: image,
        attribute: imgCarouselArgs.displayImageAttr.last,
        attributeValue: 'true',
      });
    },
    setArrowDisplay = (element, arrow, direction) => {
      let siblingNode = null;

      if (
        !library.isElementNode(element) ||
        !library.isElementNode(arrow) ||
        !library.isString(direction)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to set arrow display of the image carousel.'
        );
      }

      direction = direction.toLowerCase();

      if (library.objectHasOwnProperty(arrowDirections, direction)) {
        siblingNode = arrowDirections[direction]['dependentSibling'](element);
        siblingNode ? library.showElement(arrow) : library.hideElement(arrow);
      }
    },
    toggleIndicators = (indicators, selectedIndicator) => {
      library.toggleRadioBtns(indicators, selectedIndicator);
    },
    shiftImages = (image, direction = 'left', shiftNumImages = 1) => {
      let leftPosition = 0,
        shiftAmount = 0,
        imageContainer = null;

      if (
        !library.isElementNode(image) ||
        !library.isString(direction) ||
        !library.isInt(shiftNumImages)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to shift the image carousel.'
        );
      }

      if (shiftNumImages < 0) {
        throw new RangeError(
          'The number of images to shift the image carousel is out of range.'
        );
      }

      imageContainer = image.parentElement;

      if (imageContainer) {
        direction = direction.toLowerCase();
        leftPosition =
          parseInt(library.getCSSProperty(imageContainer, 'left'), 10) || 0;

        if (library.objectHasOwnProperty(arrowDirections, direction)) {
          shiftAmount = library.getElementTotalWidth(image) * shiftNumImages;
        } else {
          shiftAmount = 0;
        }

        leftPosition +=
          direction === 'left' || direction === 'up'
            ? -1 * shiftAmount
            : shiftAmount;

        imageContainer.style.left = leftPosition + 'px';
      }
    },
    horzLinearImageShift = (carousel, firstDisplayImage) => {
      let viewport = null,
        images = null,
        imagesContainer = null,
        firstImageId = '',
        direction = '',
        shiftNumImages = 0,
        firstImageIndices = {},
        prevFirstImageIndex = -1,
        currFirstImageIndex = -1,
        indicators = null,
        selectedIndicator = null,
        leftArrow = null,
        rightArrow = null,
        lastDisplayImage = {},
        imageShiftDuration = 0;

      if (!isCarouselValid(carousel) || !isImageValid(firstDisplayImage)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to set horizontally shift the image carousel.'
        );
      }

      viewport = getImagesViewport(carousel);
      images = getImages(carousel);
      indicators = getIndicators(carousel);
      leftArrow = getArrow(carousel, 'left');
      rightArrow = getArrow(carousel, 'right');

      if (images.length) {
        firstImageIndices = library.getPrevAndCurrElementIndex(images, {
          prevAttribute: imgCarouselArgs.displayImageAttr.first,
          currRefElement: firstDisplayImage,
        });
      }

      if (firstImageIndices) {
        prevFirstImageIndex = firstImageIndices.prevIndex;
        currFirstImageIndex = firstImageIndices.currIndex;
      }

      if (
        library.isIndexValid(prevFirstImageIndex, images) &&
        library.isIndexValid(currFirstImageIndex, images)
      ) {
        shiftNumImages = Math.abs(prevFirstImageIndex - currFirstImageIndex);

        if (prevFirstImageIndex > currFirstImageIndex) {
          direction = 'right';
        } else if (prevFirstImageIndex < currFirstImageIndex) {
          direction = 'left';
        } else {
          direction = '';
        }
      }

      if (direction && shiftNumImages) {
        firstImageId = firstDisplayImage.getAttribute(
          imgCarouselArgs.imageAttr.id
        );
        selectedIndicator = getSelectedIndicator(indicators, firstImageId);
        lastDisplayImage = getLastDisplayImage(images, {
          viewport: viewport,
          startIndex: currFirstImageIndex,
          shift: currFirstImageIndex - prevFirstImageIndex,
        });

        library.hideElement(leftArrow);
        library.hideElement(rightArrow);
        setFirstImageAttribute(images, firstDisplayImage);
        shiftImages(firstDisplayImage, direction, shiftNumImages);
        toggleIndicators(indicators, selectedIndicator.element);
        imagesContainer = library.getFirstElement(
          imgCarouselArgs.class.imagesContainer,
          carousel
        );
        // CSS property returns a value in seconds.
        imageShiftDuration = parseFloat(
          library.getCSSProperty(imagesContainer, 'transition-duration')
        );

        // Converts value to milliseconds.
        if (imageShiftDuration) {
          imageShiftDuration *= 1000;
        }

        if (leftArrow) {
          setTimeout(
            setArrowDisplay,
            imageShiftDuration,
            firstDisplayImage,
            leftArrow,
            'left'
          );
        }

        if (rightArrow && lastDisplayImage) {
          setLastImageAttribute(images, lastDisplayImage);
          setTimeout(
            setArrowDisplay,
            imageShiftDuration,
            lastDisplayImage,
            rightArrow,
            'right'
          );
        }
      }
    },
    setLinearCarousel = carousel => {
      let indicatorsContainer = null,
        indicators = null,
        selectedIndicator = null,
        leftArrow = null,
        rightArrow = null,
        viewport = null,
        imagesContainer = null,
        images = null,
        firstImage = null,
        firstImageId = '',
        firstImageIndex = -1,
        isFirstImageValid = false,
        lastImage = null,
        lastImageIndex = -1,
        isLastImageValid = false;

      if (!isCarouselValid(carousel)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to set the linear image carousel.'
        );
      }

      images = getImages(carousel);
      imagesContainer = library.getFirstElement(
        imgCarouselArgs.class.imagesContainer,
        carousel
      );
      indicatorsContainer = getIndicatorsContainer(carousel);
      indicators = getIndicators(carousel);
      leftArrow = getArrow(carousel, 'left');
      rightArrow = getArrow(carousel, 'right');
      viewport = getImagesViewport(carousel);

      if (images.length) {
        firstImageIndex = getFirstImgIndex(images);
        setImagesContainerWidth(imagesContainer, images[0]);
      }

      if (library.isIndexValid(firstImageIndex, images)) {
        firstImage = images[firstImageIndex];
        lastImageIndex = getLastImgIndexByDisplay(
          viewport,
          images,
          firstImageIndex
        );

        if (viewport && library.isIndexValid(lastImageIndex, images)) {
          lastImage = images[lastImageIndex];
        }
      }

      isFirstImageValid = isImageValid(firstImage);
      isLastImageValid = isImageValid(lastImage);

      if (isFirstImageValid) {
        setFirstImageAttribute(images, firstImage);
      }

      if (isLastImageValid) {
        setLastImageAttribute(images, lastImage);
      }

      library.showElement(indicatorsContainer);

      if (indicators.length && isFirstImageValid) {
        firstImageId = firstImage.getAttribute(imgCarouselArgs.imageAttr.id);
        selectedIndicator = getSelectedIndicator(indicators, firstImageId);

        if (selectedIndicator) {
          toggleIndicators(indicators, selectedIndicator.element);
        }
      }

      if (leftArrow && isFirstImageValid) {
        setArrowDisplay(firstImage, leftArrow, 'left');
      }

      if (rightArrow && isLastImageValid) {
        setArrowDisplay(lastImage, rightArrow, 'right');
      }
    };
  return {
    isIndicatorValid,
    isCarouselValid,
    getCarousels,
    getImages,
    getIndicators,
    getArrow,
    horzLinearImageShift,
    setLinearCarousel,
  };
})(window, document, library);
