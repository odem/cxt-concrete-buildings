/** ======================================================================== */

/**
 * @fileoverview Manages site navigation, and setting adblock attributes.
 *
 * @author Shana Odem
 * @copyright 2019
 *
 * @param  {Object} window - browser's window containing a DOM document.
 * @param  {Object} document - DOM document containing the page contents.
 */

if (document.addEventListener) {
  document.addEventListener('DOMContentLoaded', page.init, false);
  document.addEventListener('DOMContentLoaded', navSite.makeResponsive, false);
  document.addEventListener('DOMContentLoaded', interactiveModel.load, false);
  document.addEventListener('DOMContentLoaded', media.lazyLoad, false);
  document.addEventListener('DOMContentLoaded', buildings.setSort, false);
  document.addEventListener('DOMContentLoaded', buildings.setFilter, false);
  document.addEventListener(
    'DOMContentLoaded',
    buildingImages.setCarousels,
    false
  );
  document.addEventListener(
    'DOMContentLoaded',
    installInstructions.setFilter,
    false
  );
  document.addEventListener('DOMContentLoaded', copyright.setYear, false);
}
