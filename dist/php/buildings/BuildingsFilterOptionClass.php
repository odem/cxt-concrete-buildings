<?php
  class BuildingsFilterOption {
    private $values = [];

    private function initializeValue($value) {
      if (isset($value)) {
        $value = strval($value);

        if ($value != '' && !$this->containsValue($value)) {
          $this->values[$value] = 1;
        }
      }
    }

    function __construct($value) {
      $this->initializeValue($value);
    }

    function incrementCount($value) {
      if (isset($value)) {
        $value = strval($value);

        if ($value != '' && $this->containsValue($value)) {
          $this->values[$value] += 1;
        }
      }
    }

    function addValue($value) {
      $this->initializeValue($value);
    }

    function containsValue($value) {
      if (isset($value)) {
        $value = strval($value);

        return $value != '' && array_key_exists($value, $this->values);
      }
    }

    function getValues() {
      return $this->values;
    }
  }
?>