<?php
  require_once('BuildingsFilterOptionClass.php');

  class BuildingsFilterResponse {
    private $data = [
      "buildings" => [],
      "building-type" => null,
      "building-size" => null,
      "plumbing" => null,
      "restroom-type" => null,
      "num-restroom" => null,
      "num-toilet" => null,
      "num-toilet-ada" => null,
      "num-shower" => null,
      "num-shower-ada" => null,
      "num-sink" => null
    ];

    private function isKeyValid($option) {
      return array_key_exists($option, $this->data);
    }

    function getDataArray () {
      $data = [];

      foreach($this->data as $key => $option) {
        if ($key == 'buildings') {
          $data[$key] = $option;
        }

        if (is_a($option, 'BuildingsFilterOption')) {
          $data[$key] = $option->getValues();
        }
      }

      return $data;
    }

    function update($key, $value) {
      if (is_string($value) || is_numeric($value)) {
        if ($key == 'building-name') {
          $this->data['buildings'][] = $value;
        }

        if ($key != 'building-name' && $this->isKeyValid($key)) {
          $option = $this->data[$key];
          $valueStr = strval($value);

          if (is_numeric($value)) {
            $valueStr = 'n' . $valueStr;
          }

          if (is_a($option, 'BuildingsFilterOption')) {
            $option->containsValue($valueStr)
              ? $option->incrementCount($valueStr)
              : $option->addValue($valueStr);
          } else {
            $this->data[$key] = new BuildingsFilterOption($valueStr);
          }
        }
      }
    }
  }
?>