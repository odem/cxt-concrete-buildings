const interactiveModel = (function(
  document,
  library,
  media,
  dropdown,
  smoothScroll
) {
  const configurator = {
      api: null,
      config: null,
      iframe: null,
      modelContents: null,
      modelContainer: null,
      optionBtns: null,
      optionNavBtns: null,
      options: {},
    },
    materialCategories = {
      wall: [
        'WALL_EXT_SANS',
        'Barnwood_B',
        'EXT_W_SANS',
        'EXT_W_ROCK',
        'EXT_W',
      ],
      roof: ['R_EXT_SANS', 'R_EDGE', 'R_STUCCO'],
      fixture: ['Vitreous', 'HD_White'],
    },
    materials = {
      WALL_EXT_SANS: 'wall',
      Barnwood_B: 'wall',
      EXT_W_SANS: 'wall',
      EXT_W_ROCK: 'wall',
      EXT_W: 'wall',
      R_EXT_SANS: 'roof',
      R_EDGE: 'roof',
      R_STUCCO: 'roof',
      Vitreous: 'fixture',
      HD_White: 'fixture',
    },
    modelArgs = {
      class: {
        media: 'js-media',
        mediaIframe: 'js-media-iframe',
        optionBtn: 'js-building-model-option',
        optionNavBtn: 'js-building-model-option-nav-btn',
        helpSxn: 'js-model-help-sxn',
        helpLink: 'js-model-help-link',
        colorGrid: 'js-building-model-color-grid',
        dropdownBtn: ' js-dropdown-btn',
        dropdownMenu: ' js-dropdown-menu',
        warning: 'js-model-warning',
      },
      sxnAttr: { overlay: 'data-overlay' },
      containerAttr: { modelId: 'data-modelId' },
      optionBtnAttr: { color: 'data-color' },
      optionNavBtnAttr: {
        category: 'data-category',
        controls: 'aria-controls',
      },
      optionTabAttr: {
        controls: 'aria-controls',
        selectedColor: 'data-selected-color',
        category: 'data-category',
      },
      helpLinkAttr: { controls: 'aria-controls' },
      warningMsg: {
        disabled:
          'Your browser supports WebGL, but it is disabled. WebGL is needed ' +
          'to view the 3D model. See "WebGL Disabled" in the help section ' +
          'below.',
        unsupported:
          'Your browser does not supports WebGL. WebGL is needed to view ' +
          'the 3D model. See "WebGL Supported Browsers" in the help section ' +
          'below.',
      },
    },
    helpSxnDropdown = dropdown(),
    isModelContainerValid = container => {
      return (
        library.isElementNode(container) &&
        library.elementHasReqAttrs(container, modelArgs.containerAttr)
      );
    },
    isOptionNavBtnValid = btn => {
      return (
        library.isValidRadioBtn(btn) &&
        library.elementHasReqAttrs(btn, modelArgs.optionNavBtnAttr)
      );
    },
    isOptionTabValid = tab => {
      return (
        library.isElementNode(tab) &&
        library.elementHasReqAttrs(tab, modelArgs.optionTabAttr)
      );
    },
    getModelId = node => {
      if (library.isElementNode(node)) {
        return node.getAttribute(modelArgs.containerAttr.modelId);
      }
      return '';
    },
    getOptionNavBtn = id => {
      let btns = null,
        btn = null;

      if (library.isString(id)) {
        btns = library.getElements(modelArgs.class.optionNavBtn);

        for (let i = 0; i < btns.length; i++) {
          btn = btns[i];

          if (
            isOptionNavBtnValid(btn) &&
            btn.getAttribute(modelArgs.optionNavBtnAttr.controls) === id &&
            library.isRadioBtnSelected(btn)
          ) {
            return btn;
          }
        }
      }

      return null;
    },
    openCorrespondingDropdown = (args, evt) => {
      evt.stopPropagation();
      setTimeout(smoothScroll.jump, 280, args.node, { offset: -75 });
      helpSxnDropdown.open(args);
    },
    addHelpSxnLinkEvents = container => {
      let links = null,
        link = null,
        sxnId = '',
        sxn = null,
        sxnBtn = null,
        sxnMenu = null;

      if (library.isElementNode(container)) {
        links = library.getElements(modelArgs.class.helpLink, container);

        for (let i = 0; i < links.length; i++) {
          link = links[i];
          sxnId = link.getAttribute(modelArgs.helpLinkAttr.controls);

          if (sxnId) {
            sxn = document.getElementById(sxnId);
          }

          if (sxn) {
            sxnBtn = library.getFirstElement(modelArgs.class.dropdownBtn, sxn);
            sxnMenu = library.getFirstElement(
              modelArgs.class.dropdownMenu,
              sxn
            );
          }

          if (sxnBtn) {
            link.addEventListener(
              'click',
              helpSxnDropdown.closeDropdownsOutsideClick.bind(
                null,
                openCorrespondingDropdown,
                {
                  node: sxn,
                  btn: sxnBtn,
                  menu: sxnMenu,
                }
              ),
              false
            );
          }
        }
      }
    },
    addHelpSxnClickEvents = args => {
      if (helpSxnDropdown.isReqArgsValid(args)) {
        args.btn.addEventListener(
          'click',
          helpSxnDropdown.toggle.bind(null, args),
          false
        );

        args.submenus.forEach(submenu => {
          if (helpSxnDropdown.isReqArgsValid(submenu)) {
            addHelpSxnClickEvents(submenu);
          }
        });
      }
    },
    selectOption = (optionBtns, evt) => {
      if (library.isArrayLike(optionBtns)) {
        const selectedOptionBtn = evt.currentTarget,
          value = selectedOptionBtn.getAttribute(modelArgs.optionBtnAttr.color),
          optionGroup = selectedOptionBtn.parentNode.parentNode.parentNode,
          optionGroupId = optionGroup.getAttribute('id'),
          optionNavBtn = getOptionNavBtn(optionGroupId);
        let materialName = '',
          category = '',
          materialsToUpdate = null;

        if (isOptionNavBtnValid(optionNavBtn)) {
          library.toggleRadioBtns(optionBtns, selectedOptionBtn);
          category = optionNavBtn.getAttribute(
            modelArgs.optionNavBtnAttr.category
          );
          optionNavBtn.setAttribute('data-selected-color', value);
          materialsToUpdate = materialCategories[category];

          for (let i = 0; i < materialsToUpdate.length; i++) {
            materialName = materialsToUpdate[i];

            if (
              materialName &&
              library.objectHasOwnProperty(materials, materialName) &&
              configurator.api.materialHash[materialName]
            ) {
              configurator.api.setColor(
                materialName,
                configurator.api.AlbedoPBR,
                'color',
                value
              );
              configurator.api.setColor(
                materialName,
                configurator.api.DiffusePBR,
                'color',
                value
              );
              configurator.api.setColor(
                materialName,
                configurator.api.DiffuseColor,
                'color',
                value
              );
            }
          }
        }
      }
    },
    selectOptionTab = (optionTabs, optionBtns, evt) => {
      let selectedOptionTab = null,
        tabSxnId = '',
        tabSxns = null,
        tabSxn = null;

      if (library.isArrayLike(optionTabs) && library.isArrayLike(optionBtns)) {
        selectedOptionTab = evt.target;

        if (isOptionTabValid(selectedOptionTab)) {
          library.toggleRadioBtns(optionTabs, selectedOptionTab);
          tabSxnId = selectedOptionTab.getAttribute(
            modelArgs.optionTabAttr.controls
          );
          tabSxn = document.getElementById(tabSxnId);
          tabSxns = library.getElements(modelArgs.class.colorGrid);
          library.toggleRadioNodes(tabSxns, tabSxn);
          setOptionBtns(optionBtns, selectedOptionTab);
        }
      }
    },
    setOptionBtns = (optionBtns, selectedNavBtn) => {
      let selectedColor = '',
        selectedOptionBtn = null;

      if (
        library.isArrayLike(optionBtns) &&
        isOptionNavBtnValid(selectedNavBtn) &&
        selectedNavBtn.hasAttribute('data-selected-color')
      ) {
        selectedColor = selectedNavBtn
          .getAttribute('data-selected-color')
          .toLowerCase();
        selectedOptionBtn = library.getSelectedBtn(optionBtns, function(btn) {
          return (
            btn.hasAttribute(modelArgs.optionBtnAttr.color) &&
            btn.getAttribute(modelArgs.optionBtnAttr.color).toLowerCase() ===
              selectedColor
          );
        });

        library.toggleRadioBtns(optionBtns, selectedOptionBtn);
      }
    },
    onModelAPIReady = selectedOptionNavBtn => {
      const optionBtns = configurator.optionBtns;
      const optionNavBtns = configurator.optionNavBtns;

      configurator.api.removeEventListener(
        configurator.api.EVENT_INITIALIZED,
        onModelAPIReady
      );

      setOptionBtns(optionBtns, selectedOptionNavBtn);

      for (let i = 0; i < optionBtns.length; i++) {
        optionBtns[i].addEventListener(
          'click',
          selectOption.bind(null, optionBtns),
          false
        );
      }

      for (let i = 0; i < optionNavBtns.length; i++) {
        optionNavBtns[i].addEventListener(
          'click',
          selectOptionTab.bind(null, optionNavBtns, optionBtns),
          false
        );
      }

      configurator.modelContents.setAttribute(
        modelArgs.sxnAttr.overlay,
        'false'
      );
    },
    initializeModel = (config, selectedOptionNavBtn) => {
      configurator.config = config;
      configurator.api = new SketchfabAPIUtility(
        configurator.config.id,
        configurator.iframe,
        {
          camera: 0,
          preload: 1,
          ui_infos: 0,
          ui_controls: 0,
          graph_optimizer: 0,
          ui_watermark: 0,
          autostart: 1,
        }
      );

      configurator.api.addEventListener(
        configurator.api.EVENT_INITIALIZED,
        onModelAPIReady.bind(null, selectedOptionNavBtn)
      );

      configurator.api.create();
      library.showElement(
        library.getFirstElement(
          modelArgs.class.mediaIframe,
          configurator.modelContainer
        )
      );
    },
    load = () => {
      const webglDetection = library.detectWebgl(),
        models = library.getElements(modelArgs.class.media);
      let model = null,
        modelId = '',
        selectedOptionNavBtn = null,
        modelSxn = null,
        buildingHasModel = false,
        helpSxn = null,
        helpDropdown = {},
        warning = null;

      for (let i = 0; i < models.length; i++) {
        model = models[i];
        configurator.modelContainer = model.parentNode;
        modelSxn = configurator.modelContainer.parentNode;
        configurator.modelContents = modelSxn.parentNode;
        helpSxn = library.getFirstElement(modelArgs.class.helpSxn, modelSxn);

        if (helpSxn) {
          helpDropdown = helpSxnDropdown.getDropdownStructure(helpSxn);

          if (helpSxnDropdown.isReqArgsValid(helpDropdown)) {
            addHelpSxnClickEvents(helpDropdown);
            addHelpSxnLinkEvents(modelSxn);
          }
        }

        if (modelSxn) {
          warning = library.getFirstElement(modelArgs.class.warning, modelSxn);
        }

        if (webglDetection <= 0) {
          if (warning) {
            warning.innerHTML =
              webglDetection === 0
                ? modelArgs.warningMsg.unsupported
                : modelArgs.warningMsg.disabled;

            library.showElement(warning);

            helpSxnDropdown.open({
              node: helpDropdown.node,
              btn: helpDropdown.btn,
              menu: helpDropdown.menu,
              btnDisplay: helpDropdown.btnDisplay,
            });
          }
        } else {
          if (warning) {
            library.hideElement(warning);
          }

          if (
            modelSxn.hasAttribute('model') &&
            modelSxn.getAttribute('model') === 'true'
          ) {
            buildingHasModel = true;
          }

          if (buildingHasModel) {
            configurator.modelContents.setAttribute(
              modelArgs.sxnAttr.overlay,
              'true'
            );
          }

          if (
            buildingHasModel &&
            isModelContainerValid(configurator.modelContainer)
          ) {
            modelId = getModelId(configurator.modelContainer);
            media.loadInteractiveIframe(model, 'sketchfab', modelId);
          }

          if (modelId) {
            configurator.iframe = library.getFirstElement(
              modelArgs.class.mediaIframe,
              model
            );
          }

          if (modelId && configurator.iframe) {
            configurator.optionBtns = library.getElements(
              modelArgs.class.optionBtn,
              configurator.modelContents
            );
            configurator.optionNavBtns = library.getElements(
              modelArgs.class.optionNavBtn,
              configurator.modelContents
            );
            selectedOptionNavBtn = library.getSelectedBtn(
              configurator.optionNavBtns,
              library.isRadioBtnSelected
            );

            initializeModel({ id: modelId }, selectedOptionNavBtn);
          }
        }
      }
    };
  return {
    load,
  };
})(document, library, media, dropdown, smoothScroll);
