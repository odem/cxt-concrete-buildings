# CXT Incorporated Concrete Buildings Website Rebuild

CXT Concrete Buildings is a leading manufacturer of precast concrete products, including restrooms, showers, concession and utility buildings. Their buildings are used nationwide at federal, state, county, city, and private recreational sites.

Currently, working with CXT to finalize the rebuild of their website. I'm waiting on information and resources for a few buildings, and other resources, so you will see placeholder images, and blank page sections where the needed content will go.

The rebuild includes modernizing the site to be responsive, accessible, and SEO-friendly, as well as adding new functionality. The new functionality includes interactive 3D building models (users can change building colors in real-time), sorting and filtering information, and email form automation. The rebuild also includes JavaScript fallbacks and warnings.

The rebuild does **not** include their replacement parts site.

CXT requested the website to be built **without** a CMS or framework (such as React, Angular, or Vue). They requested the final website files be (plain) HTML, CSS, JavaScript (JS), and PHP.

- **HTML:** Handlebars templates are used as HTML source files, which are compiled to (plain) HTML using Gulp.
- **CSS:** SCSS is used as the CSS source files, which is compiled to CSS and minified using Gulp.
- **JS:** Separate JS files are used to create 'modules' - making each file/module an IIFE to create namespaces. The JS files are written as ES6, and converted to browser-safe JS code using Babel. The separate JS files are combined and minified using Gulp.

## Demo

New site: https://www.shanaodem.com/cxt/

For comparison, old site: http://www.cxtinc.com/

## My Roles

- Co-Design
- Front-End Development
- Back-End Development

## Tools/Skills Used

- HTML5
- SCSS/CSS3
- JavaScript
- Handlebars.js
- Express.js
- Node.js
- PHP
- MySQL
- Nodemon
- Mocha
- Gulp

## File Structure

The `src` folder contains the raw `.hbs`, `.scss`, and `.js` files that get compiled into final production files. The `src` folder also contains the `.json` data files used in the Handlebars template files, and dev server file.
The `dist` folder contains a complete build of all production files, backend `.php` files, and resource files, including images, and pdfs.

### Dependencies

- babel-core
- babel-present-env
- del
- eslint
- eslint-config-google
- express
- gulp
- gulp-babel
- gulp-clean-css
- gulp-compile-handlebars
- gulp-concat
- gulp-mocha
- gulp-nodemon
- gulp-rename
- gulp-sass
- gulp-uglify
- handlebars
