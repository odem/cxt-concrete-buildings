const filter = (function(window, document, library) {
  const filterArgs = {
      class: {
        btn: 'js-filter-btn',
        item: 'js-filter-item',
      },
      itemAttr: {
        hidden: 'aria-hidden',
        category: 'data-category',
      },
      btnAttr: {
        option: 'data-option',
      },
    },
    btnHasReqAttrs = btn => {
      return library.elementHasReqAttrs(btn, filterArgs.btnAttr);
    },
    itemHasReqAttrs = item => {
      return library.elementHasReqAttrs(item, filterArgs.itemAttr);
    },
    isBtnValid = btn => {
      return library.isValidRadioBtn(btn) && btnHasReqAttrs(btn);
    },
    isItemValid = item => {
      return library.isElementNode(item) && itemHasReqAttrs(item);
    },
    isNumItemsValid = num => {
      return library.isPositiveNumber(num);
    },
    isBtnSelected = btn => {
      return library.isRadioBtnSelected(btn);
    },
    getBtns = node => {
      return library.getElements(filterArgs.class.btn, node);
    },
    getItems = node => {
      return library.getElements(filterArgs.class.item, node);
    },
    getItemCategory = item => {
      return isItemValid(item)
        ? item.getAttribute(filterArgs.itemAttr.category)
        : '';
    },
    getSelectedOption = selectedBtn => {
      return isBtnValid(selectedBtn)
        ? selectedBtn.getAttribute(filterArgs.btnAttr.option)
        : '';
    },
    selectBtn = (btns, selectedBtn) => {
      let currBtn = null;

      if (library.isArrayLike(btns) && isBtnValid(selectedBtn)) {
        for (let i = 0; i < btns.length; i++) {
          currBtn = btns[i];

          if (isBtnValid(currBtn)) {
            currBtn === selectedBtn
              ? library.selectRadioBtn(currBtn)
              : library.unselectRadioBtn(currBtn);
          }
        }
      }
    },
    displayItems = args => {
      const { btns, selectedBtn, items, numItems } = args;
      let selectedOption = '',
        item = null,
        itemCategory = '',
        totalNumItems = 0,
        numItemsDisplayed = 0,
        displayNumItems = 0;

      if (
        library.isArrayLike(btns) &&
        isBtnValid(selectedBtn) &&
        library.isArrayLike(items)
      ) {
        selectBtn(btns, selectedBtn);
        selectedOption = getSelectedOption(selectedBtn).toLowerCase();
        totalNumItems = items.length;
        displayNumItems = isNumItemsValid(numItems) ? numItems : totalNumItems;

        for (let i = 0; i < totalNumItems; i++) {
          item = items[i];

          if (isItemValid(item)) {
            itemCategory = getItemCategory(item).toLowerCase();

            if (
              (selectedOption === itemCategory || selectedOption === 'all') &&
              numItemsDisplayed < displayNumItems
            ) {
              library.showElement(item);
              numItemsDisplayed++;
            } else {
              library.hideElement(item);
            }
          }
        }
      } else {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to display filter items.'
        );
      }
    };
  return {
    isBtnValid,
    isItemValid,
    isBtnSelected,
    getBtns,
    getItems,
    getItemCategory,
    getSelectedOption,
    displayItems,
  };
})(window, document, library);
