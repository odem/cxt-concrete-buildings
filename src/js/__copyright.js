const copyright = (function(document, library) {
  const args = {
      class: {
        year: 'js-copyrightyear',
      },
      dataAttr: {
        initYear: 'data-initialyear',
      },
    },
    getYearNodes = () => {
      return document.getElementsByClassName(args.class.year);
    },
    isNodeValid = node => {
      return library.isElementNode(node);
    },
    nodeHasInitialYear = node => {
      return isNodeValid(node) && node.hasAttribute(args.dataAttr.initYear);
    },
    getInitialYear = node => {
      return parseInt(node.getAttribute(args.dataAttr.initYear), 10);
    },
    isYearValid = year => {
      return library.isPositiveNumber(year);
    },
    getCopyrightYear = initYear => {
      const currYear = new Date().getFullYear();
      let copyright = '';

      if (currYear) {
        if (isYearValid(initYear) && initYear < currYear) {
          copyright = initYear + '-' + currYear;
        } else {
          copyright = String(currYear);
        }
      }

      return copyright;
    },
    insertYear = ({ xhr, node, initYear }) => {
      let responseData = '';

      if (isNodeValid(node) && (!initYear || isYearValid(initYear))) {
        /** Server responses with an success signal. */
        if (xhr && xhr.status === 200) {
          responseData = JSON.parse(xhr.responseText);

          /** Uses server's copyright data. */
          if (responseData.copyright != '') {
            node.innerHTML = responseData.copyright;
          } else {
            /** JavaScript computes the copyright year. */
            node.innerHTML = getCopyrightYear(initYear);
          }
        } else {
          /** Server responses with a fail signal, so JavaScript computes the
           copyright year. */
          node.innerHTML = getCopyrightYear(initYear);
        }
      }
    },
    setYear = () => {
      const nodes = getYearNodes();
      let initYear = null,
        encodedData = null,
        xhr = null,
        currNode = null;

      for (let i = 0; i < nodes.length; i++) {
        currNode = nodes[i];

        if (currNode) {
          if (nodeHasInitialYear(currNode)) {
            initYear = getInitialYear(currNode);
          }

          encodedData = 'initialyear=' + encodeURIComponent(initYear);
          xhr = new XMLHttpRequest();
          xhr.open('POST', 'copyrightyear.php', true);
          xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
          xhr.setRequestHeader(
            'Content-Type',
            'application/x-www-form-urlencoded; charset=UTF-8'
          );
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
          xhr.send(encodedData);

          xhr.addEventListener(
            'loadend',
            insertYear.bind(null, { xhr, node: currNode, initYear }),
            false
          );
        }
      }
    };
  return {
    setYear,
  };
})(document, library);
