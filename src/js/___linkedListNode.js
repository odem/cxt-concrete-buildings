const linkedListNode = (function(library) {
  const createDefaultNode = (nodeType, data, next) => {
      const node = { data: null, next: null };

      if (data != undefined) {
        node.data = data;
      }

      if (library.isFunction(nodeType) && nodeType(next)) {
        node.next = next;
      }

      return node;
    },
    isDefaultNode = node => {
      return (
        library.isObject(node) &&
        library.objectHasOwnProperty(node, 'data') &&
        library.objectHasOwnProperty(node, 'next')
      );
    },
    isSingleLinkedNode = node => {
      return isDefaultNode(node);
    },
    isDoubleLinkedNode = node => {
      return isDefaultNode(node) && library.objectHasOwnProperty(node, 'prev');
    };

  return {
    isSingleLinkedNode: function(node) {
      return isSingleLinkedNode(node);
    },
    isDoubleLinkedNode: function(node) {
      return isDoubleLinkedNode(node);
    },
    createSingleLinked: function(...args) {
      return createDefaultNode(isSingleLinkedNode, ...args);
    },
    createDoubleLinked: function(...args) {
      const prev = args[2],
        node = createDefaultNode(isDoubleLinkedNode, ...args);
      node.prev = isDoubleLinkedNode(prev) ? prev : null;

      return node;
    },
  };
})(library);
