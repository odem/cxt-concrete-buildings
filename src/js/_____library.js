const library = (function(window) {
  const isObject = obj => {
      return Boolean(obj) && typeof obj === 'object' && !Array.isArray(obj);
    },
    isFunction = func => {
      return Boolean(func) && typeof func === 'function';
    },
    isString = str => {
      return typeof str === 'string';
    },
    isNumber = value => {
      return (
        typeof value === 'number' &&
        isFinite(value) &&
        !isNaN(parseFloat(value))
      );
    },
    isPositiveNumber = value => {
      return isNumber(value) && value >= 0;
    },
    isInt = value => {
      return (
        (Number.isInteger && Number.isInteger(value)) ||
        (typeof value === 'number' &&
          isFinite(value) &&
          Math.floor(value) === value)
      );
    },
    isPositiveInt = value => {
      return isInt(value) && value > -1;
    },
    isHTMLCollection = obj => {
      return Object.prototype.toString.call(obj) === '[object HTMLCollection]';
    },
    isNodelist = obj => {
      return Object.prototype.toString.call(obj) === '[object NodeList]';
    },
    isArray = obj => {
      return Object.prototype.toString.call(obj) === '[object Array]';
    },
    objectHasOwnProperty = (obj, property) => {
      // Avoids bugs when hasOwnProperty is shadowed
      return (
        isObject(obj) &&
        isString(property) &&
        Object.prototype.hasOwnProperty.call(obj, property)
      );
    },
    isDocumentNode = obj => {
      return (
        isObject(obj) &&
        !objectHasOwnProperty(obj, 'nodeType') &&
        obj.nodeType === 9
      );
    },
    isElementNode = obj => {
      return (
        isObject(obj) &&
        !objectHasOwnProperty(obj, 'nodeType') &&
        obj.nodeType === 1
      );
    },
    isArrayLike = obj => {
      return (
        (Array.isArray ? Array.isArray(obj) : isArray(obj)) ||
        isHTMLCollection(obj) ||
        isNodelist(obj)
      );
    },
    isObjectEmpty = obj => {
      return isObject(obj) && Object.keys(obj).length === 0;
    },
    isStringEmpty = str => {
      return isString(str) && str === '';
    },
    isIndexValid = (index, collection) => {
      return (
        isPositiveInt(index) &&
        isArrayLike(collection) &&
        index < collection.length
      );
    },
    isValidBtn = btn => {
      return isElementNode(btn) && btn.hasAttribute('aria-pressed');
    },
    isValidRadioBtn = btn => {
      return isValidBtn(btn) && btn.hasAttribute('aria-disabled');
    },
    isBtnPressed = btn => {
      return isValidBtn(btn) && btn.getAttribute('aria-pressed') === 'true';
    },
    isBtnDisabled = btn => {
      return isValidBtn(btn) && btn.getAttribute('aria-disabled') === 'true';
    },
    isRadioBtnSelected = btn => {
      return isValidRadioBtn(btn) && isBtnPressed(btn) && isBtnDisabled(btn);
    },
    isElementDisabled = element => {
      return (
        isElementNode(element) &&
        element.hasAttribute('disabled') &&
        element.getAttribute('tabindex') === '-1' &&
        element.getAttribute('aria-disabled') === 'true'
      );
    },
    isValidRadioNode = node => {
      return isElementNode(node) && node.hasAttribute('aria-hidden');
    },
    getObjectValue = (obj, property) => {
      return objectHasOwnProperty(obj, property) ? obj[property] : undefined;
    },
    enableElementNode = element => {
      if (isElementNode(element)) {
        element.setAttribute('aria-disabled', 'false');
        element.removeAttribute('disabled');
        element.removeAttribute('tabindex');
      }
    },
    disableElementNode = element => {
      if (isElementNode(element)) {
        element.setAttribute('aria-disabled', 'true');
        element.setAttribute('disabled', 'true');
        element.setAttribute('tabindex', '-1');
      }
    },
    selectBtn = btn => {
      if (isValidBtn(btn)) {
        btn.setAttribute('aria-pressed', 'true');
      }
    },
    unselectBtn = btn => {
      if (isValidBtn(btn)) {
        btn.setAttribute('aria-pressed', 'false');
      }
    },
    toggleBtn = btn => {
      if (isValidBtn(btn)) {
        isBtnPressed(btn) ? unselectBtn(btn) : selectBtn(btn);
      }
    },
    enableBtn = btn => {
      if (isValidRadioBtn(btn)) {
        btn.setAttribute('aria-disabled', 'false');
        btn.removeAttribute('disabled');
        btn.removeAttribute('tabindex');
      }
    },
    disableBtn = btn => {
      if (isValidRadioBtn(btn)) {
        btn.setAttribute('aria-disabled', 'true');
        btn.setAttribute('disabled', 'true');
        btn.setAttribute('tabindex', '-1');
      }
    },
    selectRadioBtn = btn => {
      if (isValidRadioBtn(btn)) {
        selectBtn(btn);
        disableBtn(btn);
      }
    },
    unselectRadioBtn = btn => {
      if (isValidRadioBtn(btn)) {
        unselectBtn(btn);
        enableBtn(btn);
      }
    },
    toggleRadioBtns = (btns, selectedBtn) => {
      let currBtn = null;

      if (isArrayLike(btns) && isValidRadioBtn(selectedBtn)) {
        for (let i = 0; i < btns.length; i++) {
          currBtn = btns[i];

          if (isValidRadioBtn(currBtn)) {
            currBtn === selectedBtn
              ? selectRadioBtn(currBtn)
              : unselectRadioBtn(currBtn);
          }
        }
      }
    },
    getSelectedBtn = (btns, isBtnSelectedFunc) => {
      let btnsProcessed = 0,
        btn = null;

      if (isArrayLike(btns) && isFunction(isBtnSelectedFunc)) {
        while (btnsProcessed < btns.length) {
          btn = btns[btnsProcessed];

          if (isBtnSelectedFunc(btn)) {
            return btn;
          }

          btnsProcessed++;
        }
      }

      return null;
    },
    toggleRadioNodes = (nodes, selectedNode) => {
      let currNode = null;

      if (isArrayLike(nodes) && isValidRadioNode(selectedNode)) {
        for (let i = 0; i < nodes.length; i++) {
          currNode = nodes[i];

          if (isValidRadioNode(currNode)) {
            currNode === selectedNode
              ? showElement(currNode)
              : hideElement(currNode);
          }
        }
      }
    },
    errorMsg = () => {
      const invalidArg = 'invalid argument',
        invalidArgs = 'one or more invalid arguments',
        outRangeArg = 'argument is out of valid range',
        argsEvaluationFail = 'unable to evaluate parameter(s)',
        serverRequestFail = 'server request failed.',
        serverResponseIssue = 'issue with the server response.',
        connectionFailure = 'there is an issue with the connection.';

      return {
        argsEvaluationFail,
        invalidArg,
        invalidArgs,
        outRangeArg,
        serverRequestFail,
        serverResponseIssue,
        connectionFailure,
      };
    },
    objectAssign = (target, ...sources) => {
      let newObject = null,
        source = null;

      if (target) {
        newObject = Object(target);

        for (let i = 0; i < sources.length; i++) {
          source = sources[i];

          if (source) {
            for (const key in source) {
              if (objectHasOwnProperty(source, key)) {
                newObject[key] = source[key];
              }
            }
          }
        }
      }

      return newObject;
    },
    updateObject = (...args) => {
      return Object.assign ? Object.assign(...args) : objectAssign(...args);
    },
    composeObject = (...args) => {
      return Object.assign ? Object.assign(...args) : objectAssign(...args);
    },
    copyArray = array => {
      if (Array.isArray(array)) {
        return array.slice(0, array.length);
      }

      return [];
    },
    nodelistToArray = nodeList => {
      if (isNodelist(nodeList) || isHTMLCollection(nodeList)) {
        return Array.from
          ? Array.from(nodeList)
          : Array.prototype.slice.call(nodeList);
      }

      return [];
    },
    toggleDataAttribute = args => {
      const { element, attribute, state1, state2 } = args;
      let newState = '';

      if (
        isElementNode(element) &&
        isString(attribute) &&
        isString(state1) &&
        isString(state2) &&
        element.hasAttribute(attribute)
      ) {
        newState = element.getAttribute(attribute) === state1 ? state2 : state1;
        element.setAttribute(attribute, newState);
        return true;
      }

      return false;
    },
    setRadialAttribute = args => {
      const { elements, targetElement, attribute, attributeValue } = args;
      let currElement = null;

      if (
        !isArrayLike(elements) ||
        !isElementNode(targetElement) ||
        !isString(attribute) ||
        !isString(attributeValue)
      ) {
        throw new TypeError(
          errorMsg().argsEvaluationFail + ' to set radial attribute.'
        );
      }

      for (let i = 0; i < elements.length; i++) {
        currElement = elements[i];

        if (isElementNode(currElement)) {
          currElement.removeAttribute(attribute);

          if (currElement === targetElement) {
            currElement.setAttribute(attribute, attributeValue);
          }
        }
      }
    },
    hideElement = element => {
      if (isElementNode(element)) {
        element.setAttribute('aria-hidden', 'true');
      }
    },
    showElement = element => {
      if (isElementNode(element)) {
        element.setAttribute('aria-hidden', 'false');
      }
    },
    getCSSProperty = (element, property) => {
      if (isElementNode(element) && isString(property)) {
        return window.getComputedStyle(element).getPropertyValue(property);
      }

      return '';
    },
    getElementTotalWidth = element => {
      let totalWidth = 0,
        width = 0,
        leftMargin = 0,
        rightMargin = 0;

      if (isElementNode(element)) {
        width = element.offsetWidth;
        leftMargin = parseInt(getCSSProperty(element, 'margin-left'), 10) || 0;
        rightMargin =
          parseInt(getCSSProperty(element, 'margin-right'), 10) || 0;

        totalWidth = width + leftMargin + rightMargin;
      }

      return totalWidth;
    },
    getNextElementSibling = node => {
      if (isElementNode(node)) {
        return node.nextElementSibling;
      }
    },
    getPreviousElementSibling = node => {
      if (isElementNode(node)) {
        return node.previousElementSibling;
      }
    },
    getMaxNumDisplayElements = (viewport, element) => {
      let viewportWidth = 0,
        viewportLeftPad = 0,
        viewportRightPad = 0,
        elementTotalWidth = 0,
        maxNumDisplayElements = 0;

      if (!isElementNode(viewport) || !isElementNode(element)) {
        throw new TypeError(
          errorMsg().argsEvaluationFail +
            ' to get max number of display elements'
        );
      }

      elementTotalWidth = getElementTotalWidth(element);

      if (elementTotalWidth > 0) {
        viewportLeftPad =
          parseInt(library.getCSSProperty(viewport, 'padding-left')) || 0;
        viewportRightPad =
          parseInt(library.getCSSProperty(viewport, 'padding-Right')) || 0;
        viewportWidth =
          viewport.clientWidth - viewportLeftPad - viewportRightPad;
        maxNumDisplayElements = Math.floor(viewportWidth / elementTotalWidth);
      }

      return maxNumDisplayElements;
    },
    isElementFound = (element, matchOptions) => {
      let refElement = null,
        attribute = '',
        attributeValue = '',
        doesElementMatch = undefined,
        doesElementHaveAttr = undefined,
        doesAttrMatch = undefined;

      if (!isElementNode(element) || !isObject(matchOptions)) {
        throw new TypeError('Cannot evaluate element for match.');
      }

      refElement = getObjectValue(matchOptions, 'refElement');
      attribute = getObjectValue(matchOptions, 'attribute');
      attributeValue = getObjectValue(matchOptions, 'attributeValue');

      if (
        (refElement && !isElementNode(refElement)) ||
        (attribute && !isString(attribute)) ||
        (attributeValue && !isString(attributeValue))
      ) {
        throw new TypeError('Cannot evaluate match options to find element.');
      }

      doesElementMatch = refElement ? element === refElement : undefined;
      doesElementHaveAttr = attribute
        ? element.hasAttribute(attribute)
        : undefined;
      doesAttrMatch =
        attributeValue && doesElementHaveAttr
          ? element.getAttribute(attribute) === attributeValue
          : undefined;

      if (refElement && attribute && attributeValue) {
        return doesElementMatch && doesAttrMatch;
      }

      if (refElement && attribute) {
        return doesElementMatch && doesElementHaveAttr;
      }

      if (attribute && attributeValue) {
        return doesAttrMatch;
      }

      if (attribute) {
        return doesElementHaveAttr;
      }

      if (refElement) {
        return doesElementMatch;
      }

      return false;
    },
    getPrevAndCurrElementIndex = (elements, matchOptions) => {
      const prevInitialIndex = -1,
        currInitialIndex = -1;
      let numElements = 0,
        element = null,
        index = 0,
        prevIndex = prevInitialIndex,
        currIndex = currInitialIndex,
        prevElementMatchOptions = {},
        currElementMatchOptions = {};

      if (!isArrayLike(elements) || !isObject(matchOptions)) {
        throw new TypeError(
          errorMsg().argsEvaluationFail +
            ' to get previous and current element index.'
        );
      }

      numElements = elements.length;

      if (!numElements) {
        return null;
      }

      prevElementMatchOptions = {
        attribute: getObjectValue(matchOptions, 'prevAttribute'),
        attributeValue: getObjectValue(matchOptions, 'prevAttributeValue'),
        refElement: getObjectValue(matchOptions, 'prevRefElement'),
      };

      currElementMatchOptions = {
        attribute: getObjectValue(matchOptions, 'currAttribute'),
        attributeValue: getObjectValue(matchOptions, 'currAttributeValue'),
        refElement: getObjectValue(matchOptions, 'currRefElement'),
      };

      while (
        index < numElements &&
        (prevIndex === prevInitialIndex || currIndex === currInitialIndex)
      ) {
        element = elements[index];

        if (isElementNode(element)) {
          if (isElementFound(element, prevElementMatchOptions)) {
            prevIndex = index;
          }

          if (isElementFound(element, currElementMatchOptions)) {
            currIndex = index;
          }
        }

        index++;
      }

      return { prevIndex, currIndex };
    },
    getCollectionElement = (collection, matchCriteria) => {
      let currElement = null;

      if (
        !isArrayLike(collection) ||
        !objectHasOwnProperty(matchCriteria, 'attribute') ||
        (objectHasOwnProperty(matchCriteria, 'attributeValue') &&
          !isString(matchCriteria.attributeValue))
      ) {
        throw new TypeError(
          errorMsg().argsEvaluationFail + ' to get collection element.'
        );
      }

      for (let currIndex = 0; currIndex < collection.length; currIndex++) {
        currElement = collection[currIndex];

        if (
          isElementNode(currElement) &&
          isElementFound(currElement, matchCriteria)
        ) {
          return { element: currElement, index: currIndex };
        }
      }

      return null;
    },
    enableElementOverlay = element => {
      if (isElementNode(element)) {
        element.setAttribute('data-overlay', 'true');
      }
    },
    disableElementOverlay = element => {
      if (isElementNode(element)) {
        element.setAttribute('data-overlay', 'false');
      }
    },
    detectWebgl = () => {
      const canvas = document.createElement('canvas'),
        contextNames = [
          'webgl',
          'experimental-webgl',
          'moz-webgl',
          'webkit-3d',
        ];

      // Check for the WebGL rendering context
      if (window.WebGLRenderingContext) {
        for (let i = 0; i < contextNames.length; i++) {
          if (canvas.getContext && canvas.getContext(contextNames[i])) {
            // WebGL is enabled.
            return 1;
          }
        }

        // WebGL is supported, but disabled.
        return -1;
      }

      // WebGL not supported.
      return 0;
    },
    getFirstElement = (classStr, node) => {
      if (isString(classStr)) {
        if (isElementNode(node)) {
          return node.getElementsByClassName(classStr)[0];
        } else {
          return document.getElementsByClassName(classStr)[0];
        }
      }

      return null;
    },
    getElements = (classStr, node) => {
      if (isString(classStr)) {
        if (isElementNode(node)) {
          return node.getElementsByClassName(classStr);
        } else {
          return document.getElementsByClassName(classStr);
        }
      }

      return null;
    },
    elementHasReqAttrs = (node, reqAttrs) => {
      if (!isElementNode(node) || !isObject(reqAttrs)) {
        return false;
      }

      for (const attr in reqAttrs) {
        if (!node.hasAttribute(reqAttrs[attr])) {
          return false;
        }
      }

      return true;
    },
    exportedFunctions = () => {
      return {
        objectHasOwnProperty,
        isObject,
        isFunction,
        isString,
        isNumber,
        isPositiveNumber,
        isInt,
        isPositiveInt,
        isHTMLCollection,
        isNodelist,
        isDocumentNode,
        isElementNode,
        isArray,
        isArrayLike,
        isObjectEmpty,
        isStringEmpty,
        isIndexValid,
        isValidBtn,
        isValidRadioBtn,
        isBtnPressed,
        isBtnDisabled,
        isRadioBtnSelected,
        isElementDisabled,
        enableElementNode,
        disableElementNode,
        enableBtn,
        disableBtn,
        selectBtn,
        unselectBtn,
        selectRadioBtn,
        unselectRadioBtn,
        toggleBtn,
        toggleRadioBtns,
        setRadialAttribute,
        getSelectedBtn,
        toggleRadioNodes,
        errorMsg,
        objectAssign,
        updateObject,
        composeObject,
        copyArray,
        nodelistToArray,
        toggleDataAttribute,
        hideElement,
        showElement,
        getCSSProperty,
        getElementTotalWidth,
        getNextElementSibling,
        getPreviousElementSibling,
        getMaxNumDisplayElements,
        isElementFound,
        getPrevAndCurrElementIndex,
        getCollectionElement,
        enableElementOverlay,
        disableElementOverlay,
        detectWebgl,
        getFirstElement,
        getElements,
        elementHasReqAttrs,
      };
    };

  // Test Purposes
  // module.exports = exportedFunctions();

  return exportedFunctions();
})(window);
