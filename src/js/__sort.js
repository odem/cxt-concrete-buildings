const sort = (function(window, document, library) {
  const sortArgs = {
      class: {
        btn: 'js-sort-btn',
        item: 'js-sort-item',
      },
      btnAttr: {
        category: 'data-category',
        order: 'data-order',
      },
      itemAttr: { name: 'data-itemName' },
    },
    isBtnValid = btn => {
      return (
        library.isValidRadioBtn(btn) &&
        library.elementHasReqAttrs(btn, sortArgs.btnAttr)
      );
    },
    isItemValid = item => {
      return (
        library.isElementNode(item) &&
        library.elementHasReqAttrs(item, sortArgs.itemAttr)
      );
    },
    isCategoryValid = category => {
      if (library.isString(category)) {
        category = category.toLowerCase();
        return category === 'alphabetical';
      }

      return false;
    },
    isBtnSelected = btn => {
      return library.isRadioBtnSelected(btn);
    },
    getBtns = node => {
      return library.getElements(sortArgs.class.btn, node);
    },
    getItems = node => {
      return library.getElements(sortArgs.class.item, node);
    },
    getItemName = item => {
      if (isItemValid(item)) {
        return item.getAttribute(sortArgs.itemAttr.name).toLowerCase();
      }

      return '';
    },
    getItemValue = (item, sortCategory) => {
      if (isItemValid(item)) {
        sortCategory = sortCategory.toLowerCase();

        if (sortCategory === 'alphabetical') {
          return getItemName(item);
        }
      }

      return null;
    },
    getSelectedCategory = selectedBtn => {
      if (isBtnValid(selectedBtn)) {
        return selectedBtn.getAttribute(sortArgs.btnAttr.category);
      }

      return '';
    },
    getSelectedOrder = selectedBtn => {
      if (isBtnValid(selectedBtn)) {
        return selectedBtn.getAttribute(sortArgs.btnAttr.order);
      }

      return '';
    },
    selectBtn = (btns, selectedBtn) => {
      if (library.isArrayLike(btns) && isBtnValid(selectedBtn)) {
        library.toggleRadioBtns(btns, selectedBtn);
      }
    },
    sortItems = (item1, item2, category) => {
      let item1Value = null,
        item2Value = null;

      if (!isCategoryValid(category)) {
        throw new RangeError('invalid sort category.');
      }

      if (!isItemValid(item1) && isItemValid(item2)) {
        return 1;
      } else if (isItemValid(item1) && !isItemValid(item2)) {
        return -1;
      } else if (!isItemValid(item1) && !isItemValid(item2)) {
        return 0;
      } else {
        category = category.toLowerCase();
        item1Value = getItemValue(item1, category);
        item2Value = getItemValue(item2, category);

        if (item1Value && item2Value && item1Value > item2Value) {
          return 1;
        }

        if (item1Value && item2Value && item1Value < item2Value) {
          return -1;
        }

        return 0;
      }
    },
    sortDOMItems = args => {
      const { btns, selectedBtn, items } = args;
      let currItems = [],
        sortCategory = '',
        sortOrder = '',
        sortIndex = 0,
        itemsContainer = null,
        item = null;

      if (
        library.isArrayLike(btns) &&
        isBtnValid(selectedBtn) &&
        library.isArrayLike(items)
      ) {
        selectBtn(btns, selectedBtn);
        sortCategory = getSelectedCategory(selectedBtn).toLowerCase();
        sortOrder = getSelectedOrder(selectedBtn).toLowerCase();
        itemsContainer = items[0].parentNode;
        currItems = library.nodelistToArray(items);

        if (currItems.length && sortCategory) {
          currItems.sort(function(item1, item2) {
            return sortItems(item1, item2, sortCategory);
          });
        }

        library.hideElement(itemsContainer);

        for (let i = 0; i < currItems.length; i++) {
          sortIndex = sortOrder === 'descending' ? currItems.length - 1 - i : i;
          item = currItems[sortIndex];

          if (isItemValid(item)) {
            itemsContainer.appendChild(itemsContainer.removeChild(item));
          }
        }

        library.showElement(itemsContainer);
      } else {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to sort items.'
        );
      }
    };
  return {
    isBtnValid,
    isItemValid,
    isBtnSelected,
    getBtns,
    getItems,
    sortDOMItems,
  };
})(window, document, library);
