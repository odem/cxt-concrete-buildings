<?php
  require_once('buildingsfilterdbconnectvars.php');
  require_once('BuildingsFilterResponseClass.php');

  /** @var boolean indicates if submission was sent using AJAX or not. */
  $ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
          strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

  /** @var array contains db query results, and any errors. */
  $responseData = null;

  /** @var array maps option category names to column names. */
  $columns = [
    'building-type' => 'building_type',
    'building-size' => 'building_size',
    'plumbing' => 'plumbing',
    'restroom-type' => 'restroom_type',
    'num-restroom' => 'num_restroom',
    'num-toilet' => 'num_toilet',
    'num-toilet-ada' => 'num_toilet_ada',
    'num-shower' => 'num_shower',
    'num-shower-ada' => 'num_shower_ada',
    'num-sink' => 'num_sink',
  ];

  /**
   * Returns a string where each underscore (_) is replaced with a dash (-).
   *
   * If a non-string is provided, an empty string is returned.
   * @param string $str - string to replace underscores with dashes.
   *
   * @return string - if $str is not a string, empty string returned.
   */
  function underscoresToDashes($str) {
    return str_replace('_', '-', $str);
  }

  /**
   * TODO: comment
   *
   * @param string $columns - .
   *
   * @return array - .
   */
  function createQueryCondition($column, $values) {
    $numValues = 0;

    // TODO: type error
    if (!is_string($column) || !is_array($values)) {
      echo('type error');
    }

    if (!empty($column) && !empty($values)) {
      $numValues = count($values);

      if ($numValues == 1) {
        return "$column = ?";
      } else {
        $params = implode(",", array_fill(0, $numValues, "?"));

        return "$column IN ($params)";
      }
    }
  }

  /**
   * TODO: comment
   *
   * @param string $columns - .
   *
   * @return array - .
   */
  function getQueryConditions($params, $columns) {
    $query = [];
    $allParams = [];
    $queryConditions = "";

    // TODO: throw error
    if (!is_array($params) || !is_array($columns)) {
      echo('error');
    }

    foreach($params as $parameter => $values) {
      $parameter = trim(strtolower($parameter));
      $whitespaceTrimmedValues = preg_replace('/\s+/', '', $values);
      $valuesArray = explode(",", strtolower($whitespaceTrimmedValues));
      $column = array_key_exists($parameter, $columns) ? $columns[$parameter] : null;

      if (is_string($column) && !empty($column) && !empty($valuesArray)) {
        $condition = createQueryCondition($column, $valuesArray);
        $allParams = array_merge($allParams, $valuesArray);
        $prefix = empty($queryConditions) ? ' WHERE ' : ' AND ';
        $queryConditions .= $prefix . $condition;
      }
    }

    $query['params'] = $allParams;
    $query['statement'] = $queryConditions;

    return $query;
  }
  /**
   * TODO: comment
   *
   * @param string $columns - .
   *
   * @return array - .
   */
  function createRefArray($array) {
    $refArray = [];

    foreach($array as $key => $value) {
      $refArray[$key] = &$array[$key];
    }

    return $refArray;
  }

  /**
   * TODO: comment
   *
   * @param string $columns - .
   *
   * @return array - .
   */
  function getBindParamRefs($params) {
    $types;

    // TODO: type error
    if (!is_array($params)) {
      echo('type error');
    }

    $types = array(str_repeat("s", count($params)));

    return createRefArray(array_merge($types, $params));
  }

  /** @var object database connection. */
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

  /** @var string database table. */
  $buildingsTable = "filter";

  /** @var string database query to retrieve data about applicable buildings. */
  $query = "SELECT * FROM $buildingsTable";

  /** @var string contains all query conditions, including SQL 'WHERE' command. */
  $queryConditions = "";

  /** @var array contains response data with each array key corresponding to a building option. */
  $responseData = new BuildingsFilterResponse();

  if (mysqli_connect_errno()) {
    $responseData = "Database connection failed.";
  } else {
    $conditions = getQueryConditions($_GET, $columns);
    $queryParams = $conditions['params'];
    $queryConditions = $conditions['statement'];
    $query .= $queryConditions;
    $stmt = $mysqli->prepare($query);

    if ($stmt) {
      call_user_func_array(array($stmt, 'bind_param'), getBindParamRefs($queryParams));

      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($id, $building_name, $building_type, $building_size, $plumbing, $restroom_type, $num_restroom, $num_toilet, $num_toilet_ada, $num_shower, $num_shower_ada, $num_sink);

      while($stmt->fetch()) {
        $responseData->update('building-name', $building_name);
        $responseData->update('building-type', $building_type);
        $responseData->update('building-size', $building_size);
        $responseData->update('plumbing', $plumbing);
        $responseData->update('restroom-type', $restroom_type);
        $responseData->update('num-restroom', $num_restroom);
        $responseData->update('num-toilet', $num_toilet);
        $responseData->update('num-toilet-ada', $num_toilet_ada);
        $responseData->update('num-shower', $num_shower);
        $responseData->update('num-shower-ada', $num_shower_ada);
        $responseData->update('num-sink', $num_sink);
      }

      $stmt->free_result();
      $stmt->close();
    }
  }

  $mysqli->close();

  if ($ajax && empty($_POST)) {
    header('Content-Type: application/json');
    echo json_encode($responseData->getDataArray());
  }
?>