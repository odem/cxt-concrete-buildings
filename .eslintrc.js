module.exports = {
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  extends: 'google',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
  },
  rules: {
    'arrow-parens': ['warn', 'as-needed'],
    indent: ['error', 2, { MemberExpression: 1 }],
    'no-mixed-spaces-and-tabs': 'error',
    'no-tabs': 'error',
    'no-use-before-define': 'warn',
    'no-var': 'warn',
    'object-curly-spacing': ['error', 'always'],
    'array-bracket-spacing': ['error', 'never'],
    'one-var': ['warn', 'always'],
  },
  overrides: [
    {
      files: ['dist/js/*'],
      rules: {
        'no-var': 'off',
        'one-var': ['off', 'never'],
      },
    },
  ],
};
