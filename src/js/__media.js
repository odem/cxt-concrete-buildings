const media = (function(document, library) {
  const mediaArgs = {
      class: {
        container: 'js-media',
        btn: 'js-media-btn',
        thumbnail: 'js-visualMedia-img',
      },
      containerAttr: { image: 'data-image' },
      btnAttr: { host: 'data-host', mediaId: 'data-media' },
    },
    isContainerValid = node => {
      return (
        library.isElementNode(node) &&
        library.elementHasReqAttrs(node, mediaArgs.containerAttr)
      );
    },
    isBtnValid = btn => {
      return (
        library.isValidBtn(btn) &&
        library.elementHasReqAttrs(btn, mediaArgs.btnAttr)
      );
    },
    isHostValid = host => {
      return library.isString(host);
    },
    isIdValid = id => {
      return library.isString(id);
    },
    getSketchfabSrc = id => {
      if (isIdValid(id)) {
        return 'https://sketchfab.com/models/' + id + '/embed?autostart=1';
      }

      return '';
    },
    getVimeoSrc = id => {
      if (isIdValid(id)) {
        return (
          'https://player.vimeo.com/video/' +
          id +
          '?autoplay=1&color=ffffff&title=0&byline=0&portrait=0'
        );
      }

      return '';
    },
    getYoutubeSrc = id => {
      if (isIdValid(id)) {
        return (
          'https://www.youtube-nocookie.com/embed/' +
          id +
          '?rel=0&amp;showinfo=0&amp;autoplay=1&mute=1'
        );
      }

      return '';
    },
    getYoutubeImgSrc = id => {
      if (isIdValid(id)) {
        return 'https://img.youtube.com/vi/' + id + '/sddefault.jpg';
      }

      return '';
    },
    getMediaThumbnail = container => {
      return library.getFirstElement(mediaArgs.class.thumbnail, container);
    },
    hideMediaThumbnail = container => {
      library.hideElement(getMediaThumbnail(container));
    },
    loadInteractiveIframe = (container, host, id) => {
      let iframe = null;

      if (
        isContainerValid(container) &&
        isHostValid(host) &&
        isIdValid(id) &&
        host.toLowerCase() === 'sketchfab'
      ) {
        iframe = document.createElement('iframe');
        iframe.classList.add('VisualMedia-iframe');
        iframe.classList.add('js-media-iframe');
        iframe.setAttribute('id', id);
        iframe.setAttribute('src', '');
        iframe.setAttribute('aria-hidden', 'true');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allow', 'autoplay; fullscreen; vr');
        iframe.setAttribute('allowvr', 'true');
        iframe.setAttribute('allowfullscreen', 'true');
        iframe.setAttribute('webkitallowfullscreen', 'true');
        iframe.setAttribute('mozallowfullscreen', 'true');
        iframe.innerHTML = '';
        container.appendChild(iframe);
      }
    },
    loadMedia = args => {
      const { container, btn, host, id } = args;
      let iframe = null,
        iframeSrc = '',
        hostName = '';

      if (
        isContainerValid(container) &&
        isBtnValid(btn) &&
        isHostValid(host) &&
        isIdValid(id)
      ) {
        hostName = host.toLowerCase();

        if (hostName === 'vimeo') {
          iframeSrc = getVimeoSrc(id);
        } else if (hostName === 'youtube') {
          iframeSrc = getYoutubeSrc(id);
        } else if (hostName === 'sketchfab') {
          iframeSrc = getSketchfabSrc(id);
        } else {
          throw new RangeError(
            'cannot lazyload media due to invalid media host: ' + hostName
          );
        }

        library.disableBtn(btn);
        library.hideElement(getMediaThumbnail(container));
        iframe = document.createElement('iframe');
        iframe.setAttribute('src', iframeSrc);
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        iframe.classList.add('VisualMedia-iframe');
        iframe.classList.add('js-media-iframe');
        iframe.innerHTML = '';
        container.appendChild(iframe);
      }
    },
    loadThumbnail = args => {
      const { container, host, id } = args;
      let isImgSet = 'false',
        img = null;

      if (
        isHostValid(host) &&
        host.toLowerCase() === 'youtube' &&
        isContainerValid(container) &&
        isIdValid(id)
      ) {
        isImgSet = container
          .getAttribute(mediaArgs.containerAttr.image)
          .toLowerCase();

        if (isImgSet === 'false') {
          img = new Image();
          img.src = getYoutubeImgSrc(id);
          img.classList.add('VisualMedia-img');
          img.classList.add('js-visualMedia-img');

          if (library.isElementNode(img) && document.addEventListener) {
            img.addEventListener('load', function() {
              container.appendChild(img);
            });
          }
        }
      }
    },
    lazyLoad = () => {
      const btns = library.getElements(mediaArgs.class.btn);
      let btn = null,
        container = null,
        host = '',
        id = '';

      if (library.isArrayLike(btns)) {
        for (let i = 0, n = btns.length; i < n; i++) {
          btn = btns[i];

          if (isBtnValid(btn)) {
            host = btn.getAttribute(mediaArgs.btnAttr.host);
            id = btn.getAttribute(mediaArgs.btnAttr.mediaId);
            container =
              library.getFirstElement(
                mediaArgs.class.container,
                btn.parentNode
              ) || btn.parentNode;

            if (host.toLowerCase() === 'youtube') {
              loadThumbnail({ container, host, id });
            }

            if (library.isElementNode(btn) && document.addEventListener) {
              btn.addEventListener(
                'click',
                loadMedia.bind(null, { container, btn, host, id }),
                false
              );
            }
          }
        }
      }
    };
  return {
    lazyLoad,
    loadInteractiveIframe,
    hideMediaThumbnail,
  };
})(document, library);
