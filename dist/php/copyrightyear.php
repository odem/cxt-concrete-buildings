<?php
    function copyrightyear() {
        $data = [
            "value" => null,
            "valid" => false,
        ];

        $copyright = '';


        function valid_data($data) {
            return is_int($data) || (is_string($data) && ($data === '' || intval($data)));
        }

        function validate_data(&$data) {
            if (isset($_POST["initialyear"])) {
                $raw_data = json_decode($_POST["initialyear"], true);
                $processed_data = htmlspecialchars(trim($raw_data));

                if (valid_data($processed_data)) {
                    $data["value"] = $processed_data;
                    $data["valid"] = true;
                } else {
                    $data["value"] = null;
                    $data["valid"] = false;
                }
            }
        }

        function auto_copyright($initial_year) {
            if (valid_data($initial_year)) {
                $curr_year = intval(date('Y'));
                $init_year = (int)$initial_year ? (int)$initial_year : $curr_year;

                if ($init_year < $curr_year) {
                    $copyright_year = $init_year . '-' . $curr_year;
                } else {
                    $copyright_year = $curr_year;
                }
            }

            return (string)$copyright_year;
        }

        // Program starts
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            validate_data($data);

            if ($data["valid"]) {
                $copyright = auto_copyright($data["value"]);
            }

            // Only sends a response if an ajax connection
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                header('Content-Type: application/json');
                $response_data = ["copyright" => $copyright];
                echo json_encode($response_data);
            }
        }
    }

    copyrightyear();
?>