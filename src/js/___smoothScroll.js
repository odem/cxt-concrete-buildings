const smoothScroll = (function(window, library) {
  const location = () => {
      return window.scrollY || window.pageYOffset;
    },
    top = (element, startPosition) => {
      if (library.isElementNode(element) && library.isNumber(startPosition)) {
        return element.getBoundingClientRect().top + startPosition;
      }

      return 0;
    },
    // Robert Penner's easeInOutQuad - http://robertpenner.com/easing/
    easeInOutQuad = (currTime, begValue, valueChange, duration) => {
      if (
        !library.isNumber(currTime) ||
        !library.isNumber(begValue) ||
        !library.isNumber(valueChange) ||
        !library.isNumber(duration)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to ease smooth scroll.'
        );
      }

      currTime /= duration / 2;

      if (currTime < 1) {
        return (valueChange / 2) * currTime * currTime + begValue;
      }

      currTime--;

      return (-valueChange / 2) * (currTime * (currTime - 2) - 1) + begValue;
    },
    end = (startPosition, scrollDistance, callback) => {
      if (
        !library.isNumber(startPosition) ||
        !library.isNumber(scrollDistance)
      ) {
        library.errorMsg().argsEvaluationFail + ' to finish smooth scroll.';
      }

      window.scrollTo(0, startPosition + scrollDistance);

      if (library.isFunction(callback)) {
        callback();
      }
    },
    loop = args => {
      const {
        currTime,
        startPosition,
        scrollDistance,
        duration,
        callback,
        easing,
      } = args;
      let startTime = args.startTime,
        timeElapsed = 0,
        nextScrollPosition = 0;

      if (
        !library.isNumber(currTime) ||
        !library.isNumber(startPosition) ||
        !library.isNumber(scrollDistance) ||
        !library.isNumber(duration) ||
        !library.isFunction(easing) ||
        (startTime && !library.isPositiveNumber(startTime))
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to smooth scroll loop.'
        );
      }

      if (!startTime) {
        startTime = currTime;
      }

      // determine time spent scrolling so far
      timeElapsed = currTime - startTime;
      nextScrollPosition = easing(
        timeElapsed,
        startPosition,
        scrollDistance,
        duration
      );

      window.scrollTo(0, nextScrollPosition);

      timeElapsed < duration
        ? window.requestAnimationFrame(function(currTime) {
            loop({
              currTime,
              startTime,
              startPosition,
              scrollDistance,
              duration,
              callback,
              easing,
            });
          })
        : end(startPosition, scrollDistance, callback);
    },
    jump = (target, options) => {
      let jumpOpt = {},
        scrollDistance = 0,
        startPosition = 0,
        stopPosition = 0,
        duration = 0,
        startTime = undefined;

      if (
        !library.isElementNode(target) ||
        (options && !library.isObject(options))
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to smooth scroll jump.'
        );
      }

      jumpOpt = {
        duration: options.duration || 1000,
        positionOffset: options.offset || 0,
        callback: options.callback,
        easing: options.easing || easeInOutQuad,
      };

      startPosition = location();
      stopPosition = top(target, startPosition);
      scrollDistance = stopPosition - startPosition + jumpOpt.positionOffset;
      duration = library.isFunction(jumpOpt.duration)
        ? jumpOpt.duration(scrollDistance)
        : jumpOpt.duration;

      // start the loop
      window.requestAnimationFrame(function(currTime) {
        loop({
          currTime,
          startTime,
          startPosition,
          scrollDistance,
          duration,
          callback: jumpOpt.callback,
          easing: jumpOpt.easing,
        });
      });
    };
  return {
    jump,
  };
})(window, library);
