/** Imports ================================================================= */
const gulp = require('gulp'),
  // Handlebars imports
  handlebars = require('gulp-compile-handlebars'),
  // Handlebars data
  navSiteData = require('./src/data/nav-site.json'),
  footerSiteData = require('./src/data/footer-site.json'),
  homeData = require('./src/data/home.json'),
  contactData = require('./src/data/contact.json'),
  installData = require('./src/data/install.json'),
  maintenanceData = require('./src/data/maintenance.json'),
  productsData = require('./src/data/products.json'),
  texturesColorsData = require('./src/data/textures-colors.json'),
  colorsData = require('./src/data/colors.json'),
  texturesData = require('./src/data/textures.json'),
  buildingsData = require('./src/data/buildings.json'),
  buildingsConcessionData = require('./src/data/buildings-concession.json'),
  buildingsFlushData = require('./src/data/buildings-flush.json'),
  buildingsUtilityData = require('./src/data/buildings-utility.json'),
  buildingsShowerData = require('./src/data/buildings-shower.json'),
  buildingsVaultData = require('./src/data/buildings-vault.json'),
  buildingArapahoeData = require('./src/data/building-arapahoe.json'),
  buildingCascadianData = require('./src/data/building-cascadian.json'),
  buildingCascadianDoubleData = require('./src/data/building-cascadian-double.json'),
  buildingCheyenneData = require('./src/data/building-cheyenne.json'),
  buildingCortezData = require('./src/data/building-cortez.json'),
  buildingDakotaData = require('./src/data/building-dakota.json'),
  buildingDenaliData = require('./src/data/building-denali.json'),
  buildingDiabloData = require('./src/data/building-diablo.json'),
  buildingFontanaData = require('./src/data/building-fontana.json'),
  buildingGunnisonData = require('./src/data/building-gunnison.json'),
  buildingKodiakData = require('./src/data/building-kodiak.json'),
  buildingMalibuData = require('./src/data/building-malibu.json'),
  buildingMendocinoData = require('./src/data/building-mendocino.json'),
  buildingMontroseData = require('./src/data/building-montrose.json'),
  buildingNavajoData = require('./src/data/building-navajo.json'),
  buildingOzarkIData = require('./src/data/building-ozarkI.json'),
  buildingOzarkIIData = require('./src/data/building-ozarkII.json'),
  buildingPioneerData = require('./src/data/building-pioneer.json'),
  buildingPomonaData = require('./src/data/building-pomona.json'),
  buildingRainierData = require('./src/data/building-rainier.json'),
  buildingRockyMtnData = require('./src/data/building-rocky-mtn.json'),
  buildingRockyMtnDoubleData = require('./src/data/building-rocky-mtn-double.json'),
  buildingSchweitzerIData = require('./src/data/building-schweitzerI.json'),
  buildingSchweitzerIIData = require('./src/data/building-schweitzerII.json'),
  buildingTaosData = require('./src/data/building-taos.json'),
  buildingTetonData = require('./src/data/building-teton.json'),
  buildingTiogaData = require('./src/data/building-tioga.json'),
  extremeWeatherData = require('./src/data/extreme-weather.json'),
  resourcesBuildingsData = require('./src/data/resources-buildings.json'),
  // SASS & CSS
  sass = require('gulp-sass'),
  minifyCSS = require('gulp-clean-css'),
  // JS
  babel = require('gulp-babel'),
  minifyJS = require('gulp-uglify'),
  // Server
  nodemon = require('gulp-nodemon'),
  // File manipulation
  del = require('del'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  /** Configuration ========================================================= */
  baseDirSrc = 'src',
  baseDirDest = 'dist',
  basePartialsSrc = baseDirSrc + '/pages/views',
  jsSrcOrder = [
    baseDirSrc + '/js/_____*.js',
    baseDirSrc + '/js/____*.js',
    baseDirSrc + '/js/___*.js',
    baseDirSrc + '/js/__*.js',
    baseDirSrc + '/js/_*.js',
    baseDirSrc + '/js/main.js',
  ],
  config = {
    srcPaths: {
      entry: baseDirSrc + '/js/main.js',
      html: baseDirSrc + '/*.html',
      css: baseDirSrc + '/scss/*.scss',
      scssLibrary: baseDirSrc + '/scss/**/*.scss',
      scssProject: baseDirSrc + '/pages/**/**/*.scss',
      js: baseDirSrc + '/js/**/*.js',
      hbs: baseDirSrc + '/pages/**/**/*.hbs',
      templateData: baseDirSrc + '/data/*.json',
      partialTemplates: [
        basePartialsSrc,
        basePartialsSrc + '/doc',
        basePartialsSrc + '/Filter',
        basePartialsSrc + '/FooterSite',
        basePartialsSrc + '/ImgCarousel',
        basePartialsSrc + '/NavSite',
        basePartialsSrc + '/Page',
        basePartialsSrc + '/PdfLink',
        basePartialsSrc + '/Sort',
        basePartialsSrc + '/Sxn',
        basePartialsSrc + '/VideoCard',
        basePartialsSrc + '/VisualMedia',
        baseDirSrc + '/pages/views-home',
        baseDirSrc + '/pages/contact/views',
        baseDirSrc + '/pages/installation/views',
        baseDirSrc + '/pages/products/views',
        baseDirSrc + '/pages/products/buildings/views',
      ],
    },
    srcFiles: {
      templateData: {
        navSite: navSiteData,
        footerSite: footerSiteData,
        home: homeData,
        contact: contactData,
        install: installData,
        maintenance: maintenanceData,
        products: productsData,
        texturesColors: texturesColorsData,
        colors: colorsData,
        textures: texturesData,
        buildings: buildingsData,
        buildingsConcession: buildingsConcessionData,
        buildingsFlush: buildingsFlushData,
        buildingsUtility: buildingsUtilityData,
        buildingsShower: buildingsShowerData,
        buildingsVault: buildingsVaultData,
        buildingArapahoe: buildingArapahoeData,
        buildingCascadian: buildingCascadianData,
        buildingCascadianDouble: buildingCascadianDoubleData,
        buildingCheyenne: buildingCheyenneData,
        buildingCortez: buildingCortezData,
        buildingDakota: buildingDakotaData,
        buildingDenali: buildingDenaliData,
        buildingDiablo: buildingDiabloData,
        buildingFontana: buildingFontanaData,
        buildingGunnison: buildingGunnisonData,
        buildingKodiak: buildingKodiakData,
        buildingMalibu: buildingMalibuData,
        buildingMendocino: buildingMendocinoData,
        buildingMontrose: buildingMontroseData,
        buildingNavajo: buildingNavajoData,
        buildingOzarkI: buildingOzarkIData,
        buildingOzarkII: buildingOzarkIIData,
        buildingPioneer: buildingPioneerData,
        buildingPomona: buildingPomonaData,
        buildingRainier: buildingRainierData,
        buildingRockyMtn: buildingRockyMtnData,
        buildingRockyMtnDouble: buildingRockyMtnDoubleData,
        buildingSchweitzerI: buildingSchweitzerIData,
        buildingSchweitzerII: buildingSchweitzerIIData,
        buildingTaos: buildingTaosData,
        buildingTeton: buildingTetonData,
        buildingTioga: buildingTiogaData,
        extremeWeather: extremeWeatherData,
        resourcesBuildings: resourcesBuildingsData,
      },
    },
  };

/** Tasks ============================================================== */
// Clears dist folder.
gulp.task('clean', () => {
  return del([
    baseDirDest + '/**/**/*.html',
    baseDirDest + '/**/*.html',
    baseDirDest + '/*.html',
    baseDirDest + '/css/*.css',
    baseDirDest + '/js/main*.js',
  ]);
});

gulp.task('clean-hbs-views', () => {
  return del([
    baseDirDest + '/views',
    baseDirDest + '/views-home',
    baseDirDest + '**/views',
    baseDirDest + '/**/**/views',
  ]);
});

// Compiles Handlebar templates.
gulp.task('hbs-build', () => {
  const allData = config.srcFiles.templateData,
    options = { batch: config.srcPaths.partialTemplates };
  return gulp
    .src(config.srcPaths.hbs)
    .pipe(handlebars(allData, options))
    .pipe(rename({ extname: '.html' }))
    .pipe(gulp.dest(baseDirDest));
});

// Outputs a warning to the console about template data changes.
gulp.task('template-data', () => {
  console.log(
    'WARNING: Template data changed must restart gulp to see changes.'
  );
});

// Compiles Handlebar templates to HTML,
// and removes view files from dist folder.
gulp.task('html', gulp.series('hbs-build', 'clean-hbs-views'));

// Compiles Sass to CSS and minifies CSS.
gulp.task('css', () => {
  return gulp
    .src(config.srcPaths.css)
    .pipe(sass())
    .pipe(gulp.dest(baseDirDest + '/css'))
    .pipe(rename({ suffix: '_min' }))
    .pipe(minifyCSS())
    .pipe(gulp.dest(baseDirDest + '/css'));
});

// Bundles and minifies JS.
gulp.task('js', function() {
  return gulp
    .src(jsSrcOrder)
    .pipe(concat('main.js'))
    .pipe(babel({ presets: ['@babel/preset-env'] }))
    .pipe(gulp.dest(baseDirDest + '/js'))
    .pipe(rename({ suffix: '_min' }))
    .pipe(minifyJS())
    .pipe(gulp.dest(baseDirDest + '/js'));
});

// Watches for file changes.
gulp.task('watch', () => {
  gulp.watch(config.srcPaths.js, gulp.series('js'));
  gulp.watch(config.srcPaths.scssLibrary, gulp.series('css'));
  gulp.watch(config.srcPaths.scssProject, gulp.series('css'));
  gulp.watch(config.srcPaths.css, gulp.series('css'));
  gulp.watch(config.srcPaths.hbs, gulp.series('html'));
  gulp.watch(config.srcPaths.templateData, gulp.series('template-data'));
});

// Runs Server.
gulp.task('server', done => {
  nodemon({
    script: baseDirSrc + '/server.js',
    watch: [baseDirDest],
    done: done,
  }).on('start', gulp.series('watch'));
});

// Default task that bundles entire site and host on local server.
gulp.task('default', gulp.series('clean', 'html', 'css', 'js', 'server'));

// Serverless task bundles entire site.
gulp.task('serverless', gulp.series('clean', 'html', 'css', 'js'));
