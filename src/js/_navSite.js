const navSite = (function(window, document, library, dropdown) {
  const navSiteDropdown = dropdown(),
    args = {
      breakpoint: 950,
      class: {
        navSiteNode: 'js-navSite',
        btn: 'js-navSite-btn',
        btnDisplay: 'js-navSite-btnHamburger',
        menu: 'js-navSite-menu',
        pageContent: 'js-page-content',
      },
    },
    getArgs = () => {
      return {
        breakpoint: args.breakpoint,
        btn: library.getFirstElement(args.class.btn),
        btnDisplay: library.getFirstElement(args.class.btnDisplay),
        menu: library.getFirstElement(args.class.menu),
        pageOverlay: library.getFirstElement(args.class.pageContent),
        node: library.getFirstElement(args.class.navSiteNode),
      };
    },
    isReqArgsValid = args => {
      return (
        library.isObject(args) &&
        library.isPositiveNumber(args.breakpoint) &&
        navSiteDropdown.isReqArgsValid(args)
      );
    },
    hasValidOverlayArg = overlay => {
      return library.isElementNode(overlay);
    },
    setView = () => {
      const args = getArgs();

      if (isReqArgsValid(args)) {
        args.btn.setAttribute(
          'aria-hidden',
          window.innerWidth >= args.breakpoint ? 'true' : 'false'
        );

        if (navSiteDropdown.hasValidBtnDisplayArg(args)) {
          args.btnDisplay.setAttribute(
            'data-menu',
            library.isBtnPressed(args.btn) ? 'open' : 'close'
          );
        }

        if (window.innerWidth >= args.breakpoint) {
          args.menu.setAttribute('aria-hidden', 'false');

          if (hasValidOverlayArg(args.pageOverlay)) {
            args.pageOverlay.setAttribute('data-overlay', 'false');
          }
        } else {
          args.menu.setAttribute(
            'aria-hidden',
            String(!library.isBtnPressed(args.btn))
          );

          if (hasValidOverlayArg(args.pageOverlay)) {
            args.pageOverlay.setAttribute(
              'data-overlay',
              String(library.isBtnPressed(args.btn))
            );
          }
        }

        return true;
      } else {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' for site nav initial display.'
        );
      }
    },
    closePageOverlay = evt => {
      const { node, pageOverlay } = getArgs(),
        isClickOutside = !node.contains(evt.target);

      if (isClickOutside && navSiteDropdown.areAllDropdownsClosed()) {
        pageOverlay.setAttribute('data-overlay', 'false');
        document.removeEventListener('click', closePageOverlay, false);
      }
    },
    togglePageOverlay = () => {
      const { btn, pageOverlay } = getArgs(),
        isBtnPressed = library.isBtnPressed(btn);

      if (hasValidOverlayArg(pageOverlay)) {
        pageOverlay.setAttribute('data-overlay', String(isBtnPressed));

        if (isBtnPressed) {
          document.addEventListener('click', closePageOverlay, false);
        }

        return true;
      } else {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' for site nav page overlay.'
        );
      }
    },
    addDropdownBtnEvents = args => {
      if (navSiteDropdown.isReqArgsValid(args) && document.addEventListener) {
        args.btn.addEventListener(
          'click',
          navSiteDropdown.toggle.bind(null, args),
          false
        );

        args.submenus.forEach(submenu => {
          if (navSiteDropdown.isReqArgsValid(submenu)) {
            addDropdownBtnEvents(submenu);
          }
        });
      }
    },
    makeResponsive = () => {
      const args = getArgs();
      let dropdowns = {};

      if (isReqArgsValid(args)) {
        dropdowns = navSiteDropdown.getDropdownStructure(args.node);
        setView();

        if (dropdowns) {
          addDropdownBtnEvents(dropdowns);
        }

        if (hasValidOverlayArg(args.pageOverlay)) {
          args.btn.addEventListener('click', togglePageOverlay, false);
        }

        if (window.addEventListener) {
          window.addEventListener('resize', setView, false);
          window.addEventListener('orientationchange', setView, false);
        }

        return true;
      } else {
        throw new TypeError('Site nav could not be made responsive.');
      }
    };
  return {
    makeResponsive,
  };
})(window, document, library, dropdown);
