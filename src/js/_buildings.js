const buildings = (function(document, library, sort, linkedList) {
  const buildingsArgs = {
      class: {
        sortBtns: 'js-buildings-sort-btns',
        sortItems: 'js-buildings-sort-items',
        filter: 'js-Buildings-filter',
        filterBtn: 'js-buildings-filterbtn',
        filterOptions: 'js-buildings-filterOptions',
        filterCategory: 'js-buildings-filterCategory',
        filterCategoryBtn: 'js-buildings-filterCategoryBtn',
        filterCategoryMenu: 'js-buildings-filterCategoryMenu',
        filterCategoryInput: 'js-buildings-filterCategoryInput',
        filterCategoryInputQuantity: 'js-buildings-filterCategoryInputQuantity',
        buildings: 'js-buildings-item',
        sxnContent: 'js-Buildings-pagecontent',
        pageContent: 'js-page-content',
        warning: 'js-warning',
      },
      filterAttr: { page: 'data-page' },
      buildingAttr: { name: 'data-itemName' },
      sortBtnAttr: { order: 'data-order' },
      categoryMenuAttr: { hidden: 'aria-hidden' },
      optionAttr: {
        value: 'value',
        category: 'data-category',
      },
      categoryAttr: { category: 'data-category' },
      warningMsg: {
        db: 'Database connection issue.',
        network: 'Network connection issue.',
        filter:
          'Buildings cannot be filtered at this time. Please try again later.',
      },
      filterServerFile: 'buildingsFilter.php',
    },
    selectedFilterOptions = {},
    getSortBtnsContainer = () => {
      return library.getFirstElement(buildingsArgs.class.sortBtns);
    },
    getfilter = node => {
      if (library.isElementNode(node) || library.isDocumentNode(node)) {
        return node.getElementsByClassName('js-buildings-filter')[0];
      }

      return null;
    },
    getFilterInputs = node => {
      if (library.isElementNode(node) || library.isDocumentNode(node)) {
        return node.getElementsByClassName('js-buildings-filterCategoryInput');
      }

      return null;
    },
    isValidFilter = filter => {
      return (
        library.isElementNode(filter) &&
        library.elementHasReqAttrs(filter, buildingsArgs.filterAttr)
      );
    },
    isValidBuilding = building => {
      return (
        library.isElementNode(building) &&
        library.elementHasReqAttrs(building, buildingsArgs.buildingAttr)
      );
    },
    isCategoryMenuValid = menu => {
      return (
        library.isElementNode(menu) &&
        library.elementHasReqAttrs(menu, buildingsArgs.categoryMenuAttr)
      );
    },
    isOptionElementValid = node => {
      return (
        library.isElementNode(node) &&
        library.elementHasReqAttrs(node, buildingsArgs.optionAttr)
      );
    },
    isValidCategoryElement = element => {
      return (
        library.isElementNode(element) &&
        library.elementHasReqAttrs(element, buildingsArgs.categoryAttr)
      );
    },
    getFilterPage = filter => {
      return isValidFilter(filter)
        ? filter.getAttribute(buildingsArgs.filterAttr.page)
        : '';
    },
    getBuildingName = building => {
      return isValidBuilding(building)
        ? building.getAttribute(buildingsArgs.buildingAttr.name)
        : '';
    },
    getSortOrder = sortBtns => {
      let currBtn = null;

      if (library.isArrayLike(sortBtns)) {
        for (let i = 0; i < sortBtns.length; i++) {
          currBtn = sortBtns[i];

          if (
            sort.isBtnValid(currBtn) &&
            currBtn.getAttribute('aria-pressed') === 'true'
          ) {
            return currBtn.getAttribute(buildingsArgs.sortBtnAttr.order);
          }
        }
      }

      return '';
    },
    getOptionCategory = option => {
      if (isOptionElementValid(option) || isValidCategoryElement(option)) {
        return option.getAttribute(buildingsArgs.optionAttr.category);
      }

      return '';
    },
    getOptionValue = option => {
      if (isOptionElementValid(option)) {
        return option.getAttribute(buildingsArgs.optionAttr.value);
      }

      return '';
    },
    toggleFilterCategory = (btn, menu) => {
      if (library.isValidBtn(btn)) {
        library.toggleDataAttribute({
          element: btn,
          attribute: 'aria-pressed',
          state1: 'true',
          state2: 'false',
        });
      }

      if (isCategoryMenuValid(menu)) {
        library.toggleDataAttribute({
          element: menu,
          attribute: 'aria-hidden',
          state1: 'true',
          state2: 'false',
        });
      }
    },
    isFilterOptionSelected = option => {
      return library.isElementNode(option) && option.hasAttribute('checked');
    },
    toggleFilterOption = (input, inputContainer) => {
      if (
        !library.isElementNode(input) ||
        !library.isElementNode(inputContainer)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to toggle filter option: ' +
            input
        );
      }

      if (isFilterOptionSelected(input)) {
        input.removeAttribute('checked');
      } else {
        input.setAttribute('checked', 'true');
      }

      if (library.isElementNode(inputContainer)) {
        library.toggleDataAttribute({
          element: inputContainer,
          attribute: 'aria-pressed',
          state1: 'true',
          state2: 'false',
        });
      }
    },
    toggleFilter = (btn, menu) => {
      if (!library.isValidBtn(btn)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to toggle filter associated with filter button: ' +
            btn
        );
      }

      library.toggleBtn(btn);

      library.isBtnPressed(btn)
        ? library.showElement(menu)
        : library.hideElement(menu);
    },
    filterBuildings = displayBuildings => {
      let numDisplayBuildings = 0,
        currDisplayBuildingName = '',
        displayIndex = 0,
        allBuildings = null,
        currBuilding = null,
        currBuildingName = '',
        sortOrder = '';

      if (!library.isArrayLike(displayBuildings)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to filter buildings.'
        );
      }

      numDisplayBuildings = displayBuildings.length;

      if (numDisplayBuildings) {
        sortOrder = getSortOrder(sort.getBtns(getSortBtnsContainer()));

        if (sortOrder === 'ascending') {
          displayBuildings.sort();
        }

        if (sortOrder === 'descending') {
          displayBuildings.sort(function(a, b) {
            if (a < b) {
              return 1;
            } else if (a > b) {
              return -1;
            } else {
              return 0;
            }
          });
        }

        allBuildings = library.getElements(
          buildingsArgs.class.buildings,
          document
        );

        for (let i = 0; i < allBuildings.length; i++) {
          currBuilding = allBuildings[i];
          currBuildingName = getBuildingName(currBuilding);

          if (displayIndex < numDisplayBuildings) {
            currDisplayBuildingName = displayBuildings[displayIndex];
          } else {
            currDisplayBuildingName = '';
          }

          if (
            currBuildingName &&
            currDisplayBuildingName &&
            currBuildingName === currDisplayBuildingName
          ) {
            library.showElement(currBuilding);
            displayIndex++;
          }

          if (currBuildingName && currBuildingName != currDisplayBuildingName) {
            library.hideElement(currBuilding);
          }
        }
      }
    },
    addFilterOption = (category, value) => {
      let list = null;

      if (!library.isString(category) || !library.isString(value)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to add filter option: ' +
            category
        );
      }

      if (library.isObject(selectedFilterOptions)) {
        if (!library.objectHasOwnProperty(selectedFilterOptions, category)) {
          selectedFilterOptions[category] = linkedList.create('singlelinked');
        }

        list = selectedFilterOptions[category];

        if (list && library.objectHasOwnProperty(list, 'addFirst')) {
          list.addFirst(value);
        }
      }
    },
    removeFilterOption = (category, value) => {
      let list = null;

      if (!library.isString(category) || !library.isString(value)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to remove filter option: ' +
            category
        );
      }

      if (library.isObject(selectedFilterOptions)) {
        list = selectedFilterOptions[category];

        if (list && library.objectHasOwnProperty(list, 'removeNode')) {
          list.removeNode(value);
        }
      }
    },
    updatePageFilterOptions = pagename => {
      let addOptions = null,
        option = null;
      const options = {
        buildingsconcession: [{ 'building-type': 'concession' }],
        buildingsflush: [{ 'building-type': 'restroom' }, { plumbing: 'yes' }],
        buildingsshower: [{ 'building-type': 'restroom-shower' }],
        buildingsutility: [{ 'building-type': 'utility' }],
        buildingsvault: [{ 'building-type': 'restroom' }, { plumbing: 'no' }],
      };

      if (!library.isString(pagename)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to update filter options.'
        );
      }

      pagename = pagename.toLowerCase();

      if (library.objectHasOwnProperty(options, pagename)) {
        addOptions = options[pagename];

        for (let i = 0; i < addOptions.length; i++) {
          option = addOptions[i];

          for (const category in option) {
            if (library.objectHasOwnProperty(option, category)) {
              addFilterOption(category, option[category]);
            }
          }
        }
      }
    },
    modifyFilterOptionsDisplay = (categories, applicableOptions) => {
      let applicableValues = null,
        categoryElement = null,
        allCategoryOptions = null,
        option = null,
        optionValue = '',
        optionValueNumeric = NaN,
        optionQuantity = null,
        isOptionApplicable = false,
        numAssociatedBuildings = 0;

      if (
        !library.isArrayLike(categories) ||
        !library.isObject(applicableOptions)
      ) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail +
            ' to modify filter options display.'
        );
      }

      for (let i = 0; i < categories.length; i++) {
        categoryElement = categories[i];

        if (isValidCategoryElement(categoryElement)) {
          allCategoryOptions = getFilterInputs(categoryElement);
          applicableValues =
            applicableOptions[getOptionCategory(categoryElement)];
        }

        if (library.isArrayLike(allCategoryOptions)) {
          for (let k = 0; k < allCategoryOptions.length; k++) {
            option = allCategoryOptions[k];

            if (isOptionElementValid(option)) {
              optionValue = getOptionValue(option);
              optionValueNumeric = Number(optionValue, 10);
              optionQuantity = library.getFirstElement(
                buildingsArgs.class.filterCategoryInputQuantity,
                option.parentNode
              );

              if (!isNaN(optionValueNumeric)) {
                optionValue = 'n' + optionValue;
              }

              isOptionApplicable = library.objectHasOwnProperty(
                applicableValues,
                optionValue
              );

              if (optionQuantity) {
                numAssociatedBuildings = isOptionApplicable
                  ? applicableValues[optionValue]
                  : 0;

                optionQuantity.innerHTML = '(' + numAssociatedBuildings + ')';
              }

              if (isOptionApplicable && library.isElementDisabled(option)) {
                library.enableElementNode(option);
                library.enableElementNode(option.parentNode);
              }

              if (!isOptionApplicable && !isFilterOptionSelected(option)) {
                library.disableElementNode(option);
                library.disableElementNode(option.parentNode);
              }
            }
          }
        }
      }
    },
    buildUrlParameters = () => {
      let optionsList = null,
        parameter = '',
        urlParameters = '';

      for (const category in selectedFilterOptions) {
        if (library.objectHasOwnProperty(selectedFilterOptions, category)) {
          optionsList = selectedFilterOptions[category];

          if (
            library.objectHasOwnProperty(optionsList, 'isEmpty') &&
            library.objectHasOwnProperty(optionsList, 'toString') &&
            !optionsList.isEmpty()
          ) {
            parameter = String(category) + '=' + optionsList.toString();
            urlParameters = library.isStringEmpty(urlParameters)
              ? parameter
              : urlParameters + '&' + parameter;
          }
        }
      }

      return urlParameters;
    },
    sendRequest = () => {
      const xhr = new XMLHttpRequest();
      let url = '',
        pagePathname = '',
        urlParameters = '';

      pagePathname = buildingsArgs.filterServerFile;
      urlParameters = buildUrlParameters();
      url = pagePathname + '?' + urlParameters;

      xhr.open('POST', url);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
      xhr.send();

      return xhr;
    },
    submitFilterOption = selectedOption => {
      let optionValue = '',
        optionCategory = '',
        updateFilterOptions = null,
        xhr = null,
        pageContent = null,
        sxnContent = null;

      if (selectedOption && !isOptionElementValid(selectedOption)) {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to submit filter option'
        );
      }

      pageContent = library.getFirstElement(
        buildingsArgs.class.pageContent,
        document
      );
      sxnContent = library.getFirstElement(
        buildingsArgs.class.sxnContent,
        document
      );
      library.enableElementOverlay(pageContent);
      library.enableElementOverlay(sxnContent);

      if (selectedOption) {
        optionCategory = getOptionCategory(selectedOption);
        optionValue = getOptionValue(selectedOption);
        updateFilterOptions = isFilterOptionSelected(selectedOption)
          ? removeFilterOption
          : addFilterOption;

        updateFilterOptions(optionCategory, optionValue);
      }

      xhr = sendRequest();

      if (addEventListener) {
        xhr.addEventListener(
          'loadend',
          function() {
            const filter = getfilter(document);
            let responseData = {},
              filterWarning = null;

            if (filter) {
              filterWarning = library.getFirstElement(
                buildingsArgs.class.warning,
                filter
              );
            }

            if (filterWarning) {
              filterWarning.innerHTML = '';
              library.hideElement(filterWarning);
            }

            if (xhr.status === 200) {
              responseData = JSON.parse(xhr.responseText);

              if (!library.isObjectEmpty(responseData)) {
                if (selectedOption) {
                  toggleFilterOption(selectedOption, selectedOption.parentNode);
                }

                modifyFilterOptionsDisplay(
                  library.getElements(
                    buildingsArgs.class.filterCategory,
                    document
                  ),
                  responseData
                );
                library.disableElementOverlay(pageContent);
                library.disableElementOverlay(sxnContent);
                filterBuildings(responseData.buildings);
              } else {
                library.disableElementOverlay(pageContent);
                library.disableElementOverlay(sxnContent);

                if (filterWarning) {
                  filterWarning.innerHTML =
                    buildingsArgs.warningMsg.db +
                    ' ' +
                    buildingsArgs.warningMsg.filter;
                  library.showElement(filterWarning);
                }

                throw new Error(library.errorMsg().serverResponseIssue);
              }
            } else {
              library.disableElementOverlay(pageContent);
              library.disableElementOverlay(sxnContent);

              if (filterWarning) {
                filterWarning.innerHTML =
                  buildingsArgs.warningMsg.network +
                  ' ' +
                  buildingsArgs.warningMsg.filter;
                library.showElement(filterWarning);
              }

              throw new Error(
                library.errorMsg().serverRequestFail +
                  ' server status: ' +
                  xhr.status
              );
            }
          },
          false
        );
      }
    },
    setFilter = () => {
      let page = null,
        filter = null,
        filterBtn = null,
        filterOptions = null,
        categoryBtns = null,
        btn = null,
        categoryInputs = null,
        input = null,
        menu = null;

      filter = getfilter(document);
      categoryBtns = library.getElements(
        buildingsArgs.class.filterCategoryBtn,
        document
      );
      categoryInputs = library.getElements(
        buildingsArgs.class.filterCategoryInput,
        document
      );

      if (isValidFilter(filter)) {
        filterBtn = library.getFirstElement(
          buildingsArgs.class.filterBtn,
          filter
        );
        filterOptions = library.getFirstElement(
          buildingsArgs.class.filterOptions,
          filter
        );
        page = getFilterPage(filter);
      }

      if (page) {
        updatePageFilterOptions(page);
      }

      if (filter) {
        submitFilterOption();
      }

      if (filter && addEventListener) {
        filter.addEventListener(
          'submit',
          function(evt) {
            const inputSelected = document.activeElement;
            evt.preventDefault();
            submitFilterOption(inputSelected);
          },
          false
        );
      }

      if (categoryInputs && addEventListener) {
        for (let i = 0; i < categoryInputs.length; i++) {
          input = categoryInputs[i];

          if (library.isElementNode(input)) {
            input.addEventListener(
              'click',
              submitFilterOption.bind(null, input),
              false
            );
          }
        }
      }

      if (filterBtn && addEventListener) {
        filterBtn.addEventListener(
          'click',
          toggleFilter.bind(null, filterBtn, filterOptions),
          false
        );
      }

      if (categoryBtns && addEventListener) {
        for (let i = 0; i < categoryBtns.length; i++) {
          btn = categoryBtns[i];

          if (library.isValidBtn(btn)) {
            menu = library.getFirstElement(
              buildingsArgs.class.filterCategoryMenu,
              btn.parentNode.parentNode
            );

            if (isCategoryMenuValid(menu)) {
              btn.addEventListener(
                'click',
                toggleFilterCategory.bind(null, btn, menu),
                false
              );
            }
          }
        }
      }
    },
    setSort = () => {
      let btns = null,
        btn = null,
        items = null;

      btns = sort.getBtns(getSortBtnsContainer());
      items = sort.getItems(
        library.getFirstElement(buildingsArgs.class.sortItems)
      );

      if (library.isArrayLike(btns) && library.isArrayLike(items)) {
        if (items.length) {
          sort.sortDOMItems({
            items,
            btns,
            selectedBtn: library.getSelectedBtn(btns, sort.isBtnSelected),
          });
        }

        for (let i = 0; i < btns.length; i++) {
          btn = btns[i];

          if (library.isElementNode(btn) && document.addEventListener) {
            btn.addEventListener(
              'click',
              sort.sortDOMItems.bind(null, {
                items,
                btns,
                selectedBtn: btn,
              }),
              false
            );
          }
        }
      }
    };
  return {
    setSort,
    setFilter,
  };
})(document, library, sort, linkedList);
