const dropdown = (function(document, library) {
  return function() {
    const dropdownArgs = {
        class: {
          dropdown: 'js-dropdown',
          btn: 'js-dropdown-btn',
          btnDisplay: 'js-dropdown-btnDisplay',
          menu: 'js-dropdown-menu',
        },
        reqProps: {
          node: 'node',
          btn: 'btn',
          menu: 'menu',
        },
        optProps: {
          btnDisplay: 'btnDisplay',
        },
        btnDisplayAttr: { display: 'data-menu' },
      },
      openDropdowns = [],
      getBtn = dropdown => {
        return library.getFirstElement(dropdownArgs.class.btn, dropdown);
      },
      getBtnDisplay = dropdown => {
        return library.getFirstElement(dropdownArgs.class.btnDisplay, dropdown);
      },
      getMenu = dropdown => {
        return library.getFirstElement(dropdownArgs.class.menu, dropdown);
      },
      getDropdowns = node => {
        return library.getElements(dropdownArgs.class.dropdown, node);
      },
      isOpenDropdownsArrayValid = () => {
        return Array.isArray(openDropdowns);
      },
      isOpenDropdownsEmpty = () => {
        return isOpenDropdownsArrayValid() && !openDropdowns.length;
      },
      isReqArgsValid = args => {
        return (
          library.isObject(args) &&
          library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) &&
          library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) &&
          library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) &&
          library.isValidBtn(args.btn) &&
          library.isElementNode(args.node) &&
          library.isElementNode(args.menu)
        );
      },
      hasValidBtnDisplayArg = args => {
        return (
          library.isObject(args) &&
          library.objectHasOwnProperty(
            args,
            dropdownArgs.optProps.btnDisplay
          ) &&
          library.isElementNode(args.btnDisplay)
        );
      },
      getDropdownStructure = dropdown => {
        const menu = getMenu(dropdown),
          getSubmenuArray = (submenus, menu) => {
            const submenuArray = [];

            let processSubmenus = [];

            processSubmenus = Array.isArray(submenus)
              ? submenus
              : library.nodelistToArray(submenus);

            if (processSubmenus) {
              processSubmenus.forEach(submenu => {
                if (submenu.parentNode === menu) {
                  submenuArray.push(getDropdownStructure(submenu));
                }
              });
            }

            return submenuArray;
          };

        let submenus = null,
          btn = null;

        if (menu) {
          submenus = getDropdowns(menu);
          btn = getBtn(dropdown);

          return {
            node: dropdown,
            menu: menu,
            btn: btn,
            btnDisplay: getBtnDisplay(btn),
            submenus: !submenus ? [] : getSubmenuArray(submenus, menu),
          };
        }
      },
      close = dropdown => {
        if (isReqArgsValid(dropdown)) {
          if (isOpenDropdownsArrayValid()) {
            library.unselectBtn(dropdown.btn);
            library.hideElement(dropdown.menu);

            if (hasValidBtnDisplayArg(dropdown)) {
              dropdown.btnDisplay.setAttribute(
                dropdownArgs.btnDisplayAttr.display,
                'close'
              );
            }

            if (!isOpenDropdownsEmpty()) {
              openDropdowns.pop();
            }

            if (isOpenDropdownsEmpty()) {
              document.removeEventListener('click', handleClickOutside, false);
            }

            return true;
          }
        } else {
          throw new TypeError(
            library.errorMsg().argsEvaluationFail +
              ' to close dropdown: ' +
              dropdown.node
          );
        }
      },
      open = dropdown => {
        if (isReqArgsValid(dropdown)) {
          if (isOpenDropdownsArrayValid()) {
            library.selectBtn(dropdown.btn);
            library.showElement(dropdown.menu);

            if (hasValidBtnDisplayArg(dropdown)) {
              dropdown.btnDisplay.setAttribute(
                dropdownArgs.btnDisplayAttr.display,
                'open'
              );
            }

            if (isOpenDropdownsEmpty()) {
              document.addEventListener('click', handleClickOutside, false);
            }

            openDropdowns.push(dropdown);

            return true;
          }
        } else {
          throw new TypeError(
            library.errorMsg().argsEvaluationFail +
              ' to open dropdown: ' +
              dropdown.node
          );
        }
      },
      closeDropdownsOutsideClick = (callback, args, evt) => {
        const clickedElement = evt.target;
        let lastOpenDropdown = {},
          isClickOutside = false;

        if (isOpenDropdownsArrayValid()) {
          if (!isOpenDropdownsEmpty()) {
            lastOpenDropdown = openDropdowns[openDropdowns.length - 1];
            isClickOutside = !lastOpenDropdown.node.contains(clickedElement);

            while (isClickOutside && !isOpenDropdownsEmpty()) {
              close(lastOpenDropdown);

              if (!isOpenDropdownsEmpty()) {
                lastOpenDropdown = openDropdowns[openDropdowns.length - 1];
                isClickOutside = !lastOpenDropdown.node.contains(
                  clickedElement
                );
              }
            }

            if (callback) {
              callback(args, evt);
            }
          }
        }
      },
      handleClickOutside = evt => {
        closeDropdownsOutsideClick(null, null, evt);
      },
      toggle = (dropdown, evt) => {
        if (isReqArgsValid(dropdown)) {
          closeDropdownsOutsideClick(null, null, evt);

          library.toggleDataAttribute({
            element: dropdown.btn,
            attribute: 'aria-pressed',
            state1: 'true',
            state2: 'false',
          });

          library.isBtnPressed(dropdown.btn) ? open(dropdown) : close(dropdown);
        } else {
          throw new TypeError(
            library.errorMsg().argsEvaluationFail +
              ' to toggle dropdown: ' +
              dropdown.node
          );
        }
      };
    return {
      areAllDropdownsClosed: isOpenDropdownsEmpty,
      getDropdownStructure,
      hasValidBtnDisplayArg,
      isReqArgsValid,
      close,
      open,
      toggle,
      closeDropdownsOutsideClick,
    };
  };
})(document, library);
