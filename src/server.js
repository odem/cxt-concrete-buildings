const express = require('express'),
  server = express(),
  port = 3000;

server.use('/js', express.static('dist/js'));
server.use('/css', express.static('dist/css'));
server.use('/images/logos', express.static('dist/images/logos'));
server.use('/images/icons', express.static('dist/images/icons'));
server.use('/images', express.static('dist/images'));
server.use('/resources', express.static('dist/resources'));

server.get('/', (req, res) => {
  res.sendFile('index.html', { root: 'dist/' });
});

server.get('/contact', (req, res) => {
  res.sendFile('index.html', { root: 'dist/contact' });
});

server.get('/contact/index.html', (req, res) => {
  res.sendFile('index.html', { root: 'dist/contact' });
});

server.get('/installation', (req, res) => {
  res.sendFile('index.html', { root: 'dist/installation' });
});

server.get('/maintenance', (req, res) => {
  res.sendFile('index.html', { root: 'dist/maintenance' });
});

server.get('/products', (req, res) => {
  res.sendFile('index.html', { root: 'dist/products' });
});

server.get('/products/textures-colors.html', (req, res) => {
  res.sendFile('textures-colors.html', { root: 'dist/products' });
});

server.get('/products/buildings', (req, res) => {
  res.sendFile('index.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/concession.html', (req, res) => {
  res.sendFile('concession.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/flush.html', (req, res) => {
  res.sendFile('flush.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/utility.html', (req, res) => {
  res.sendFile('utility.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/shower.html', (req, res) => {
  res.sendFile('shower.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/vault.html', (req, res) => {
  res.sendFile('vault.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/arapahoe.html', (req, res) => {
  res.sendFile('arapahoe.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/cascadian.html', (req, res) => {
  res.sendFile('cascadian.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/cascadian-double.html', (req, res) => {
  res.sendFile('cascadian-double.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/cheyenne.html', (req, res) => {
  res.sendFile('cheyenne.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/cortez.html', (req, res) => {
  res.sendFile('cortez.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/dakota.html', (req, res) => {
  res.sendFile('dakota.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/denali.html', (req, res) => {
  res.sendFile('denali.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/diablo.html', (req, res) => {
  res.sendFile('diablo.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/fontana.html', (req, res) => {
  res.sendFile('fontana.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/gunnison.html', (req, res) => {
  res.sendFile('gunnison.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/kodiak.html', (req, res) => {
  res.sendFile('kodiak.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/malibu.html', (req, res) => {
  res.sendFile('malibu.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/mendocino.html', (req, res) => {
  res.sendFile('mendocino.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/montrose.html', (req, res) => {
  res.sendFile('montrose.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/navajo.html', (req, res) => {
  res.sendFile('navajo.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/ozarkI.html', (req, res) => {
  res.sendFile('ozarkI.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/ozarkII.html', (req, res) => {
  res.sendFile('ozarkII.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/pioneer.html', (req, res) => {
  res.sendFile('pioneer.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/pomona.html', (req, res) => {
  res.sendFile('pomona.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/rainier.html', (req, res) => {
  res.sendFile('rainier.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/rocky-mtn.html', (req, res) => {
  res.sendFile('rocky-mtn.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/rocky-mtn-double.html', (req, res) => {
  res.sendFile('rocky-mtn-double.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/schweitzerI.html', (req, res) => {
  res.sendFile('schweitzerI.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/schweitzerII.html', (req, res) => {
  res.sendFile('schweitzerII.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/taos.html', (req, res) => {
  res.sendFile('taos.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/teton.html', (req, res) => {
  res.sendFile('teton.html', { root: 'dist/products/buildings' });
});

server.get('/products/buildings/tioga.html', (req, res) => {
  res.sendFile('tioga.html', { root: 'dist/products/buildings' });
});

server.listen(port, () => {
  console.log(`Server listening to port: ${port}`);
});
