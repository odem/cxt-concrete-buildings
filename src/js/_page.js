const page = (function(document, library) {
  const args = {
      class: {
        pageHeader: 'js-page-header',
        pageContent: 'js-page-content',
      },
    },
    enableJS = node => {
      if (library.isElementNode(node)) {
        node.setAttribute('data-js', 'enabled');
      }
    },
    init = () => {
      enableJS(library.getFirstElement(args.class.pageHeader));
      enableJS(library.getFirstElement(args.class.pageContent));
    };
  return { init };
})(document, library);
