"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var library = function (window) {
  var isObject = function isObject(obj) {
    return Boolean(obj) && _typeof(obj) === 'object' && !Array.isArray(obj);
  },
      isFunction = function isFunction(func) {
    return Boolean(func) && typeof func === 'function';
  },
      isString = function isString(str) {
    return typeof str === 'string';
  },
      isNumber = function isNumber(value) {
    return typeof value === 'number' && isFinite(value) && !isNaN(parseFloat(value));
  },
      isPositiveNumber = function isPositiveNumber(value) {
    return isNumber(value) && value >= 0;
  },
      isInt = function isInt(value) {
    return Number.isInteger && Number.isInteger(value) || typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
  },
      isPositiveInt = function isPositiveInt(value) {
    return isInt(value) && value > -1;
  },
      isHTMLCollection = function isHTMLCollection(obj) {
    return Object.prototype.toString.call(obj) === '[object HTMLCollection]';
  },
      isNodelist = function isNodelist(obj) {
    return Object.prototype.toString.call(obj) === '[object NodeList]';
  },
      isArray = function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
  },
      objectHasOwnProperty = function objectHasOwnProperty(obj, property) {
    // Avoids bugs when hasOwnProperty is shadowed
    return isObject(obj) && isString(property) && Object.prototype.hasOwnProperty.call(obj, property);
  },
      isDocumentNode = function isDocumentNode(obj) {
    return isObject(obj) && !objectHasOwnProperty(obj, 'nodeType') && obj.nodeType === 9;
  },
      isElementNode = function isElementNode(obj) {
    return isObject(obj) && !objectHasOwnProperty(obj, 'nodeType') && obj.nodeType === 1;
  },
      isArrayLike = function isArrayLike(obj) {
    return (Array.isArray ? Array.isArray(obj) : isArray(obj)) || isHTMLCollection(obj) || isNodelist(obj);
  },
      isObjectEmpty = function isObjectEmpty(obj) {
    return isObject(obj) && Object.keys(obj).length === 0;
  },
      isStringEmpty = function isStringEmpty(str) {
    return isString(str) && str === '';
  },
      isIndexValid = function isIndexValid(index, collection) {
    return isPositiveInt(index) && isArrayLike(collection) && index < collection.length;
  },
      isValidBtn = function isValidBtn(btn) {
    return isElementNode(btn) && btn.hasAttribute('aria-pressed');
  },
      isValidRadioBtn = function isValidRadioBtn(btn) {
    return isValidBtn(btn) && btn.hasAttribute('aria-disabled');
  },
      isBtnPressed = function isBtnPressed(btn) {
    return isValidBtn(btn) && btn.getAttribute('aria-pressed') === 'true';
  },
      isBtnDisabled = function isBtnDisabled(btn) {
    return isValidBtn(btn) && btn.getAttribute('aria-disabled') === 'true';
  },
      isRadioBtnSelected = function isRadioBtnSelected(btn) {
    return isValidRadioBtn(btn) && isBtnPressed(btn) && isBtnDisabled(btn);
  },
      isElementDisabled = function isElementDisabled(element) {
    return isElementNode(element) && element.hasAttribute('disabled') && element.getAttribute('tabindex') === '-1' && element.getAttribute('aria-disabled') === 'true';
  },
      isValidRadioNode = function isValidRadioNode(node) {
    return isElementNode(node) && node.hasAttribute('aria-hidden');
  },
      getObjectValue = function getObjectValue(obj, property) {
    return objectHasOwnProperty(obj, property) ? obj[property] : undefined;
  },
      enableElementNode = function enableElementNode(element) {
    if (isElementNode(element)) {
      element.setAttribute('aria-disabled', 'false');
      element.removeAttribute('disabled');
      element.removeAttribute('tabindex');
    }
  },
      disableElementNode = function disableElementNode(element) {
    if (isElementNode(element)) {
      element.setAttribute('aria-disabled', 'true');
      element.setAttribute('disabled', 'true');
      element.setAttribute('tabindex', '-1');
    }
  },
      selectBtn = function selectBtn(btn) {
    if (isValidBtn(btn)) {
      btn.setAttribute('aria-pressed', 'true');
    }
  },
      unselectBtn = function unselectBtn(btn) {
    if (isValidBtn(btn)) {
      btn.setAttribute('aria-pressed', 'false');
    }
  },
      toggleBtn = function toggleBtn(btn) {
    if (isValidBtn(btn)) {
      isBtnPressed(btn) ? unselectBtn(btn) : selectBtn(btn);
    }
  },
      enableBtn = function enableBtn(btn) {
    if (isValidRadioBtn(btn)) {
      btn.setAttribute('aria-disabled', 'false');
      btn.removeAttribute('disabled');
      btn.removeAttribute('tabindex');
    }
  },
      disableBtn = function disableBtn(btn) {
    if (isValidRadioBtn(btn)) {
      btn.setAttribute('aria-disabled', 'true');
      btn.setAttribute('disabled', 'true');
      btn.setAttribute('tabindex', '-1');
    }
  },
      selectRadioBtn = function selectRadioBtn(btn) {
    if (isValidRadioBtn(btn)) {
      selectBtn(btn);
      disableBtn(btn);
    }
  },
      unselectRadioBtn = function unselectRadioBtn(btn) {
    if (isValidRadioBtn(btn)) {
      unselectBtn(btn);
      enableBtn(btn);
    }
  },
      toggleRadioBtns = function toggleRadioBtns(btns, selectedBtn) {
    var currBtn = null;

    if (isArrayLike(btns) && isValidRadioBtn(selectedBtn)) {
      for (var i = 0; i < btns.length; i++) {
        currBtn = btns[i];

        if (isValidRadioBtn(currBtn)) {
          currBtn === selectedBtn ? selectRadioBtn(currBtn) : unselectRadioBtn(currBtn);
        }
      }
    }
  },
      getSelectedBtn = function getSelectedBtn(btns, isBtnSelectedFunc) {
    var btnsProcessed = 0,
        btn = null;

    if (isArrayLike(btns) && isFunction(isBtnSelectedFunc)) {
      while (btnsProcessed < btns.length) {
        btn = btns[btnsProcessed];

        if (isBtnSelectedFunc(btn)) {
          return btn;
        }

        btnsProcessed++;
      }
    }

    return null;
  },
      toggleRadioNodes = function toggleRadioNodes(nodes, selectedNode) {
    var currNode = null;

    if (isArrayLike(nodes) && isValidRadioNode(selectedNode)) {
      for (var i = 0; i < nodes.length; i++) {
        currNode = nodes[i];

        if (isValidRadioNode(currNode)) {
          currNode === selectedNode ? showElement(currNode) : hideElement(currNode);
        }
      }
    }
  },
      errorMsg = function errorMsg() {
    var invalidArg = 'invalid argument',
        invalidArgs = 'one or more invalid arguments',
        outRangeArg = 'argument is out of valid range',
        argsEvaluationFail = 'unable to evaluate parameter(s)',
        serverRequestFail = 'server request failed.',
        serverResponseIssue = 'issue with the server response.',
        connectionFailure = 'there is an issue with the connection.';
    return {
      argsEvaluationFail: argsEvaluationFail,
      invalidArg: invalidArg,
      invalidArgs: invalidArgs,
      outRangeArg: outRangeArg,
      serverRequestFail: serverRequestFail,
      serverResponseIssue: serverResponseIssue,
      connectionFailure: connectionFailure
    };
  },
      objectAssign = function objectAssign(target) {
    var newObject = null,
        source = null;

    if (target) {
      newObject = Object(target);

      for (var i = 0; i < (arguments.length <= 1 ? 0 : arguments.length - 1); i++) {
        source = i + 1 < 1 || arguments.length <= i + 1 ? undefined : arguments[i + 1];

        if (source) {
          for (var key in source) {
            if (objectHasOwnProperty(source, key)) {
              newObject[key] = source[key];
            }
          }
        }
      }
    }

    return newObject;
  },
      updateObject = function updateObject() {
    return Object.assign ? Object.assign.apply(Object, arguments) : objectAssign.apply(void 0, arguments);
  },
      composeObject = function composeObject() {
    return Object.assign ? Object.assign.apply(Object, arguments) : objectAssign.apply(void 0, arguments);
  },
      copyArray = function copyArray(array) {
    if (Array.isArray(array)) {
      return array.slice(0, array.length);
    }

    return [];
  },
      nodelistToArray = function nodelistToArray(nodeList) {
    if (isNodelist(nodeList) || isHTMLCollection(nodeList)) {
      return Array.from ? Array.from(nodeList) : Array.prototype.slice.call(nodeList);
    }

    return [];
  },
      toggleDataAttribute = function toggleDataAttribute(args) {
    var element = args.element,
        attribute = args.attribute,
        state1 = args.state1,
        state2 = args.state2;
    var newState = '';

    if (isElementNode(element) && isString(attribute) && isString(state1) && isString(state2) && element.hasAttribute(attribute)) {
      newState = element.getAttribute(attribute) === state1 ? state2 : state1;
      element.setAttribute(attribute, newState);
      return true;
    }

    return false;
  },
      setRadialAttribute = function setRadialAttribute(args) {
    var elements = args.elements,
        targetElement = args.targetElement,
        attribute = args.attribute,
        attributeValue = args.attributeValue;
    var currElement = null;

    if (!isArrayLike(elements) || !isElementNode(targetElement) || !isString(attribute) || !isString(attributeValue)) {
      throw new TypeError(errorMsg().argsEvaluationFail + ' to set radial attribute.');
    }

    for (var i = 0; i < elements.length; i++) {
      currElement = elements[i];

      if (isElementNode(currElement)) {
        currElement.removeAttribute(attribute);

        if (currElement === targetElement) {
          currElement.setAttribute(attribute, attributeValue);
        }
      }
    }
  },
      hideElement = function hideElement(element) {
    if (isElementNode(element)) {
      element.setAttribute('aria-hidden', 'true');
    }
  },
      showElement = function showElement(element) {
    if (isElementNode(element)) {
      element.setAttribute('aria-hidden', 'false');
    }
  },
      getCSSProperty = function getCSSProperty(element, property) {
    if (isElementNode(element) && isString(property)) {
      return window.getComputedStyle(element).getPropertyValue(property);
    }

    return '';
  },
      getElementTotalWidth = function getElementTotalWidth(element) {
    var totalWidth = 0,
        width = 0,
        leftMargin = 0,
        rightMargin = 0;

    if (isElementNode(element)) {
      width = element.offsetWidth;
      leftMargin = parseInt(getCSSProperty(element, 'margin-left'), 10) || 0;
      rightMargin = parseInt(getCSSProperty(element, 'margin-right'), 10) || 0;
      totalWidth = width + leftMargin + rightMargin;
    }

    return totalWidth;
  },
      getNextElementSibling = function getNextElementSibling(node) {
    if (isElementNode(node)) {
      return node.nextElementSibling;
    }
  },
      getPreviousElementSibling = function getPreviousElementSibling(node) {
    if (isElementNode(node)) {
      return node.previousElementSibling;
    }
  },
      getMaxNumDisplayElements = function getMaxNumDisplayElements(viewport, element) {
    var viewportWidth = 0,
        viewportLeftPad = 0,
        viewportRightPad = 0,
        elementTotalWidth = 0,
        maxNumDisplayElements = 0;

    if (!isElementNode(viewport) || !isElementNode(element)) {
      throw new TypeError(errorMsg().argsEvaluationFail + ' to get max number of display elements');
    }

    elementTotalWidth = getElementTotalWidth(element);

    if (elementTotalWidth > 0) {
      viewportLeftPad = parseInt(library.getCSSProperty(viewport, 'padding-left')) || 0;
      viewportRightPad = parseInt(library.getCSSProperty(viewport, 'padding-Right')) || 0;
      viewportWidth = viewport.clientWidth - viewportLeftPad - viewportRightPad;
      maxNumDisplayElements = Math.floor(viewportWidth / elementTotalWidth);
    }

    return maxNumDisplayElements;
  },
      isElementFound = function isElementFound(element, matchOptions) {
    var refElement = null,
        attribute = '',
        attributeValue = '',
        doesElementMatch = undefined,
        doesElementHaveAttr = undefined,
        doesAttrMatch = undefined;

    if (!isElementNode(element) || !isObject(matchOptions)) {
      throw new TypeError('Cannot evaluate element for match.');
    }

    refElement = getObjectValue(matchOptions, 'refElement');
    attribute = getObjectValue(matchOptions, 'attribute');
    attributeValue = getObjectValue(matchOptions, 'attributeValue');

    if (refElement && !isElementNode(refElement) || attribute && !isString(attribute) || attributeValue && !isString(attributeValue)) {
      throw new TypeError('Cannot evaluate match options to find element.');
    }

    doesElementMatch = refElement ? element === refElement : undefined;
    doesElementHaveAttr = attribute ? element.hasAttribute(attribute) : undefined;
    doesAttrMatch = attributeValue && doesElementHaveAttr ? element.getAttribute(attribute) === attributeValue : undefined;

    if (refElement && attribute && attributeValue) {
      return doesElementMatch && doesAttrMatch;
    }

    if (refElement && attribute) {
      return doesElementMatch && doesElementHaveAttr;
    }

    if (attribute && attributeValue) {
      return doesAttrMatch;
    }

    if (attribute) {
      return doesElementHaveAttr;
    }

    if (refElement) {
      return doesElementMatch;
    }

    return false;
  },
      getPrevAndCurrElementIndex = function getPrevAndCurrElementIndex(elements, matchOptions) {
    var prevInitialIndex = -1,
        currInitialIndex = -1;
    var numElements = 0,
        element = null,
        index = 0,
        prevIndex = prevInitialIndex,
        currIndex = currInitialIndex,
        prevElementMatchOptions = {},
        currElementMatchOptions = {};

    if (!isArrayLike(elements) || !isObject(matchOptions)) {
      throw new TypeError(errorMsg().argsEvaluationFail + ' to get previous and current element index.');
    }

    numElements = elements.length;

    if (!numElements) {
      return null;
    }

    prevElementMatchOptions = {
      attribute: getObjectValue(matchOptions, 'prevAttribute'),
      attributeValue: getObjectValue(matchOptions, 'prevAttributeValue'),
      refElement: getObjectValue(matchOptions, 'prevRefElement')
    };
    currElementMatchOptions = {
      attribute: getObjectValue(matchOptions, 'currAttribute'),
      attributeValue: getObjectValue(matchOptions, 'currAttributeValue'),
      refElement: getObjectValue(matchOptions, 'currRefElement')
    };

    while (index < numElements && (prevIndex === prevInitialIndex || currIndex === currInitialIndex)) {
      element = elements[index];

      if (isElementNode(element)) {
        if (isElementFound(element, prevElementMatchOptions)) {
          prevIndex = index;
        }

        if (isElementFound(element, currElementMatchOptions)) {
          currIndex = index;
        }
      }

      index++;
    }

    return {
      prevIndex: prevIndex,
      currIndex: currIndex
    };
  },
      getCollectionElement = function getCollectionElement(collection, matchCriteria) {
    var currElement = null;

    if (!isArrayLike(collection) || !objectHasOwnProperty(matchCriteria, 'attribute') || objectHasOwnProperty(matchCriteria, 'attributeValue') && !isString(matchCriteria.attributeValue)) {
      throw new TypeError(errorMsg().argsEvaluationFail + ' to get collection element.');
    }

    for (var currIndex = 0; currIndex < collection.length; currIndex++) {
      currElement = collection[currIndex];

      if (isElementNode(currElement) && isElementFound(currElement, matchCriteria)) {
        return {
          element: currElement,
          index: currIndex
        };
      }
    }

    return null;
  },
      enableElementOverlay = function enableElementOverlay(element) {
    if (isElementNode(element)) {
      element.setAttribute('data-overlay', 'true');
    }
  },
      disableElementOverlay = function disableElementOverlay(element) {
    if (isElementNode(element)) {
      element.setAttribute('data-overlay', 'false');
    }
  },
      detectWebgl = function detectWebgl() {
    var canvas = document.createElement('canvas'),
        contextNames = ['webgl', 'experimental-webgl', 'moz-webgl', 'webkit-3d']; // Check for the WebGL rendering context

    if (window.WebGLRenderingContext) {
      for (var i = 0; i < contextNames.length; i++) {
        if (canvas.getContext && canvas.getContext(contextNames[i])) {
          // WebGL is enabled.
          return 1;
        }
      } // WebGL is supported, but disabled.


      return -1;
    } // WebGL not supported.


    return 0;
  },
      getFirstElement = function getFirstElement(classStr, node) {
    if (isString(classStr)) {
      if (isElementNode(node)) {
        return node.getElementsByClassName(classStr)[0];
      } else {
        return document.getElementsByClassName(classStr)[0];
      }
    }

    return null;
  },
      getElements = function getElements(classStr, node) {
    if (isString(classStr)) {
      if (isElementNode(node)) {
        return node.getElementsByClassName(classStr);
      } else {
        return document.getElementsByClassName(classStr);
      }
    }

    return null;
  },
      elementHasReqAttrs = function elementHasReqAttrs(node, reqAttrs) {
    if (!isElementNode(node) || !isObject(reqAttrs)) {
      return false;
    }

    for (var attr in reqAttrs) {
      if (!node.hasAttribute(reqAttrs[attr])) {
        return false;
      }
    }

    return true;
  },
      exportedFunctions = function exportedFunctions() {
    return {
      objectHasOwnProperty: objectHasOwnProperty,
      isObject: isObject,
      isFunction: isFunction,
      isString: isString,
      isNumber: isNumber,
      isPositiveNumber: isPositiveNumber,
      isInt: isInt,
      isPositiveInt: isPositiveInt,
      isHTMLCollection: isHTMLCollection,
      isNodelist: isNodelist,
      isDocumentNode: isDocumentNode,
      isElementNode: isElementNode,
      isArray: isArray,
      isArrayLike: isArrayLike,
      isObjectEmpty: isObjectEmpty,
      isStringEmpty: isStringEmpty,
      isIndexValid: isIndexValid,
      isValidBtn: isValidBtn,
      isValidRadioBtn: isValidRadioBtn,
      isBtnPressed: isBtnPressed,
      isBtnDisabled: isBtnDisabled,
      isRadioBtnSelected: isRadioBtnSelected,
      isElementDisabled: isElementDisabled,
      enableElementNode: enableElementNode,
      disableElementNode: disableElementNode,
      enableBtn: enableBtn,
      disableBtn: disableBtn,
      selectBtn: selectBtn,
      unselectBtn: unselectBtn,
      selectRadioBtn: selectRadioBtn,
      unselectRadioBtn: unselectRadioBtn,
      toggleBtn: toggleBtn,
      toggleRadioBtns: toggleRadioBtns,
      setRadialAttribute: setRadialAttribute,
      getSelectedBtn: getSelectedBtn,
      toggleRadioNodes: toggleRadioNodes,
      errorMsg: errorMsg,
      objectAssign: objectAssign,
      updateObject: updateObject,
      composeObject: composeObject,
      copyArray: copyArray,
      nodelistToArray: nodelistToArray,
      toggleDataAttribute: toggleDataAttribute,
      hideElement: hideElement,
      showElement: showElement,
      getCSSProperty: getCSSProperty,
      getElementTotalWidth: getElementTotalWidth,
      getNextElementSibling: getNextElementSibling,
      getPreviousElementSibling: getPreviousElementSibling,
      getMaxNumDisplayElements: getMaxNumDisplayElements,
      isElementFound: isElementFound,
      getPrevAndCurrElementIndex: getPrevAndCurrElementIndex,
      getCollectionElement: getCollectionElement,
      enableElementOverlay: enableElementOverlay,
      disableElementOverlay: disableElementOverlay,
      detectWebgl: detectWebgl,
      getFirstElement: getFirstElement,
      getElements: getElements,
      elementHasReqAttrs: elementHasReqAttrs
    };
  }; // Test Purposes
  // module.exports = exportedFunctions();


  return exportedFunctions();
}(window);

var linkedListNode = function (library) {
  var createDefaultNode = function createDefaultNode(nodeType, data, next) {
    var node = {
      data: null,
      next: null
    };

    if (data != undefined) {
      node.data = data;
    }

    if (library.isFunction(nodeType) && nodeType(next)) {
      node.next = next;
    }

    return node;
  },
      isDefaultNode = function isDefaultNode(node) {
    return library.isObject(node) && library.objectHasOwnProperty(node, 'data') && library.objectHasOwnProperty(node, 'next');
  },
      _isSingleLinkedNode = function isSingleLinkedNode(node) {
    return isDefaultNode(node);
  },
      _isDoubleLinkedNode = function isDoubleLinkedNode(node) {
    return isDefaultNode(node) && library.objectHasOwnProperty(node, 'prev');
  };

  return {
    isSingleLinkedNode: function isSingleLinkedNode(node) {
      return _isSingleLinkedNode(node);
    },
    isDoubleLinkedNode: function isDoubleLinkedNode(node) {
      return _isDoubleLinkedNode(node);
    },
    createSingleLinked: function createSingleLinked() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return createDefaultNode.apply(void 0, [_isSingleLinkedNode].concat(args));
    },
    createDoubleLinked: function createDoubleLinked() {
      for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      var prev = args[2],
          node = createDefaultNode.apply(void 0, [_isDoubleLinkedNode].concat(args));
      node.prev = _isDoubleLinkedNode(prev) ? prev : null;
      return node;
    }
  };
}(library);

var smoothScroll = function (window, library) {
  var location = function location() {
    return window.scrollY || window.pageYOffset;
  },
      top = function top(element, startPosition) {
    if (library.isElementNode(element) && library.isNumber(startPosition)) {
      return element.getBoundingClientRect().top + startPosition;
    }

    return 0;
  },
      // Robert Penner's easeInOutQuad - http://robertpenner.com/easing/
  easeInOutQuad = function easeInOutQuad(currTime, begValue, valueChange, duration) {
    if (!library.isNumber(currTime) || !library.isNumber(begValue) || !library.isNumber(valueChange) || !library.isNumber(duration)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to ease smooth scroll.');
    }

    currTime /= duration / 2;

    if (currTime < 1) {
      return valueChange / 2 * currTime * currTime + begValue;
    }

    currTime--;
    return -valueChange / 2 * (currTime * (currTime - 2) - 1) + begValue;
  },
      end = function end(startPosition, scrollDistance, callback) {
    if (!library.isNumber(startPosition) || !library.isNumber(scrollDistance)) {
      library.errorMsg().argsEvaluationFail + ' to finish smooth scroll.';
    }

    window.scrollTo(0, startPosition + scrollDistance);

    if (library.isFunction(callback)) {
      callback();
    }
  },
      loop = function loop(args) {
    var currTime = args.currTime,
        startPosition = args.startPosition,
        scrollDistance = args.scrollDistance,
        duration = args.duration,
        callback = args.callback,
        easing = args.easing;
    var startTime = args.startTime,
        timeElapsed = 0,
        nextScrollPosition = 0;

    if (!library.isNumber(currTime) || !library.isNumber(startPosition) || !library.isNumber(scrollDistance) || !library.isNumber(duration) || !library.isFunction(easing) || startTime && !library.isPositiveNumber(startTime)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to smooth scroll loop.');
    }

    if (!startTime) {
      startTime = currTime;
    } // determine time spent scrolling so far


    timeElapsed = currTime - startTime;
    nextScrollPosition = easing(timeElapsed, startPosition, scrollDistance, duration);
    window.scrollTo(0, nextScrollPosition);
    timeElapsed < duration ? window.requestAnimationFrame(function (currTime) {
      loop({
        currTime: currTime,
        startTime: startTime,
        startPosition: startPosition,
        scrollDistance: scrollDistance,
        duration: duration,
        callback: callback,
        easing: easing
      });
    }) : end(startPosition, scrollDistance, callback);
  },
      jump = function jump(target, options) {
    var jumpOpt = {},
        scrollDistance = 0,
        startPosition = 0,
        stopPosition = 0,
        duration = 0,
        startTime = undefined;

    if (!library.isElementNode(target) || options && !library.isObject(options)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to smooth scroll jump.');
    }

    jumpOpt = {
      duration: options.duration || 1000,
      positionOffset: options.offset || 0,
      callback: options.callback,
      easing: options.easing || easeInOutQuad
    };
    startPosition = location();
    stopPosition = top(target, startPosition);
    scrollDistance = stopPosition - startPosition + jumpOpt.positionOffset;
    duration = library.isFunction(jumpOpt.duration) ? jumpOpt.duration(scrollDistance) : jumpOpt.duration; // start the loop

    window.requestAnimationFrame(function (currTime) {
      loop({
        currTime: currTime,
        startTime: startTime,
        startPosition: startPosition,
        scrollDistance: scrollDistance,
        duration: duration,
        callback: jumpOpt.callback,
        easing: jumpOpt.easing
      });
    });
  };

  return {
    jump: jump
  };
}(window, library);

var copyright = function (document, library) {
  var args = {
    "class": {
      year: 'js-copyrightyear'
    },
    dataAttr: {
      initYear: 'data-initialyear'
    }
  },
      getYearNodes = function getYearNodes() {
    return document.getElementsByClassName(args["class"].year);
  },
      isNodeValid = function isNodeValid(node) {
    return library.isElementNode(node);
  },
      nodeHasInitialYear = function nodeHasInitialYear(node) {
    return isNodeValid(node) && node.hasAttribute(args.dataAttr.initYear);
  },
      getInitialYear = function getInitialYear(node) {
    return parseInt(node.getAttribute(args.dataAttr.initYear), 10);
  },
      isYearValid = function isYearValid(year) {
    return library.isPositiveNumber(year);
  },
      getCopyrightYear = function getCopyrightYear(initYear) {
    var currYear = new Date().getFullYear();
    var copyright = '';

    if (currYear) {
      if (isYearValid(initYear) && initYear < currYear) {
        copyright = initYear + '-' + currYear;
      } else {
        copyright = String(currYear);
      }
    }

    return copyright;
  },
      insertYear = function insertYear(_ref) {
    var xhr = _ref.xhr,
        node = _ref.node,
        initYear = _ref.initYear;
    var responseData = '';

    if (isNodeValid(node) && (!initYear || isYearValid(initYear))) {
      /** Server responses with an success signal. */
      if (xhr && xhr.status === 200) {
        responseData = JSON.parse(xhr.responseText);
        /** Uses server's copyright data. */

        if (responseData.copyright != '') {
          node.innerHTML = responseData.copyright;
        } else {
          /** JavaScript computes the copyright year. */
          node.innerHTML = getCopyrightYear(initYear);
        }
      } else {
        /** Server responses with a fail signal, so JavaScript computes the
         copyright year. */
        node.innerHTML = getCopyrightYear(initYear);
      }
    }
  },
      setYear = function setYear() {
    var nodes = getYearNodes();
    var initYear = null,
        encodedData = null,
        xhr = null,
        currNode = null;

    for (var i = 0; i < nodes.length; i++) {
      currNode = nodes[i];

      if (currNode) {
        if (nodeHasInitialYear(currNode)) {
          initYear = getInitialYear(currNode);
        }

        encodedData = 'initialyear=' + encodeURIComponent(initYear);
        xhr = new XMLHttpRequest();
        xhr.open('POST', 'copyrightyear.php', true);
        xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(encodedData);
        xhr.addEventListener('loadend', insertYear.bind(null, {
          xhr: xhr,
          node: currNode,
          initYear: initYear
        }), false);
      }
    }
  };

  return {
    setYear: setYear
  };
}(document, library);

var dropdown = function (document, library) {
  return function () {
    var dropdownArgs = {
      "class": {
        dropdown: 'js-dropdown',
        btn: 'js-dropdown-btn',
        btnDisplay: 'js-dropdown-btnDisplay',
        menu: 'js-dropdown-menu'
      },
      reqProps: {
        node: 'node',
        btn: 'btn',
        menu: 'menu'
      },
      optProps: {
        btnDisplay: 'btnDisplay'
      },
      btnDisplayAttr: {
        display: 'data-menu'
      }
    },
        openDropdowns = [],
        getBtn = function getBtn(dropdown) {
      return library.getFirstElement(dropdownArgs["class"].btn, dropdown);
    },
        getBtnDisplay = function getBtnDisplay(dropdown) {
      return library.getFirstElement(dropdownArgs["class"].btnDisplay, dropdown);
    },
        getMenu = function getMenu(dropdown) {
      return library.getFirstElement(dropdownArgs["class"].menu, dropdown);
    },
        getDropdowns = function getDropdowns(node) {
      return library.getElements(dropdownArgs["class"].dropdown, node);
    },
        isOpenDropdownsArrayValid = function isOpenDropdownsArrayValid() {
      return Array.isArray(openDropdowns);
    },
        isOpenDropdownsEmpty = function isOpenDropdownsEmpty() {
      return isOpenDropdownsArrayValid() && !openDropdowns.length;
    },
        isReqArgsValid = function isReqArgsValid(args) {
      return library.isObject(args) && library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) && library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) && library.objectHasOwnProperty(args, dropdownArgs.reqProps.node) && library.isValidBtn(args.btn) && library.isElementNode(args.node) && library.isElementNode(args.menu);
    },
        hasValidBtnDisplayArg = function hasValidBtnDisplayArg(args) {
      return library.isObject(args) && library.objectHasOwnProperty(args, dropdownArgs.optProps.btnDisplay) && library.isElementNode(args.btnDisplay);
    },
        getDropdownStructure = function getDropdownStructure(dropdown) {
      var menu = getMenu(dropdown),
          getSubmenuArray = function getSubmenuArray(submenus, menu) {
        var submenuArray = [];
        var processSubmenus = [];
        processSubmenus = Array.isArray(submenus) ? submenus : library.nodelistToArray(submenus);

        if (processSubmenus) {
          processSubmenus.forEach(function (submenu) {
            if (submenu.parentNode === menu) {
              submenuArray.push(getDropdownStructure(submenu));
            }
          });
        }

        return submenuArray;
      };

      var submenus = null,
          btn = null;

      if (menu) {
        submenus = getDropdowns(menu);
        btn = getBtn(dropdown);
        return {
          node: dropdown,
          menu: menu,
          btn: btn,
          btnDisplay: getBtnDisplay(btn),
          submenus: !submenus ? [] : getSubmenuArray(submenus, menu)
        };
      }
    },
        close = function close(dropdown) {
      if (isReqArgsValid(dropdown)) {
        if (isOpenDropdownsArrayValid()) {
          library.unselectBtn(dropdown.btn);
          library.hideElement(dropdown.menu);

          if (hasValidBtnDisplayArg(dropdown)) {
            dropdown.btnDisplay.setAttribute(dropdownArgs.btnDisplayAttr.display, 'close');
          }

          if (!isOpenDropdownsEmpty()) {
            openDropdowns.pop();
          }

          if (isOpenDropdownsEmpty()) {
            document.removeEventListener('click', handleClickOutside, false);
          }

          return true;
        }
      } else {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to close dropdown: ' + dropdown.node);
      }
    },
        open = function open(dropdown) {
      if (isReqArgsValid(dropdown)) {
        if (isOpenDropdownsArrayValid()) {
          library.selectBtn(dropdown.btn);
          library.showElement(dropdown.menu);

          if (hasValidBtnDisplayArg(dropdown)) {
            dropdown.btnDisplay.setAttribute(dropdownArgs.btnDisplayAttr.display, 'open');
          }

          if (isOpenDropdownsEmpty()) {
            document.addEventListener('click', handleClickOutside, false);
          }

          openDropdowns.push(dropdown);
          return true;
        }
      } else {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to open dropdown: ' + dropdown.node);
      }
    },
        closeDropdownsOutsideClick = function closeDropdownsOutsideClick(callback, args, evt) {
      var clickedElement = evt.target;
      var lastOpenDropdown = {},
          isClickOutside = false;

      if (isOpenDropdownsArrayValid()) {
        if (!isOpenDropdownsEmpty()) {
          lastOpenDropdown = openDropdowns[openDropdowns.length - 1];
          isClickOutside = !lastOpenDropdown.node.contains(clickedElement);

          while (isClickOutside && !isOpenDropdownsEmpty()) {
            close(lastOpenDropdown);

            if (!isOpenDropdownsEmpty()) {
              lastOpenDropdown = openDropdowns[openDropdowns.length - 1];
              isClickOutside = !lastOpenDropdown.node.contains(clickedElement);
            }
          }

          if (callback) {
            callback(args, evt);
          }
        }
      }
    },
        handleClickOutside = function handleClickOutside(evt) {
      closeDropdownsOutsideClick(null, null, evt);
    },
        toggle = function toggle(dropdown, evt) {
      if (isReqArgsValid(dropdown)) {
        closeDropdownsOutsideClick(null, null, evt);
        library.toggleDataAttribute({
          element: dropdown.btn,
          attribute: 'aria-pressed',
          state1: 'true',
          state2: 'false'
        });
        library.isBtnPressed(dropdown.btn) ? open(dropdown) : close(dropdown);
      } else {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to toggle dropdown: ' + dropdown.node);
      }
    };

    return {
      areAllDropdownsClosed: isOpenDropdownsEmpty,
      getDropdownStructure: getDropdownStructure,
      hasValidBtnDisplayArg: hasValidBtnDisplayArg,
      isReqArgsValid: isReqArgsValid,
      close: close,
      open: open,
      toggle: toggle,
      closeDropdownsOutsideClick: closeDropdownsOutsideClick
    };
  };
}(document, library);

var filter = function (window, document, library) {
  var filterArgs = {
    "class": {
      btn: 'js-filter-btn',
      item: 'js-filter-item'
    },
    itemAttr: {
      hidden: 'aria-hidden',
      category: 'data-category'
    },
    btnAttr: {
      option: 'data-option'
    }
  },
      btnHasReqAttrs = function btnHasReqAttrs(btn) {
    return library.elementHasReqAttrs(btn, filterArgs.btnAttr);
  },
      itemHasReqAttrs = function itemHasReqAttrs(item) {
    return library.elementHasReqAttrs(item, filterArgs.itemAttr);
  },
      isBtnValid = function isBtnValid(btn) {
    return library.isValidRadioBtn(btn) && btnHasReqAttrs(btn);
  },
      isItemValid = function isItemValid(item) {
    return library.isElementNode(item) && itemHasReqAttrs(item);
  },
      isNumItemsValid = function isNumItemsValid(num) {
    return library.isPositiveNumber(num);
  },
      isBtnSelected = function isBtnSelected(btn) {
    return library.isRadioBtnSelected(btn);
  },
      getBtns = function getBtns(node) {
    return library.getElements(filterArgs["class"].btn, node);
  },
      getItems = function getItems(node) {
    return library.getElements(filterArgs["class"].item, node);
  },
      getItemCategory = function getItemCategory(item) {
    return isItemValid(item) ? item.getAttribute(filterArgs.itemAttr.category) : '';
  },
      getSelectedOption = function getSelectedOption(selectedBtn) {
    return isBtnValid(selectedBtn) ? selectedBtn.getAttribute(filterArgs.btnAttr.option) : '';
  },
      selectBtn = function selectBtn(btns, selectedBtn) {
    var currBtn = null;

    if (library.isArrayLike(btns) && isBtnValid(selectedBtn)) {
      for (var i = 0; i < btns.length; i++) {
        currBtn = btns[i];

        if (isBtnValid(currBtn)) {
          currBtn === selectedBtn ? library.selectRadioBtn(currBtn) : library.unselectRadioBtn(currBtn);
        }
      }
    }
  },
      displayItems = function displayItems(args) {
    var btns = args.btns,
        selectedBtn = args.selectedBtn,
        items = args.items,
        numItems = args.numItems;
    var selectedOption = '',
        item = null,
        itemCategory = '',
        totalNumItems = 0,
        numItemsDisplayed = 0,
        displayNumItems = 0;

    if (library.isArrayLike(btns) && isBtnValid(selectedBtn) && library.isArrayLike(items)) {
      selectBtn(btns, selectedBtn);
      selectedOption = getSelectedOption(selectedBtn).toLowerCase();
      totalNumItems = items.length;
      displayNumItems = isNumItemsValid(numItems) ? numItems : totalNumItems;

      for (var i = 0; i < totalNumItems; i++) {
        item = items[i];

        if (isItemValid(item)) {
          itemCategory = getItemCategory(item).toLowerCase();

          if ((selectedOption === itemCategory || selectedOption === 'all') && numItemsDisplayed < displayNumItems) {
            library.showElement(item);
            numItemsDisplayed++;
          } else {
            library.hideElement(item);
          }
        }
      }
    } else {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to display filter items.');
    }
  };

  return {
    isBtnValid: isBtnValid,
    isItemValid: isItemValid,
    isBtnSelected: isBtnSelected,
    getBtns: getBtns,
    getItems: getItems,
    getItemCategory: getItemCategory,
    getSelectedOption: getSelectedOption,
    displayItems: displayItems
  };
}(window, document, library);

var imgCarousel = function (window, document, library) {
  var imgCarouselArgs = {
    "class": {
      carousel: 'js-imgCarousel',
      imagesViewport: 'js-imgCarousel-imagesViewport',
      imagesContainer: 'js-imgCarousel-imagesContainer',
      image: 'js-imgCarousel-img',
      indicatorsContainer: 'js-imgCarousel-imgIndicatorContainer',
      indicator: 'js-imgCarousel-imgIndicator'
    },
    indicatorAttr: {
      image: 'data-image'
    },
    imageAttr: {
      id: 'id'
    },
    displayImageAttr: {
      first: 'data-firstshown',
      last: 'data-lastshown'
    }
  },
      arrowDirections = {
    left: {
      classname: 'js-imgCarousel-arrowLeft',
      dependentSibling: library.getPreviousElementSibling
    },
    right: {
      classname: 'js-imgCarousel-arrowRight',
      dependentSibling: library.getNextElementSibling
    },
    up: {
      classname: 'js-imgCarousel-arrowUp',
      dependentSibling: library.getPreviousElementSibling
    },
    down: {
      classname: 'js-imgCarousel-arrowDown',
      dependentSibling: library.getNextElementSibling
    }
  },
      getCarousels = function getCarousels(node) {
    return library.getElements(imgCarouselArgs["class"].carousel, node);
  },
      getImagesViewport = function getImagesViewport(node) {
    return library.getFirstElement(imgCarouselArgs["class"].imagesViewport, node);
  },
      getImages = function getImages(node) {
    return library.getElements(imgCarouselArgs["class"].image, node);
  },
      getIndicatorsContainer = function getIndicatorsContainer(node) {
    return library.getFirstElement(imgCarouselArgs["class"].indicatorsContainer, node);
  },
      getIndicators = function getIndicators(node) {
    return library.getElements(imgCarouselArgs["class"].indicator, node);
  },
      isIndicatorValid = function isIndicatorValid(indicator) {
    return library.isValidRadioBtn(indicator) && library.elementHasReqAttrs(indicator, imgCarouselArgs.indicatorAttr);
  },
      isCarouselValid = function isCarouselValid(carousel) {
    return library.isElementNode(carousel);
  },
      isImageValid = function isImageValid(image) {
    return library.isElementNode(image) && library.elementHasReqAttrs(image, imgCarouselArgs.imageAttr);
  },
      isImagesContainerValid = function isImagesContainerValid(container) {
    return library.isElementNode(container);
  },
      getSelectedIndicator = function getSelectedIndicator(indicators, value) {
    return library.getCollectionElement(indicators, {
      attribute: imgCarouselArgs.indicatorAttr.image,
      attributeValue: value
    });
  },
      getArrow = function getArrow(node, direction) {
    var arrowClassname = '';

    if (!library.isElementNode(node) || !library.isString(direction)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to get image carousel arrow.');
    }

    if (library.objectHasOwnProperty(arrowDirections, direction)) {
      arrowClassname = arrowDirections[direction]['classname'];
      return node.getElementsByClassName(arrowClassname)[0];
    }

    return null;
  },
      getFirstImgIndex = function getFirstImgIndex(images) {
    var numImages = 0,
        imagesShift = 0,
        image = 0,
        totalImageWidth = 0,
        firstDisplayIndex = -1;

    if (!library.isArrayLike(images)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to get first image index of image carousel.');
    }

    numImages = images.length;

    if (numImages) {
      image = images[0];
      totalImageWidth = library.getElementTotalWidth(image);

      if (totalImageWidth > 0) {
        imagesShift = parseInt(library.getCSSProperty(image.parentElement, 'left'), 10) || 0;
        firstDisplayIndex++;

        while (imagesShift < 0) {
          firstDisplayIndex++;
          imagesShift += totalImageWidth;
        }
      }
    }

    return firstDisplayIndex;
  },
      getLastImgIndexByDisplay = function getLastImgIndexByDisplay(viewport, elements, startIndex) {
    var numElements = 0,
        elementsShift = 0,
        element = 0,
        totalElementWidth = 0,
        maxNumDisplayElements = 0,
        lastIndex = -1,
        lastDisplayIndex = -1,
        lastPossibleDisplayIndex = -1;

    if (!library.isElementNode(viewport) || !library.isArrayLike(elements) || !library.isPositiveInt(startIndex)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to get last image index based on displayed image carousel.');
    }

    numElements = elements.length;

    if (numElements && library.isIndexValid(startIndex, elements)) {
      element = elements[0];
      maxNumDisplayElements = library.getMaxNumDisplayElements(viewport, element);

      if (maxNumDisplayElements > 0) {
        totalElementWidth = library.getElementTotalWidth(element);
        elementsShift = parseInt(library.getCSSProperty(element.parentElement, 'left'), 10) || 0;

        while (elementsShift >= totalElementWidth) {
          maxNumDisplayElements--;
          elementsShift -= totalElementWidth;
        }

        lastIndex = numElements - 1;
        lastPossibleDisplayIndex = startIndex + maxNumDisplayElements - 1;
        lastDisplayIndex = lastIndex > lastPossibleDisplayIndex ? lastPossibleDisplayIndex : lastIndex;
      }
    }

    return lastDisplayIndex;
  },
      getLastImgIndexByIndex = function getLastImgIndexByIndex(collection, refIndex, shiftAmount) {
    var index = -1,
        numItems = 0,
        maxIndexValue = 0;

    if (!library.isArrayLike(collection) || !library.isInt(refIndex) || !library.isInt(shiftAmount)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to get last image index based on index of image carousel.');
    }

    numItems = collection.length;
    maxIndexValue = numItems - 1;

    if (numItems && library.isIndexValid(refIndex, collection)) {
      index = refIndex + shiftAmount;
    }

    return index > maxIndexValue ? maxIndexValue : index;
  },
      getLastDisplayImage = function getLastDisplayImage(images, options) {
    var index = -1,
        prevLastImage = null,
        shift = 0,
        viewport = undefined,
        startIndex = -1;

    if (!library.isArrayLike(images) || !library.isObject(options)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to get last display image of carousel.');
    }

    shift = library.objectHasOwnProperty(options, 'shift') ? options.shift : undefined;
    viewport = library.objectHasOwnProperty(options, 'viewport') ? options.viewport : undefined;
    startIndex = library.objectHasOwnProperty(options, 'startIndex') ? options.startIndex : -1;
    prevLastImage = library.getCollectionElement(images, {
      attribute: imgCarouselArgs.displayImageAttr.last
    });

    if (prevLastImage && shift != undefined) {
      index = getLastImgIndexByIndex(images, prevLastImage.index, shift);
    }

    if (viewport && library.isIndexValid(images, startIndex)) {
      index = getLastImgIndexByDisplay(viewport, images, startIndex);
    }

    return library.isIndexValid(index, images) ? images[index] : null;
  },
      setImagesContainerWidth = function setImagesContainerWidth(container, image) {
    var maxNumImagesShown = 5,
        widthMultiplier = 2;

    if (isImagesContainerValid(container) && isImageValid(image)) {
      container.style.width = image.offsetWidth * maxNumImagesShown * widthMultiplier + 'px';
    }
  },
      setFirstImageAttribute = function setFirstImageAttribute(images, image) {
    if (!library.isArrayLike(images) || !library.isElementNode(image)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to set first image attribute(s) of the image carousel.');
    }

    library.setRadialAttribute({
      elements: images,
      targetElement: image,
      attribute: imgCarouselArgs.displayImageAttr.first,
      attributeValue: 'true'
    });
  },
      setLastImageAttribute = function setLastImageAttribute(images, image) {
    if (!library.isArrayLike(images) || !library.isElementNode(image)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to set last image attribute(s) of the image carousel.');
    }

    library.setRadialAttribute({
      elements: images,
      targetElement: image,
      attribute: imgCarouselArgs.displayImageAttr.last,
      attributeValue: 'true'
    });
  },
      setArrowDisplay = function setArrowDisplay(element, arrow, direction) {
    var siblingNode = null;

    if (!library.isElementNode(element) || !library.isElementNode(arrow) || !library.isString(direction)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to set arrow display of the image carousel.');
    }

    direction = direction.toLowerCase();

    if (library.objectHasOwnProperty(arrowDirections, direction)) {
      siblingNode = arrowDirections[direction]['dependentSibling'](element);
      siblingNode ? library.showElement(arrow) : library.hideElement(arrow);
    }
  },
      toggleIndicators = function toggleIndicators(indicators, selectedIndicator) {
    library.toggleRadioBtns(indicators, selectedIndicator);
  },
      shiftImages = function shiftImages(image) {
    var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'left';
    var shiftNumImages = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
    var leftPosition = 0,
        shiftAmount = 0,
        imageContainer = null;

    if (!library.isElementNode(image) || !library.isString(direction) || !library.isInt(shiftNumImages)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to shift the image carousel.');
    }

    if (shiftNumImages < 0) {
      throw new RangeError('The number of images to shift the image carousel is out of range.');
    }

    imageContainer = image.parentElement;

    if (imageContainer) {
      direction = direction.toLowerCase();
      leftPosition = parseInt(library.getCSSProperty(imageContainer, 'left'), 10) || 0;

      if (library.objectHasOwnProperty(arrowDirections, direction)) {
        shiftAmount = library.getElementTotalWidth(image) * shiftNumImages;
      } else {
        shiftAmount = 0;
      }

      leftPosition += direction === 'left' || direction === 'up' ? -1 * shiftAmount : shiftAmount;
      imageContainer.style.left = leftPosition + 'px';
    }
  },
      horzLinearImageShift = function horzLinearImageShift(carousel, firstDisplayImage) {
    var viewport = null,
        images = null,
        imagesContainer = null,
        firstImageId = '',
        direction = '',
        shiftNumImages = 0,
        firstImageIndices = {},
        prevFirstImageIndex = -1,
        currFirstImageIndex = -1,
        indicators = null,
        selectedIndicator = null,
        leftArrow = null,
        rightArrow = null,
        lastDisplayImage = {},
        imageShiftDuration = 0;

    if (!isCarouselValid(carousel) || !isImageValid(firstDisplayImage)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to set horizontally shift the image carousel.');
    }

    viewport = getImagesViewport(carousel);
    images = getImages(carousel);
    indicators = getIndicators(carousel);
    leftArrow = getArrow(carousel, 'left');
    rightArrow = getArrow(carousel, 'right');

    if (images.length) {
      firstImageIndices = library.getPrevAndCurrElementIndex(images, {
        prevAttribute: imgCarouselArgs.displayImageAttr.first,
        currRefElement: firstDisplayImage
      });
    }

    if (firstImageIndices) {
      prevFirstImageIndex = firstImageIndices.prevIndex;
      currFirstImageIndex = firstImageIndices.currIndex;
    }

    if (library.isIndexValid(prevFirstImageIndex, images) && library.isIndexValid(currFirstImageIndex, images)) {
      shiftNumImages = Math.abs(prevFirstImageIndex - currFirstImageIndex);

      if (prevFirstImageIndex > currFirstImageIndex) {
        direction = 'right';
      } else if (prevFirstImageIndex < currFirstImageIndex) {
        direction = 'left';
      } else {
        direction = '';
      }
    }

    if (direction && shiftNumImages) {
      firstImageId = firstDisplayImage.getAttribute(imgCarouselArgs.imageAttr.id);
      selectedIndicator = getSelectedIndicator(indicators, firstImageId);
      lastDisplayImage = getLastDisplayImage(images, {
        viewport: viewport,
        startIndex: currFirstImageIndex,
        shift: currFirstImageIndex - prevFirstImageIndex
      });
      library.hideElement(leftArrow);
      library.hideElement(rightArrow);
      setFirstImageAttribute(images, firstDisplayImage);
      shiftImages(firstDisplayImage, direction, shiftNumImages);
      toggleIndicators(indicators, selectedIndicator.element);
      imagesContainer = library.getFirstElement(imgCarouselArgs["class"].imagesContainer, carousel); // CSS property returns a value in seconds.

      imageShiftDuration = parseFloat(library.getCSSProperty(imagesContainer, 'transition-duration')); // Converts value to milliseconds.

      if (imageShiftDuration) {
        imageShiftDuration *= 1000;
      }

      if (leftArrow) {
        setTimeout(setArrowDisplay, imageShiftDuration, firstDisplayImage, leftArrow, 'left');
      }

      if (rightArrow && lastDisplayImage) {
        setLastImageAttribute(images, lastDisplayImage);
        setTimeout(setArrowDisplay, imageShiftDuration, lastDisplayImage, rightArrow, 'right');
      }
    }
  },
      setLinearCarousel = function setLinearCarousel(carousel) {
    var indicatorsContainer = null,
        indicators = null,
        selectedIndicator = null,
        leftArrow = null,
        rightArrow = null,
        viewport = null,
        imagesContainer = null,
        images = null,
        firstImage = null,
        firstImageId = '',
        firstImageIndex = -1,
        isFirstImageValid = false,
        lastImage = null,
        lastImageIndex = -1,
        isLastImageValid = false;

    if (!isCarouselValid(carousel)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to set the linear image carousel.');
    }

    images = getImages(carousel);
    imagesContainer = library.getFirstElement(imgCarouselArgs["class"].imagesContainer, carousel);
    indicatorsContainer = getIndicatorsContainer(carousel);
    indicators = getIndicators(carousel);
    leftArrow = getArrow(carousel, 'left');
    rightArrow = getArrow(carousel, 'right');
    viewport = getImagesViewport(carousel);

    if (images.length) {
      firstImageIndex = getFirstImgIndex(images);
      setImagesContainerWidth(imagesContainer, images[0]);
    }

    if (library.isIndexValid(firstImageIndex, images)) {
      firstImage = images[firstImageIndex];
      lastImageIndex = getLastImgIndexByDisplay(viewport, images, firstImageIndex);

      if (viewport && library.isIndexValid(lastImageIndex, images)) {
        lastImage = images[lastImageIndex];
      }
    }

    isFirstImageValid = isImageValid(firstImage);
    isLastImageValid = isImageValid(lastImage);

    if (isFirstImageValid) {
      setFirstImageAttribute(images, firstImage);
    }

    if (isLastImageValid) {
      setLastImageAttribute(images, lastImage);
    }

    library.showElement(indicatorsContainer);

    if (indicators.length && isFirstImageValid) {
      firstImageId = firstImage.getAttribute(imgCarouselArgs.imageAttr.id);
      selectedIndicator = getSelectedIndicator(indicators, firstImageId);

      if (selectedIndicator) {
        toggleIndicators(indicators, selectedIndicator.element);
      }
    }

    if (leftArrow && isFirstImageValid) {
      setArrowDisplay(firstImage, leftArrow, 'left');
    }

    if (rightArrow && isLastImageValid) {
      setArrowDisplay(lastImage, rightArrow, 'right');
    }
  };

  return {
    isIndicatorValid: isIndicatorValid,
    isCarouselValid: isCarouselValid,
    getCarousels: getCarousels,
    getImages: getImages,
    getIndicators: getIndicators,
    getArrow: getArrow,
    horzLinearImageShift: horzLinearImageShift,
    setLinearCarousel: setLinearCarousel
  };
}(window, document, library);

var linkedList = function (library, linkedListNode) {
  var state = {
    head: null,
    length: 0
  },
      methods = {
    clear: function clear() {
      this.head = null;
      this.length = 0;
    },
    contains: function contains(data) {
      return this.indexOf(data) === -1 ? false : true;
    },
    getSize: function getSize() {
      return this.length;
    },
    indexOf: function indexOf(data) {
      var index = 0,
          currNode = this.head;

      while (currNode != null) {
        if (currNode.data === data) {
          return index;
        }

        currNode = currNode.next;
        index++;
      }

      return -1;
    },
    isEmpty: function isEmpty() {
      return this.length === 0;
    },
    print: function print(separator) {
      var defaultSeparator = ' -> ';

      if (separator === undefined) {
        separator = defaultSeparator;
      }

      if (!library.isString(separator)) {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to print linked list.');
      }

      console.log('list: ' + this.toString(separator));
    },
    toString: function toString(separator) {
      var defaultSeparator = ',';
      var current = this.head,
          list = '';

      if (separator === undefined) {
        separator = defaultSeparator;
      }

      if (!library.isString(separator)) {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to convert linked list data to string.');
      }

      for (var i = 0; i < this.length - 1; i++) {
        list += current.data.toString() + separator;
        current = current.next;
      }

      if (this.length) {
        list += current.data.toString();
      }

      return list;
    }
  },
      singleLinkedMethods = {
    add: function add(index, data) {
      var currNode = null;

      if (!library.isInt(index)) {
        throw new TypeError('index must be an integer to add node at a specific linked list spot.');
      }

      if (index < 0 || index > this.length) {
        throw new RangeError('index is outside linked list range.');
      }

      this.length++;

      if (this.head === null) {
        this.head = linkedListNode.createSingleLinked(data);
      } else {
        if (index === 0) {
          this.head = linkedListNode.createSingleLinked(data, this.head);
        } else {
          currNode = this.head;

          for (var i = 0; i < index - 1; i++) {
            currNode = currNode.next;
          }

          currNode.next = linkedListNode.createSingleLinked(data, currNode.next);
        }
      }
    },
    remove: function remove(index) {
      var currNode = this.head;

      if (!library.isInt(index)) {
        throw new TypeError('index must be an integer to remove node at specific spot in linked list.');
      }

      if (index < 0 || index >= this.length) {
        throw new RangeError('index is outside linked list range.');
      }

      this.length--;

      if (index === 0) {
        this.head = currNode.next;
      } else {
        for (var i = 0; i < index - 1; i++) {
          currNode = currNode.next;
        }

        currNode.next = currNode.next.next;
      }
    },
    removeNode: function removeNode(data) {
      var currNode = this.head;

      if (currNode != null && currNode.data === data) {
        this.head = currNode.next;
        this.length--;
      }

      while (currNode != null && currNode.next != null && currNode.next.data != data) {
        currNode = currNode.next;
      }

      if (currNode != null && currNode.next != null) {
        currNode.next = currNode.next.next;
        this.length--;
      }
    },
    set: function set(index, data) {
      var currNode = this.head;

      if (!library.isInt(index)) {
        throw new TypeError('index must be an integer to add data at a specific linked list spot.');
      }

      if (index < 0 || index >= this.length) {
        throw new RangeError('index is outside linked list range.');
      }

      if (currNode != null) {
        for (var i = 0; i < index; i++) {
          currNode = currNode.next;
        }

        currNode.data = data;
      }
    }
  },
      doubleLinkedMethods = {
    add: function add(index, data) {},
    addFirst: function addFirst(data) {},
    addLast: function addLast(data) {},
    clear: function clear() {},
    contains: function contains(data) {},
    getSize: function getSize() {},
    indexOf: function indexOf(data) {},
    isEmpty: function isEmpty() {},
    print: function print() {},
    remove: function remove(index) {},
    removeFirst: function removeFirst() {},
    removeLast: function removeLast() {},
    removeNode: function removeNode(data) {},
    set: function set(index, data) {}
  };
  return {
    create: function create(type) {
      var protoMethodTypes = {
        singlelinked: singleLinkedMethods,
        doublelinked: doubleLinkedMethods
      };
      var protoMethods = null,
          list = null;

      if (!library.isString(type)) {
        throw new TypeError(library.errorMsg().argsEvaluationFail + ' to create linked list.');
      }

      protoMethods = protoMethodTypes[type.toLowerCase()];

      if (protoMethods === undefined) {
        throw new RangeError('linked list type must "singlelinked" or "doublelinked" (case-insensitive).');
      }

      list = library.composeObject({}, state, methods, protoMethods);
      return {
        add: function add(index, data) {
          return list.add(index, data);
        },
        addFirst: function addFirst(data) {
          return list.add(0, data);
        },
        addLast: function addLast(data) {
          return list.add(list.length, data);
        },
        clear: function clear() {
          return list.clear();
        },
        contains: function contains(data) {
          return list.contains(data);
        },
        getSize: function getSize() {
          return list.getSize();
        },
        indexOf: function indexOf(data) {
          return list.indexOf(data);
        },
        isEmpty: function isEmpty() {
          return list.isEmpty();
        },
        print: function print(separator) {
          return list.print(separator);
        },
        remove: function remove(index) {
          return list.remove(index);
        },
        removeFirst: function removeFirst() {
          return list.remove(0);
        },
        removeLast: function removeLast() {
          return list.remove(list.length - 1);
        },
        removeNode: function removeNode(data) {
          return list.removeNode(data);
        },
        set: function set(index, data) {
          return list.set(index, data);
        },
        toString: function toString(separator) {
          return list.toString(separator);
        }
      };
    }
  };
}(library, linkedListNode);

var media = function (document, library) {
  var mediaArgs = {
    "class": {
      container: 'js-media',
      btn: 'js-media-btn',
      thumbnail: 'js-visualMedia-img'
    },
    containerAttr: {
      image: 'data-image'
    },
    btnAttr: {
      host: 'data-host',
      mediaId: 'data-media'
    }
  },
      isContainerValid = function isContainerValid(node) {
    return library.isElementNode(node) && library.elementHasReqAttrs(node, mediaArgs.containerAttr);
  },
      isBtnValid = function isBtnValid(btn) {
    return library.isValidBtn(btn) && library.elementHasReqAttrs(btn, mediaArgs.btnAttr);
  },
      isHostValid = function isHostValid(host) {
    return library.isString(host);
  },
      isIdValid = function isIdValid(id) {
    return library.isString(id);
  },
      getSketchfabSrc = function getSketchfabSrc(id) {
    if (isIdValid(id)) {
      return 'https://sketchfab.com/models/' + id + '/embed?autostart=1';
    }

    return '';
  },
      getVimeoSrc = function getVimeoSrc(id) {
    if (isIdValid(id)) {
      return 'https://player.vimeo.com/video/' + id + '?autoplay=1&color=ffffff&title=0&byline=0&portrait=0';
    }

    return '';
  },
      getYoutubeSrc = function getYoutubeSrc(id) {
    if (isIdValid(id)) {
      return 'https://www.youtube-nocookie.com/embed/' + id + '?rel=0&amp;showinfo=0&amp;autoplay=1&mute=1';
    }

    return '';
  },
      getYoutubeImgSrc = function getYoutubeImgSrc(id) {
    if (isIdValid(id)) {
      return 'https://img.youtube.com/vi/' + id + '/sddefault.jpg';
    }

    return '';
  },
      getMediaThumbnail = function getMediaThumbnail(container) {
    return library.getFirstElement(mediaArgs["class"].thumbnail, container);
  },
      hideMediaThumbnail = function hideMediaThumbnail(container) {
    library.hideElement(getMediaThumbnail(container));
  },
      loadInteractiveIframe = function loadInteractiveIframe(container, host, id) {
    var iframe = null;

    if (isContainerValid(container) && isHostValid(host) && isIdValid(id) && host.toLowerCase() === 'sketchfab') {
      iframe = document.createElement('iframe');
      iframe.classList.add('VisualMedia-iframe');
      iframe.classList.add('js-media-iframe');
      iframe.setAttribute('id', id);
      iframe.setAttribute('src', '');
      iframe.setAttribute('aria-hidden', 'true');
      iframe.setAttribute('frameborder', '0');
      iframe.setAttribute('allow', 'autoplay; fullscreen; vr');
      iframe.setAttribute('allowvr', 'true');
      iframe.setAttribute('allowfullscreen', 'true');
      iframe.setAttribute('webkitallowfullscreen', 'true');
      iframe.setAttribute('mozallowfullscreen', 'true');
      iframe.innerHTML = '';
      container.appendChild(iframe);
    }
  },
      loadMedia = function loadMedia(args) {
    var container = args.container,
        btn = args.btn,
        host = args.host,
        id = args.id;
    var iframe = null,
        iframeSrc = '',
        hostName = '';

    if (isContainerValid(container) && isBtnValid(btn) && isHostValid(host) && isIdValid(id)) {
      hostName = host.toLowerCase();

      if (hostName === 'vimeo') {
        iframeSrc = getVimeoSrc(id);
      } else if (hostName === 'youtube') {
        iframeSrc = getYoutubeSrc(id);
      } else if (hostName === 'sketchfab') {
        iframeSrc = getSketchfabSrc(id);
      } else {
        throw new RangeError('cannot lazyload media due to invalid media host: ' + hostName);
      }

      library.disableBtn(btn);
      library.hideElement(getMediaThumbnail(container));
      iframe = document.createElement('iframe');
      iframe.setAttribute('src', iframeSrc);
      iframe.setAttribute('frameborder', '0');
      iframe.setAttribute('allowfullscreen', '');
      iframe.setAttribute('webkitallowfullscreen', '');
      iframe.setAttribute('mozallowfullscreen', '');
      iframe.classList.add('VisualMedia-iframe');
      iframe.classList.add('js-media-iframe');
      iframe.innerHTML = '';
      container.appendChild(iframe);
    }
  },
      loadThumbnail = function loadThumbnail(args) {
    var container = args.container,
        host = args.host,
        id = args.id;
    var isImgSet = 'false',
        img = null;

    if (isHostValid(host) && host.toLowerCase() === 'youtube' && isContainerValid(container) && isIdValid(id)) {
      isImgSet = container.getAttribute(mediaArgs.containerAttr.image).toLowerCase();

      if (isImgSet === 'false') {
        img = new Image();
        img.src = getYoutubeImgSrc(id);
        img.classList.add('VisualMedia-img');
        img.classList.add('js-visualMedia-img');

        if (library.isElementNode(img) && document.addEventListener) {
          img.addEventListener('load', function () {
            container.appendChild(img);
          });
        }
      }
    }
  },
      lazyLoad = function lazyLoad() {
    var btns = library.getElements(mediaArgs["class"].btn);
    var btn = null,
        container = null,
        host = '',
        id = '';

    if (library.isArrayLike(btns)) {
      for (var i = 0, n = btns.length; i < n; i++) {
        btn = btns[i];

        if (isBtnValid(btn)) {
          host = btn.getAttribute(mediaArgs.btnAttr.host);
          id = btn.getAttribute(mediaArgs.btnAttr.mediaId);
          container = library.getFirstElement(mediaArgs["class"].container, btn.parentNode) || btn.parentNode;

          if (host.toLowerCase() === 'youtube') {
            loadThumbnail({
              container: container,
              host: host,
              id: id
            });
          }

          if (library.isElementNode(btn) && document.addEventListener) {
            btn.addEventListener('click', loadMedia.bind(null, {
              container: container,
              btn: btn,
              host: host,
              id: id
            }), false);
          }
        }
      }
    }
  };

  return {
    lazyLoad: lazyLoad,
    loadInteractiveIframe: loadInteractiveIframe,
    hideMediaThumbnail: hideMediaThumbnail
  };
}(document, library);

var sort = function (window, document, library) {
  var sortArgs = {
    "class": {
      btn: 'js-sort-btn',
      item: 'js-sort-item'
    },
    btnAttr: {
      category: 'data-category',
      order: 'data-order'
    },
    itemAttr: {
      name: 'data-itemName'
    }
  },
      isBtnValid = function isBtnValid(btn) {
    return library.isValidRadioBtn(btn) && library.elementHasReqAttrs(btn, sortArgs.btnAttr);
  },
      isItemValid = function isItemValid(item) {
    return library.isElementNode(item) && library.elementHasReqAttrs(item, sortArgs.itemAttr);
  },
      isCategoryValid = function isCategoryValid(category) {
    if (library.isString(category)) {
      category = category.toLowerCase();
      return category === 'alphabetical';
    }

    return false;
  },
      isBtnSelected = function isBtnSelected(btn) {
    return library.isRadioBtnSelected(btn);
  },
      getBtns = function getBtns(node) {
    return library.getElements(sortArgs["class"].btn, node);
  },
      getItems = function getItems(node) {
    return library.getElements(sortArgs["class"].item, node);
  },
      getItemName = function getItemName(item) {
    if (isItemValid(item)) {
      return item.getAttribute(sortArgs.itemAttr.name).toLowerCase();
    }

    return '';
  },
      getItemValue = function getItemValue(item, sortCategory) {
    if (isItemValid(item)) {
      sortCategory = sortCategory.toLowerCase();

      if (sortCategory === 'alphabetical') {
        return getItemName(item);
      }
    }

    return null;
  },
      getSelectedCategory = function getSelectedCategory(selectedBtn) {
    if (isBtnValid(selectedBtn)) {
      return selectedBtn.getAttribute(sortArgs.btnAttr.category);
    }

    return '';
  },
      getSelectedOrder = function getSelectedOrder(selectedBtn) {
    if (isBtnValid(selectedBtn)) {
      return selectedBtn.getAttribute(sortArgs.btnAttr.order);
    }

    return '';
  },
      selectBtn = function selectBtn(btns, selectedBtn) {
    if (library.isArrayLike(btns) && isBtnValid(selectedBtn)) {
      library.toggleRadioBtns(btns, selectedBtn);
    }
  },
      sortItems = function sortItems(item1, item2, category) {
    var item1Value = null,
        item2Value = null;

    if (!isCategoryValid(category)) {
      throw new RangeError('invalid sort category.');
    }

    if (!isItemValid(item1) && isItemValid(item2)) {
      return 1;
    } else if (isItemValid(item1) && !isItemValid(item2)) {
      return -1;
    } else if (!isItemValid(item1) && !isItemValid(item2)) {
      return 0;
    } else {
      category = category.toLowerCase();
      item1Value = getItemValue(item1, category);
      item2Value = getItemValue(item2, category);

      if (item1Value && item2Value && item1Value > item2Value) {
        return 1;
      }

      if (item1Value && item2Value && item1Value < item2Value) {
        return -1;
      }

      return 0;
    }
  },
      sortDOMItems = function sortDOMItems(args) {
    var btns = args.btns,
        selectedBtn = args.selectedBtn,
        items = args.items;
    var currItems = [],
        sortCategory = '',
        sortOrder = '',
        sortIndex = 0,
        itemsContainer = null,
        item = null;

    if (library.isArrayLike(btns) && isBtnValid(selectedBtn) && library.isArrayLike(items)) {
      selectBtn(btns, selectedBtn);
      sortCategory = getSelectedCategory(selectedBtn).toLowerCase();
      sortOrder = getSelectedOrder(selectedBtn).toLowerCase();
      itemsContainer = items[0].parentNode;
      currItems = library.nodelistToArray(items);

      if (currItems.length && sortCategory) {
        currItems.sort(function (item1, item2) {
          return sortItems(item1, item2, sortCategory);
        });
      }

      library.hideElement(itemsContainer);

      for (var i = 0; i < currItems.length; i++) {
        sortIndex = sortOrder === 'descending' ? currItems.length - 1 - i : i;
        item = currItems[sortIndex];

        if (isItemValid(item)) {
          itemsContainer.appendChild(itemsContainer.removeChild(item));
        }
      }

      library.showElement(itemsContainer);
    } else {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to sort items.');
    }
  };

  return {
    isBtnValid: isBtnValid,
    isItemValid: isItemValid,
    isBtnSelected: isBtnSelected,
    getBtns: getBtns,
    getItems: getItems,
    sortDOMItems: sortDOMItems
  };
}(window, document, library);

var buildingImages = function (window, document, library, imgCarousel) {
  var buildingImagesArgs = {
    indicatorAttr: {
      image: 'data-image'
    },
    displayImageAttr: {
      first: 'data-firstShown'
    }
  },
      arrowSelectedLinearShift = function arrowSelectedLinearShift(carousel, direction) {
    var images = null,
        firstDisplayImage = null,
        newFirstImage = null,
        getNewImage = function getNewImage() {};

    if (!imgCarousel.isCarouselValid(carousel) || !library.isString(direction)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to linear shift images from arrow selection.');
    }

    images = imgCarousel.getImages(carousel);
    direction = direction.toLowerCase();

    if (images.length && (direction === 'left' || direction === 'right')) {
      firstDisplayImage = library.getCollectionElement(images, {
        attribute: buildingImagesArgs.displayImageAttr.first
      });
      getNewImage = direction === 'left' ? library.getNextElementSibling : library.getPreviousElementSibling;
      newFirstImage = getNewImage(firstDisplayImage.element);

      if (newFirstImage) {
        imgCarousel.horzLinearImageShift(carousel, newFirstImage);
      }
    }
  },
      imageSelectedLinearShift = function imageSelectedLinearShift(carousel, selectedIndicator) {
    var imageId = '',
        newFirstImage = null;

    if (!imgCarousel.isCarouselValid(carousel) || !imgCarousel.isIndicatorValid(selectedIndicator)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to linear shift images from image selection.');
    }

    imageId = selectedIndicator.getAttribute(buildingImagesArgs.indicatorAttr.image);
    newFirstImage = document.getElementById(imageId);

    if (newFirstImage) {
      imgCarousel.horzLinearImageShift(carousel, newFirstImage);
    }
  },
      setCarousels = function setCarousels() {
    var carousels = imgCarousel.getCarousels(document);
    var currCarousel = null,
        indicators = null,
        indicator = null,
        leftArrow = null,
        rightArrow = null;

    if (carousels && carousels.length) {
      for (var i = 0; i < carousels.length; i++) {
        currCarousel = carousels[i];
        indicators = imgCarousel.getIndicators(currCarousel);
        rightArrow = imgCarousel.getArrow(currCarousel, 'right');
        leftArrow = imgCarousel.getArrow(currCarousel, 'left');
        imgCarousel.setLinearCarousel(currCarousel);

        if (document.addEventListener) {
          for (var _i = 0; _i < indicators.length; _i++) {
            indicator = indicators[_i];
            indicator.addEventListener('click', imageSelectedLinearShift.bind(null, currCarousel, indicator), false);
          }
        }

        if (document.addEventListener && rightArrow) {
          rightArrow.addEventListener('click', arrowSelectedLinearShift.bind(null, currCarousel, 'left'), false);
        }

        if (document.addEventListener && leftArrow) {
          leftArrow.addEventListener('click', arrowSelectedLinearShift.bind(null, currCarousel, 'right'), false);
        }

        if (window.addEventListener) {
          window.addEventListener('orientationchange', imgCarousel.setLinearCarousel.bind(null, currCarousel), false);
          window.addEventListener('resize', imgCarousel.setLinearCarousel.bind(null, currCarousel), false);
        }
      }
    }
  };

  return {
    setCarousels: setCarousels
  };
}(window, document, library, imgCarousel);

var buildings = function (document, library, sort, linkedList) {
  var buildingsArgs = {
    "class": {
      sortBtns: 'js-buildings-sort-btns',
      sortItems: 'js-buildings-sort-items',
      filter: 'js-Buildings-filter',
      filterBtn: 'js-buildings-filterbtn',
      filterOptions: 'js-buildings-filterOptions',
      filterCategory: 'js-buildings-filterCategory',
      filterCategoryBtn: 'js-buildings-filterCategoryBtn',
      filterCategoryMenu: 'js-buildings-filterCategoryMenu',
      filterCategoryInput: 'js-buildings-filterCategoryInput',
      filterCategoryInputQuantity: 'js-buildings-filterCategoryInputQuantity',
      buildings: 'js-buildings-item',
      sxnContent: 'js-Buildings-pagecontent',
      pageContent: 'js-page-content',
      warning: 'js-warning'
    },
    filterAttr: {
      page: 'data-page'
    },
    buildingAttr: {
      name: 'data-itemName'
    },
    sortBtnAttr: {
      order: 'data-order'
    },
    categoryMenuAttr: {
      hidden: 'aria-hidden'
    },
    optionAttr: {
      value: 'value',
      category: 'data-category'
    },
    categoryAttr: {
      category: 'data-category'
    },
    warningMsg: {
      db: 'Database connection issue.',
      network: 'Network connection issue.',
      filter: 'Buildings cannot be filtered at this time. Please try again later.'
    },
    filterServerFile: 'buildingsFilter.php'
  },
      selectedFilterOptions = {},
      getSortBtnsContainer = function getSortBtnsContainer() {
    return library.getFirstElement(buildingsArgs["class"].sortBtns);
  },
      getfilter = function getfilter(node) {
    if (library.isElementNode(node) || library.isDocumentNode(node)) {
      return node.getElementsByClassName('js-buildings-filter')[0];
    }

    return null;
  },
      getFilterInputs = function getFilterInputs(node) {
    if (library.isElementNode(node) || library.isDocumentNode(node)) {
      return node.getElementsByClassName('js-buildings-filterCategoryInput');
    }

    return null;
  },
      isValidFilter = function isValidFilter(filter) {
    return library.isElementNode(filter) && library.elementHasReqAttrs(filter, buildingsArgs.filterAttr);
  },
      isValidBuilding = function isValidBuilding(building) {
    return library.isElementNode(building) && library.elementHasReqAttrs(building, buildingsArgs.buildingAttr);
  },
      isCategoryMenuValid = function isCategoryMenuValid(menu) {
    return library.isElementNode(menu) && library.elementHasReqAttrs(menu, buildingsArgs.categoryMenuAttr);
  },
      isOptionElementValid = function isOptionElementValid(node) {
    return library.isElementNode(node) && library.elementHasReqAttrs(node, buildingsArgs.optionAttr);
  },
      isValidCategoryElement = function isValidCategoryElement(element) {
    return library.isElementNode(element) && library.elementHasReqAttrs(element, buildingsArgs.categoryAttr);
  },
      getFilterPage = function getFilterPage(filter) {
    return isValidFilter(filter) ? filter.getAttribute(buildingsArgs.filterAttr.page) : '';
  },
      getBuildingName = function getBuildingName(building) {
    return isValidBuilding(building) ? building.getAttribute(buildingsArgs.buildingAttr.name) : '';
  },
      getSortOrder = function getSortOrder(sortBtns) {
    var currBtn = null;

    if (library.isArrayLike(sortBtns)) {
      for (var i = 0; i < sortBtns.length; i++) {
        currBtn = sortBtns[i];

        if (sort.isBtnValid(currBtn) && currBtn.getAttribute('aria-pressed') === 'true') {
          return currBtn.getAttribute(buildingsArgs.sortBtnAttr.order);
        }
      }
    }

    return '';
  },
      getOptionCategory = function getOptionCategory(option) {
    if (isOptionElementValid(option) || isValidCategoryElement(option)) {
      return option.getAttribute(buildingsArgs.optionAttr.category);
    }

    return '';
  },
      getOptionValue = function getOptionValue(option) {
    if (isOptionElementValid(option)) {
      return option.getAttribute(buildingsArgs.optionAttr.value);
    }

    return '';
  },
      toggleFilterCategory = function toggleFilterCategory(btn, menu) {
    if (library.isValidBtn(btn)) {
      library.toggleDataAttribute({
        element: btn,
        attribute: 'aria-pressed',
        state1: 'true',
        state2: 'false'
      });
    }

    if (isCategoryMenuValid(menu)) {
      library.toggleDataAttribute({
        element: menu,
        attribute: 'aria-hidden',
        state1: 'true',
        state2: 'false'
      });
    }
  },
      isFilterOptionSelected = function isFilterOptionSelected(option) {
    return library.isElementNode(option) && option.hasAttribute('checked');
  },
      toggleFilterOption = function toggleFilterOption(input, inputContainer) {
    if (!library.isElementNode(input) || !library.isElementNode(inputContainer)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to toggle filter option: ' + input);
    }

    if (isFilterOptionSelected(input)) {
      input.removeAttribute('checked');
    } else {
      input.setAttribute('checked', 'true');
    }

    if (library.isElementNode(inputContainer)) {
      library.toggleDataAttribute({
        element: inputContainer,
        attribute: 'aria-pressed',
        state1: 'true',
        state2: 'false'
      });
    }
  },
      toggleFilter = function toggleFilter(btn, menu) {
    if (!library.isValidBtn(btn)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to toggle filter associated with filter button: ' + btn);
    }

    library.toggleBtn(btn);
    library.isBtnPressed(btn) ? library.showElement(menu) : library.hideElement(menu);
  },
      filterBuildings = function filterBuildings(displayBuildings) {
    var numDisplayBuildings = 0,
        currDisplayBuildingName = '',
        displayIndex = 0,
        allBuildings = null,
        currBuilding = null,
        currBuildingName = '',
        sortOrder = '';

    if (!library.isArrayLike(displayBuildings)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to filter buildings.');
    }

    numDisplayBuildings = displayBuildings.length;

    if (numDisplayBuildings) {
      sortOrder = getSortOrder(sort.getBtns(getSortBtnsContainer()));

      if (sortOrder === 'ascending') {
        displayBuildings.sort();
      }

      if (sortOrder === 'descending') {
        displayBuildings.sort(function (a, b) {
          if (a < b) {
            return 1;
          } else if (a > b) {
            return -1;
          } else {
            return 0;
          }
        });
      }

      allBuildings = library.getElements(buildingsArgs["class"].buildings, document);

      for (var i = 0; i < allBuildings.length; i++) {
        currBuilding = allBuildings[i];
        currBuildingName = getBuildingName(currBuilding);

        if (displayIndex < numDisplayBuildings) {
          currDisplayBuildingName = displayBuildings[displayIndex];
        } else {
          currDisplayBuildingName = '';
        }

        if (currBuildingName && currDisplayBuildingName && currBuildingName === currDisplayBuildingName) {
          library.showElement(currBuilding);
          displayIndex++;
        }

        if (currBuildingName && currBuildingName != currDisplayBuildingName) {
          library.hideElement(currBuilding);
        }
      }
    }
  },
      addFilterOption = function addFilterOption(category, value) {
    var list = null;

    if (!library.isString(category) || !library.isString(value)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to add filter option: ' + category);
    }

    if (library.isObject(selectedFilterOptions)) {
      if (!library.objectHasOwnProperty(selectedFilterOptions, category)) {
        selectedFilterOptions[category] = linkedList.create('singlelinked');
      }

      list = selectedFilterOptions[category];

      if (list && library.objectHasOwnProperty(list, 'addFirst')) {
        list.addFirst(value);
      }
    }
  },
      removeFilterOption = function removeFilterOption(category, value) {
    var list = null;

    if (!library.isString(category) || !library.isString(value)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to remove filter option: ' + category);
    }

    if (library.isObject(selectedFilterOptions)) {
      list = selectedFilterOptions[category];

      if (list && library.objectHasOwnProperty(list, 'removeNode')) {
        list.removeNode(value);
      }
    }
  },
      updatePageFilterOptions = function updatePageFilterOptions(pagename) {
    var addOptions = null,
        option = null;
    var options = {
      buildingsconcession: [{
        'building-type': 'concession'
      }],
      buildingsflush: [{
        'building-type': 'restroom'
      }, {
        plumbing: 'yes'
      }],
      buildingsshower: [{
        'building-type': 'restroom-shower'
      }],
      buildingsutility: [{
        'building-type': 'utility'
      }],
      buildingsvault: [{
        'building-type': 'restroom'
      }, {
        plumbing: 'no'
      }]
    };

    if (!library.isString(pagename)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to update filter options.');
    }

    pagename = pagename.toLowerCase();

    if (library.objectHasOwnProperty(options, pagename)) {
      addOptions = options[pagename];

      for (var i = 0; i < addOptions.length; i++) {
        option = addOptions[i];

        for (var category in option) {
          if (library.objectHasOwnProperty(option, category)) {
            addFilterOption(category, option[category]);
          }
        }
      }
    }
  },
      modifyFilterOptionsDisplay = function modifyFilterOptionsDisplay(categories, applicableOptions) {
    var applicableValues = null,
        categoryElement = null,
        allCategoryOptions = null,
        option = null,
        optionValue = '',
        optionValueNumeric = NaN,
        optionQuantity = null,
        isOptionApplicable = false,
        numAssociatedBuildings = 0;

    if (!library.isArrayLike(categories) || !library.isObject(applicableOptions)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to modify filter options display.');
    }

    for (var i = 0; i < categories.length; i++) {
      categoryElement = categories[i];

      if (isValidCategoryElement(categoryElement)) {
        allCategoryOptions = getFilterInputs(categoryElement);
        applicableValues = applicableOptions[getOptionCategory(categoryElement)];
      }

      if (library.isArrayLike(allCategoryOptions)) {
        for (var k = 0; k < allCategoryOptions.length; k++) {
          option = allCategoryOptions[k];

          if (isOptionElementValid(option)) {
            optionValue = getOptionValue(option);
            optionValueNumeric = Number(optionValue, 10);
            optionQuantity = library.getFirstElement(buildingsArgs["class"].filterCategoryInputQuantity, option.parentNode);

            if (!isNaN(optionValueNumeric)) {
              optionValue = 'n' + optionValue;
            }

            isOptionApplicable = library.objectHasOwnProperty(applicableValues, optionValue);

            if (optionQuantity) {
              numAssociatedBuildings = isOptionApplicable ? applicableValues[optionValue] : 0;
              optionQuantity.innerHTML = '(' + numAssociatedBuildings + ')';
            }

            if (isOptionApplicable && library.isElementDisabled(option)) {
              library.enableElementNode(option);
              library.enableElementNode(option.parentNode);
            }

            if (!isOptionApplicable && !isFilterOptionSelected(option)) {
              library.disableElementNode(option);
              library.disableElementNode(option.parentNode);
            }
          }
        }
      }
    }
  },
      buildUrlParameters = function buildUrlParameters() {
    var optionsList = null,
        parameter = '',
        urlParameters = '';

    for (var category in selectedFilterOptions) {
      if (library.objectHasOwnProperty(selectedFilterOptions, category)) {
        optionsList = selectedFilterOptions[category];

        if (library.objectHasOwnProperty(optionsList, 'isEmpty') && library.objectHasOwnProperty(optionsList, 'toString') && !optionsList.isEmpty()) {
          parameter = String(category) + '=' + optionsList.toString();
          urlParameters = library.isStringEmpty(urlParameters) ? parameter : urlParameters + '&' + parameter;
        }
      }
    }

    return urlParameters;
  },
      sendRequest = function sendRequest() {
    var xhr = new XMLHttpRequest();
    var url = '',
        pagePathname = '',
        urlParameters = '';
    pagePathname = buildingsArgs.filterServerFile;
    urlParameters = buildUrlParameters();
    url = pagePathname + '?' + urlParameters;
    xhr.open('POST', url);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Accept', 'application/json; charset=utf-8');
    xhr.send();
    return xhr;
  },
      submitFilterOption = function submitFilterOption(selectedOption) {
    var optionValue = '',
        optionCategory = '',
        updateFilterOptions = null,
        xhr = null,
        pageContent = null,
        sxnContent = null;

    if (selectedOption && !isOptionElementValid(selectedOption)) {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to submit filter option');
    }

    pageContent = library.getFirstElement(buildingsArgs["class"].pageContent, document);
    sxnContent = library.getFirstElement(buildingsArgs["class"].sxnContent, document);
    library.enableElementOverlay(pageContent);
    library.enableElementOverlay(sxnContent);

    if (selectedOption) {
      optionCategory = getOptionCategory(selectedOption);
      optionValue = getOptionValue(selectedOption);
      updateFilterOptions = isFilterOptionSelected(selectedOption) ? removeFilterOption : addFilterOption;
      updateFilterOptions(optionCategory, optionValue);
    }

    xhr = sendRequest();

    if (addEventListener) {
      xhr.addEventListener('loadend', function () {
        var filter = getfilter(document);
        var responseData = {},
            filterWarning = null;

        if (filter) {
          filterWarning = library.getFirstElement(buildingsArgs["class"].warning, filter);
        }

        if (filterWarning) {
          filterWarning.innerHTML = '';
          library.hideElement(filterWarning);
        }

        if (xhr.status === 200) {
          responseData = JSON.parse(xhr.responseText);

          if (!library.isObjectEmpty(responseData)) {
            if (selectedOption) {
              toggleFilterOption(selectedOption, selectedOption.parentNode);
            }

            modifyFilterOptionsDisplay(library.getElements(buildingsArgs["class"].filterCategory, document), responseData);
            library.disableElementOverlay(pageContent);
            library.disableElementOverlay(sxnContent);
            filterBuildings(responseData.buildings);
          } else {
            library.disableElementOverlay(pageContent);
            library.disableElementOverlay(sxnContent);

            if (filterWarning) {
              filterWarning.innerHTML = buildingsArgs.warningMsg.db + ' ' + buildingsArgs.warningMsg.filter;
              library.showElement(filterWarning);
            }

            throw new Error(library.errorMsg().serverResponseIssue);
          }
        } else {
          library.disableElementOverlay(pageContent);
          library.disableElementOverlay(sxnContent);

          if (filterWarning) {
            filterWarning.innerHTML = buildingsArgs.warningMsg.network + ' ' + buildingsArgs.warningMsg.filter;
            library.showElement(filterWarning);
          }

          throw new Error(library.errorMsg().serverRequestFail + ' server status: ' + xhr.status);
        }
      }, false);
    }
  },
      setFilter = function setFilter() {
    var page = null,
        filter = null,
        filterBtn = null,
        filterOptions = null,
        categoryBtns = null,
        btn = null,
        categoryInputs = null,
        input = null,
        menu = null;
    filter = getfilter(document);
    categoryBtns = library.getElements(buildingsArgs["class"].filterCategoryBtn, document);
    categoryInputs = library.getElements(buildingsArgs["class"].filterCategoryInput, document);

    if (isValidFilter(filter)) {
      filterBtn = library.getFirstElement(buildingsArgs["class"].filterBtn, filter);
      filterOptions = library.getFirstElement(buildingsArgs["class"].filterOptions, filter);
      page = getFilterPage(filter);
    }

    if (page) {
      updatePageFilterOptions(page);
    }

    if (filter) {
      submitFilterOption();
    }

    if (filter && addEventListener) {
      filter.addEventListener('submit', function (evt) {
        var inputSelected = document.activeElement;
        evt.preventDefault();
        submitFilterOption(inputSelected);
      }, false);
    }

    if (categoryInputs && addEventListener) {
      for (var i = 0; i < categoryInputs.length; i++) {
        input = categoryInputs[i];

        if (library.isElementNode(input)) {
          input.addEventListener('click', submitFilterOption.bind(null, input), false);
        }
      }
    }

    if (filterBtn && addEventListener) {
      filterBtn.addEventListener('click', toggleFilter.bind(null, filterBtn, filterOptions), false);
    }

    if (categoryBtns && addEventListener) {
      for (var _i2 = 0; _i2 < categoryBtns.length; _i2++) {
        btn = categoryBtns[_i2];

        if (library.isValidBtn(btn)) {
          menu = library.getFirstElement(buildingsArgs["class"].filterCategoryMenu, btn.parentNode.parentNode);

          if (isCategoryMenuValid(menu)) {
            btn.addEventListener('click', toggleFilterCategory.bind(null, btn, menu), false);
          }
        }
      }
    }
  },
      setSort = function setSort() {
    var btns = null,
        btn = null,
        items = null;
    btns = sort.getBtns(getSortBtnsContainer());
    items = sort.getItems(library.getFirstElement(buildingsArgs["class"].sortItems));

    if (library.isArrayLike(btns) && library.isArrayLike(items)) {
      if (items.length) {
        sort.sortDOMItems({
          items: items,
          btns: btns,
          selectedBtn: library.getSelectedBtn(btns, sort.isBtnSelected)
        });
      }

      for (var i = 0; i < btns.length; i++) {
        btn = btns[i];

        if (library.isElementNode(btn) && document.addEventListener) {
          btn.addEventListener('click', sort.sortDOMItems.bind(null, {
            items: items,
            btns: btns,
            selectedBtn: btn
          }), false);
        }
      }
    }
  };

  return {
    setSort: setSort,
    setFilter: setFilter
  };
}(document, library, sort, linkedList);

var installInstructions = function (document, library, filter) {
  var instructions = {
    initialNumDisplay: 6,
    numDisplayIncrement: 6,
    totalNumDisplay: 0,
    categoryData: []
  };

  var instructionArgs = {
    "class": {
      btns: 'js-install-instructions-btns',
      items: 'js-install-instructions-items',
      viewMoreBtn: 'js-install-instructions-viewMoreBtn'
    }
  },
      isCategoryObjectValid = function isCategoryObjectValid(obj) {
    return library.isObject(obj) && library.objectHasOwnProperty(obj, 'category') && library.isString(obj.category) && library.objectHasOwnProperty(obj, 'itemCount') && library.isPositiveNumber(obj.itemCount);
  },
      getCategoryCount = function getCategoryCount(categoryData, category) {
    var dataProcessed = 0,
        data = null;

    if (Array.isArray(categoryData) && library.isString(category)) {
      while (dataProcessed < categoryData.length) {
        data = categoryData[dataProcessed];

        if (isCategoryObjectValid(data) && data.category === category) {
          return data.itemCount;
        }

        dataProcessed++;
      }
    }

    return -1;
  },
      createNewCategory = function createNewCategory(category) {
    var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

    if (library.isString(category) && library.isPositiveNumber(count)) {
      return {
        category: category,
        itemCount: count
      };
    }

    return {};
  },
      addCategoryData = function addCategoryData(categoryData, obj) {
    if (Array.isArray(categoryData) && isCategoryObjectValid(obj)) {
      return categoryData.concat(obj);
    }

    return null;
  },
      findCategoryData = function findCategoryData(categoryData, category) {
    var index = 0,
        data = null;

    if (Array.isArray(categoryData) && library.isString(category)) {
      while (index < categoryData.length) {
        data = categoryData[index];

        if (isCategoryObjectValid(data) && data.category === category) {
          return index;
        }

        index++;
      }
    }

    return -1;
  },
      setCategoryData = function setCategoryData(categoryData, items) {
    var data = [],
        category = '',
        dataIndex = 0,
        item = null;

    if (Array.isArray(categoryData) && library.isArrayLike(items)) {
      data = library.copyArray(categoryData);

      for (var i = 0; i < items.length; i++) {
        item = items[i];

        if (filter.isItemValid(item)) {
          category = filter.getItemCategory(item);
          dataIndex = findCategoryData(data, category);

          if (dataIndex >= 0) {
            data[dataIndex].itemCount++;
          } else {
            data.push(createNewCategory(category));
          }
        }
      }
    }

    return data;
  },
      filterItems = function filterItems(args) {
    var btns = args.btns,
        selectedBtn = args.selectedBtn,
        items = args.items,
        numItems = args.numItems,
        incrementNumItems = args.incrementNumItems;
    var category = '',
        categoryCount = 0,
        viewMoreBtn = null;

    if (library.isArrayLike(btns) && filter.isBtnValid(selectedBtn) && library.isArrayLike(items)) {
      if (library.isPositiveNumber(numItems)) {
        instructions = library.updateObject({}, instructions, {
          totalNumDisplay: numItems
        });
      }

      if (library.isPositiveNumber(incrementNumItems)) {
        instructions = library.updateObject({}, instructions, {
          totalNumDisplay: instructions.totalNumDisplay + incrementNumItems
        });
      }

      category = filter.getSelectedOption(selectedBtn);
      categoryCount = getCategoryCount(instructions.categoryData, category);
      viewMoreBtn = library.getFirstElement(instructionArgs["class"].viewMoreBtn);
      categoryCount !== -1 && instructions.totalNumDisplay < categoryCount ? library.showElement(viewMoreBtn) : library.hideElement(viewMoreBtn);
      filter.displayItems({
        items: items,
        btns: btns,
        selectedBtn: selectedBtn,
        numItems: instructions.totalNumDisplay
      });
    } else {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' to filter instructions.');
    }
  },
      setFilter = function setFilter() {
    var btns = null,
        btn = null,
        viewMoreBtn = null,
        items = null,
        totalNumItems = 0;
    btns = filter.getBtns(library.getFirstElement(instructionArgs["class"].btns));
    items = filter.getItems(library.getFirstElement(instructionArgs["class"].items));

    if (library.isArrayLike(btns) && library.isArrayLike(items)) {
      totalNumItems = items.length;
      viewMoreBtn = library.getFirstElement(instructionArgs["class"].viewMoreBtn);

      for (var i = 0; i < btns.length; i++) {
        btn = btns[i];

        if (library.isElementNode(btn) && document.addEventListener) {
          btn.addEventListener('click', filterItems.bind(null, {
            items: items,
            btns: btns,
            selectedBtn: btn,
            numItems: instructions.initialNumDisplay
          }), false);
        }
      }

      if (totalNumItems) {
        instructions = library.updateObject({}, instructions, {
          categoryData: addCategoryData(instructions.categoryData, createNewCategory('all', totalNumItems))
        });
        instructions = library.updateObject({}, instructions, {
          categoryData: setCategoryData(instructions.categoryData, items)
        });
        filterItems({
          items: items,
          btns: btns,
          selectedBtn: library.getSelectedBtn(btns, filter.isBtnSelected),
          numItems: instructions.initialNumDisplay
        });

        if (library.isElementNode(viewMoreBtn) && document.addEventListener) {
          viewMoreBtn.addEventListener('click', function () {
            filterItems({
              items: items,
              btns: btns,
              selectedBtn: library.getSelectedBtn(btns, filter.isBtnSelected),
              incrementNumItems: instructions.numDisplayIncrement
            });
          }, false);
        }
      }
    }
  };

  return {
    setFilter: setFilter
  };
}(document, library, filter);

var interactiveModel = function (document, library, media, dropdown, smoothScroll) {
  var configurator = {
    api: null,
    config: null,
    iframe: null,
    modelContents: null,
    modelContainer: null,
    optionBtns: null,
    optionNavBtns: null,
    options: {}
  },
      materialCategories = {
    wall: ['WALL_EXT_SANS', 'Barnwood_B', 'EXT_W_SANS', 'EXT_W_ROCK', 'EXT_W'],
    roof: ['R_EXT_SANS', 'R_EDGE', 'R_STUCCO'],
    fixture: ['Vitreous', 'HD_White']
  },
      materials = {
    WALL_EXT_SANS: 'wall',
    Barnwood_B: 'wall',
    EXT_W_SANS: 'wall',
    EXT_W_ROCK: 'wall',
    EXT_W: 'wall',
    R_EXT_SANS: 'roof',
    R_EDGE: 'roof',
    R_STUCCO: 'roof',
    Vitreous: 'fixture',
    HD_White: 'fixture'
  },
      modelArgs = {
    "class": {
      media: 'js-media',
      mediaIframe: 'js-media-iframe',
      optionBtn: 'js-building-model-option',
      optionNavBtn: 'js-building-model-option-nav-btn',
      helpSxn: 'js-model-help-sxn',
      helpLink: 'js-model-help-link',
      colorGrid: 'js-building-model-color-grid',
      dropdownBtn: ' js-dropdown-btn',
      dropdownMenu: ' js-dropdown-menu',
      warning: 'js-model-warning'
    },
    sxnAttr: {
      overlay: 'data-overlay'
    },
    containerAttr: {
      modelId: 'data-modelId'
    },
    optionBtnAttr: {
      color: 'data-color'
    },
    optionNavBtnAttr: {
      category: 'data-category',
      controls: 'aria-controls'
    },
    optionTabAttr: {
      controls: 'aria-controls',
      selectedColor: 'data-selected-color',
      category: 'data-category'
    },
    helpLinkAttr: {
      controls: 'aria-controls'
    },
    warningMsg: {
      disabled: 'Your browser supports WebGL, but it is disabled. WebGL is needed ' + 'to view the 3D model. See "WebGL Disabled" in the help section ' + 'below.',
      unsupported: 'Your browser does not supports WebGL. WebGL is needed to view ' + 'the 3D model. See "WebGL Supported Browsers" in the help section ' + 'below.'
    }
  },
      helpSxnDropdown = dropdown(),
      isModelContainerValid = function isModelContainerValid(container) {
    return library.isElementNode(container) && library.elementHasReqAttrs(container, modelArgs.containerAttr);
  },
      isOptionNavBtnValid = function isOptionNavBtnValid(btn) {
    return library.isValidRadioBtn(btn) && library.elementHasReqAttrs(btn, modelArgs.optionNavBtnAttr);
  },
      isOptionTabValid = function isOptionTabValid(tab) {
    return library.isElementNode(tab) && library.elementHasReqAttrs(tab, modelArgs.optionTabAttr);
  },
      getModelId = function getModelId(node) {
    if (library.isElementNode(node)) {
      return node.getAttribute(modelArgs.containerAttr.modelId);
    }

    return '';
  },
      getOptionNavBtn = function getOptionNavBtn(id) {
    var btns = null,
        btn = null;

    if (library.isString(id)) {
      btns = library.getElements(modelArgs["class"].optionNavBtn);

      for (var i = 0; i < btns.length; i++) {
        btn = btns[i];

        if (isOptionNavBtnValid(btn) && btn.getAttribute(modelArgs.optionNavBtnAttr.controls) === id && library.isRadioBtnSelected(btn)) {
          return btn;
        }
      }
    }

    return null;
  },
      openCorrespondingDropdown = function openCorrespondingDropdown(args, evt) {
    evt.stopPropagation();
    setTimeout(smoothScroll.jump, 280, args.node, {
      offset: -75
    });
    helpSxnDropdown.open(args);
  },
      addHelpSxnLinkEvents = function addHelpSxnLinkEvents(container) {
    var links = null,
        link = null,
        sxnId = '',
        sxn = null,
        sxnBtn = null,
        sxnMenu = null;

    if (library.isElementNode(container)) {
      links = library.getElements(modelArgs["class"].helpLink, container);

      for (var i = 0; i < links.length; i++) {
        link = links[i];
        sxnId = link.getAttribute(modelArgs.helpLinkAttr.controls);

        if (sxnId) {
          sxn = document.getElementById(sxnId);
        }

        if (sxn) {
          sxnBtn = library.getFirstElement(modelArgs["class"].dropdownBtn, sxn);
          sxnMenu = library.getFirstElement(modelArgs["class"].dropdownMenu, sxn);
        }

        if (sxnBtn) {
          link.addEventListener('click', helpSxnDropdown.closeDropdownsOutsideClick.bind(null, openCorrespondingDropdown, {
            node: sxn,
            btn: sxnBtn,
            menu: sxnMenu
          }), false);
        }
      }
    }
  },
      addHelpSxnClickEvents = function addHelpSxnClickEvents(args) {
    if (helpSxnDropdown.isReqArgsValid(args)) {
      args.btn.addEventListener('click', helpSxnDropdown.toggle.bind(null, args), false);
      args.submenus.forEach(function (submenu) {
        if (helpSxnDropdown.isReqArgsValid(submenu)) {
          addHelpSxnClickEvents(submenu);
        }
      });
    }
  },
      selectOption = function selectOption(optionBtns, evt) {
    if (library.isArrayLike(optionBtns)) {
      var selectedOptionBtn = evt.currentTarget,
          value = selectedOptionBtn.getAttribute(modelArgs.optionBtnAttr.color),
          optionGroup = selectedOptionBtn.parentNode.parentNode.parentNode,
          optionGroupId = optionGroup.getAttribute('id'),
          optionNavBtn = getOptionNavBtn(optionGroupId);
      var materialName = '',
          category = '',
          materialsToUpdate = null;

      if (isOptionNavBtnValid(optionNavBtn)) {
        library.toggleRadioBtns(optionBtns, selectedOptionBtn);
        category = optionNavBtn.getAttribute(modelArgs.optionNavBtnAttr.category);
        optionNavBtn.setAttribute('data-selected-color', value);
        materialsToUpdate = materialCategories[category];

        for (var i = 0; i < materialsToUpdate.length; i++) {
          materialName = materialsToUpdate[i];

          if (materialName && library.objectHasOwnProperty(materials, materialName) && configurator.api.materialHash[materialName]) {
            configurator.api.setColor(materialName, configurator.api.AlbedoPBR, 'color', value);
            configurator.api.setColor(materialName, configurator.api.DiffusePBR, 'color', value);
            configurator.api.setColor(materialName, configurator.api.DiffuseColor, 'color', value);
          }
        }
      }
    }
  },
      selectOptionTab = function selectOptionTab(optionTabs, optionBtns, evt) {
    var selectedOptionTab = null,
        tabSxnId = '',
        tabSxns = null,
        tabSxn = null;

    if (library.isArrayLike(optionTabs) && library.isArrayLike(optionBtns)) {
      selectedOptionTab = evt.target;

      if (isOptionTabValid(selectedOptionTab)) {
        library.toggleRadioBtns(optionTabs, selectedOptionTab);
        tabSxnId = selectedOptionTab.getAttribute(modelArgs.optionTabAttr.controls);
        tabSxn = document.getElementById(tabSxnId);
        tabSxns = library.getElements(modelArgs["class"].colorGrid);
        library.toggleRadioNodes(tabSxns, tabSxn);
        setOptionBtns(optionBtns, selectedOptionTab);
      }
    }
  },
      setOptionBtns = function setOptionBtns(optionBtns, selectedNavBtn) {
    var selectedColor = '',
        selectedOptionBtn = null;

    if (library.isArrayLike(optionBtns) && isOptionNavBtnValid(selectedNavBtn) && selectedNavBtn.hasAttribute('data-selected-color')) {
      selectedColor = selectedNavBtn.getAttribute('data-selected-color').toLowerCase();
      selectedOptionBtn = library.getSelectedBtn(optionBtns, function (btn) {
        return btn.hasAttribute(modelArgs.optionBtnAttr.color) && btn.getAttribute(modelArgs.optionBtnAttr.color).toLowerCase() === selectedColor;
      });
      library.toggleRadioBtns(optionBtns, selectedOptionBtn);
    }
  },
      onModelAPIReady = function onModelAPIReady(selectedOptionNavBtn) {
    var optionBtns = configurator.optionBtns;
    var optionNavBtns = configurator.optionNavBtns;
    configurator.api.removeEventListener(configurator.api.EVENT_INITIALIZED, onModelAPIReady);
    setOptionBtns(optionBtns, selectedOptionNavBtn);

    for (var i = 0; i < optionBtns.length; i++) {
      optionBtns[i].addEventListener('click', selectOption.bind(null, optionBtns), false);
    }

    for (var _i3 = 0; _i3 < optionNavBtns.length; _i3++) {
      optionNavBtns[_i3].addEventListener('click', selectOptionTab.bind(null, optionNavBtns, optionBtns), false);
    }

    configurator.modelContents.setAttribute(modelArgs.sxnAttr.overlay, 'false');
  },
      initializeModel = function initializeModel(config, selectedOptionNavBtn) {
    configurator.config = config;
    configurator.api = new SketchfabAPIUtility(configurator.config.id, configurator.iframe, {
      camera: 0,
      preload: 1,
      ui_infos: 0,
      ui_controls: 0,
      graph_optimizer: 0,
      ui_watermark: 0,
      autostart: 1
    });
    configurator.api.addEventListener(configurator.api.EVENT_INITIALIZED, onModelAPIReady.bind(null, selectedOptionNavBtn));
    configurator.api.create();
    library.showElement(library.getFirstElement(modelArgs["class"].mediaIframe, configurator.modelContainer));
  },
      load = function load() {
    var webglDetection = library.detectWebgl(),
        models = library.getElements(modelArgs["class"].media);
    var model = null,
        modelId = '',
        selectedOptionNavBtn = null,
        modelSxn = null,
        buildingHasModel = false,
        helpSxn = null,
        helpDropdown = {},
        warning = null;

    for (var i = 0; i < models.length; i++) {
      model = models[i];
      configurator.modelContainer = model.parentNode;
      modelSxn = configurator.modelContainer.parentNode;
      configurator.modelContents = modelSxn.parentNode;
      helpSxn = library.getFirstElement(modelArgs["class"].helpSxn, modelSxn);

      if (helpSxn) {
        helpDropdown = helpSxnDropdown.getDropdownStructure(helpSxn);

        if (helpSxnDropdown.isReqArgsValid(helpDropdown)) {
          addHelpSxnClickEvents(helpDropdown);
          addHelpSxnLinkEvents(modelSxn);
        }
      }

      if (modelSxn) {
        warning = library.getFirstElement(modelArgs["class"].warning, modelSxn);
      }

      if (webglDetection <= 0) {
        if (warning) {
          warning.innerHTML = webglDetection === 0 ? modelArgs.warningMsg.unsupported : modelArgs.warningMsg.disabled;
          library.showElement(warning);
          helpSxnDropdown.open({
            node: helpDropdown.node,
            btn: helpDropdown.btn,
            menu: helpDropdown.menu,
            btnDisplay: helpDropdown.btnDisplay
          });
        }
      } else {
        if (warning) {
          library.hideElement(warning);
        }

        if (modelSxn.hasAttribute('model') && modelSxn.getAttribute('model') === 'true') {
          buildingHasModel = true;
        }

        if (buildingHasModel) {
          configurator.modelContents.setAttribute(modelArgs.sxnAttr.overlay, 'true');
        }

        if (buildingHasModel && isModelContainerValid(configurator.modelContainer)) {
          modelId = getModelId(configurator.modelContainer);
          media.loadInteractiveIframe(model, 'sketchfab', modelId);
        }

        if (modelId) {
          configurator.iframe = library.getFirstElement(modelArgs["class"].mediaIframe, model);
        }

        if (modelId && configurator.iframe) {
          configurator.optionBtns = library.getElements(modelArgs["class"].optionBtn, configurator.modelContents);
          configurator.optionNavBtns = library.getElements(modelArgs["class"].optionNavBtn, configurator.modelContents);
          selectedOptionNavBtn = library.getSelectedBtn(configurator.optionNavBtns, library.isRadioBtnSelected);
          initializeModel({
            id: modelId
          }, selectedOptionNavBtn);
        }
      }
    }
  };

  return {
    load: load
  };
}(document, library, media, dropdown, smoothScroll);

var navSite = function (window, document, library, dropdown) {
  var navSiteDropdown = dropdown(),
      args = {
    breakpoint: 950,
    "class": {
      navSiteNode: 'js-navSite',
      btn: 'js-navSite-btn',
      btnDisplay: 'js-navSite-btnHamburger',
      menu: 'js-navSite-menu',
      pageContent: 'js-page-content'
    }
  },
      getArgs = function getArgs() {
    return {
      breakpoint: args.breakpoint,
      btn: library.getFirstElement(args["class"].btn),
      btnDisplay: library.getFirstElement(args["class"].btnDisplay),
      menu: library.getFirstElement(args["class"].menu),
      pageOverlay: library.getFirstElement(args["class"].pageContent),
      node: library.getFirstElement(args["class"].navSiteNode)
    };
  },
      isReqArgsValid = function isReqArgsValid(args) {
    return library.isObject(args) && library.isPositiveNumber(args.breakpoint) && navSiteDropdown.isReqArgsValid(args);
  },
      hasValidOverlayArg = function hasValidOverlayArg(overlay) {
    return library.isElementNode(overlay);
  },
      setView = function setView() {
    var args = getArgs();

    if (isReqArgsValid(args)) {
      args.btn.setAttribute('aria-hidden', window.innerWidth >= args.breakpoint ? 'true' : 'false');

      if (navSiteDropdown.hasValidBtnDisplayArg(args)) {
        args.btnDisplay.setAttribute('data-menu', library.isBtnPressed(args.btn) ? 'open' : 'close');
      }

      if (window.innerWidth >= args.breakpoint) {
        args.menu.setAttribute('aria-hidden', 'false');

        if (hasValidOverlayArg(args.pageOverlay)) {
          args.pageOverlay.setAttribute('data-overlay', 'false');
        }
      } else {
        args.menu.setAttribute('aria-hidden', String(!library.isBtnPressed(args.btn)));

        if (hasValidOverlayArg(args.pageOverlay)) {
          args.pageOverlay.setAttribute('data-overlay', String(library.isBtnPressed(args.btn)));
        }
      }

      return true;
    } else {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' for site nav initial display.');
    }
  },
      closePageOverlay = function closePageOverlay(evt) {
    var _getArgs = getArgs(),
        node = _getArgs.node,
        pageOverlay = _getArgs.pageOverlay,
        isClickOutside = !node.contains(evt.target);

    if (isClickOutside && navSiteDropdown.areAllDropdownsClosed()) {
      pageOverlay.setAttribute('data-overlay', 'false');
      document.removeEventListener('click', closePageOverlay, false);
    }
  },
      togglePageOverlay = function togglePageOverlay() {
    var _getArgs2 = getArgs(),
        btn = _getArgs2.btn,
        pageOverlay = _getArgs2.pageOverlay,
        isBtnPressed = library.isBtnPressed(btn);

    if (hasValidOverlayArg(pageOverlay)) {
      pageOverlay.setAttribute('data-overlay', String(isBtnPressed));

      if (isBtnPressed) {
        document.addEventListener('click', closePageOverlay, false);
      }

      return true;
    } else {
      throw new TypeError(library.errorMsg().argsEvaluationFail + ' for site nav page overlay.');
    }
  },
      addDropdownBtnEvents = function addDropdownBtnEvents(args) {
    if (navSiteDropdown.isReqArgsValid(args) && document.addEventListener) {
      args.btn.addEventListener('click', navSiteDropdown.toggle.bind(null, args), false);
      args.submenus.forEach(function (submenu) {
        if (navSiteDropdown.isReqArgsValid(submenu)) {
          addDropdownBtnEvents(submenu);
        }
      });
    }
  },
      makeResponsive = function makeResponsive() {
    var args = getArgs();
    var dropdowns = {};

    if (isReqArgsValid(args)) {
      dropdowns = navSiteDropdown.getDropdownStructure(args.node);
      setView();

      if (dropdowns) {
        addDropdownBtnEvents(dropdowns);
      }

      if (hasValidOverlayArg(args.pageOverlay)) {
        args.btn.addEventListener('click', togglePageOverlay, false);
      }

      if (window.addEventListener) {
        window.addEventListener('resize', setView, false);
        window.addEventListener('orientationchange', setView, false);
      }

      return true;
    } else {
      throw new TypeError('Site nav could not be made responsive.');
    }
  };

  return {
    makeResponsive: makeResponsive
  };
}(window, document, library, dropdown);

var page = function (document, library) {
  var args = {
    "class": {
      pageHeader: 'js-page-header',
      pageContent: 'js-page-content'
    }
  },
      enableJS = function enableJS(node) {
    if (library.isElementNode(node)) {
      node.setAttribute('data-js', 'enabled');
    }
  },
      init = function init() {
    enableJS(library.getFirstElement(args["class"].pageHeader));
    enableJS(library.getFirstElement(args["class"].pageContent));
  };

  return {
    init: init
  };
}(document, library);
/** ======================================================================== */

/**
 * @fileoverview Manages site navigation, and setting adblock attributes.
 *
 * @author Shana Odem
 * @copyright 2019
 *
 * @param  {Object} window - browser's window containing a DOM document.
 * @param  {Object} document - DOM document containing the page contents.
 */


if (document.addEventListener) {
  document.addEventListener('DOMContentLoaded', page.init, false);
  document.addEventListener('DOMContentLoaded', navSite.makeResponsive, false);
  document.addEventListener('DOMContentLoaded', interactiveModel.load, false);
  document.addEventListener('DOMContentLoaded', media.lazyLoad, false);
  document.addEventListener('DOMContentLoaded', buildings.setSort, false);
  document.addEventListener('DOMContentLoaded', buildings.setFilter, false);
  document.addEventListener('DOMContentLoaded', buildingImages.setCarousels, false);
  document.addEventListener('DOMContentLoaded', installInstructions.setFilter, false);
  document.addEventListener('DOMContentLoaded', copyright.setYear, false);
}