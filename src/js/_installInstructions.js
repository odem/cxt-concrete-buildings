const installInstructions = (function(document, library, filter) {
  let instructions = {
    initialNumDisplay: 6,
    numDisplayIncrement: 6,
    totalNumDisplay: 0,
    categoryData: [],
  };
  const instructionArgs = {
      class: {
        btns: 'js-install-instructions-btns',
        items: 'js-install-instructions-items',
        viewMoreBtn: 'js-install-instructions-viewMoreBtn',
      },
    },
    isCategoryObjectValid = obj => {
      return (
        library.isObject(obj) &&
        library.objectHasOwnProperty(obj, 'category') &&
        library.isString(obj.category) &&
        library.objectHasOwnProperty(obj, 'itemCount') &&
        library.isPositiveNumber(obj.itemCount)
      );
    },
    getCategoryCount = (categoryData, category) => {
      let dataProcessed = 0,
        data = null;

      if (Array.isArray(categoryData) && library.isString(category)) {
        while (dataProcessed < categoryData.length) {
          data = categoryData[dataProcessed];

          if (isCategoryObjectValid(data) && data.category === category) {
            return data.itemCount;
          }

          dataProcessed++;
        }
      }

      return -1;
    },
    createNewCategory = (category, count = 1) => {
      if (library.isString(category) && library.isPositiveNumber(count)) {
        return { category, itemCount: count };
      }

      return {};
    },
    addCategoryData = (categoryData, obj) => {
      if (Array.isArray(categoryData) && isCategoryObjectValid(obj)) {
        return categoryData.concat(obj);
      }

      return null;
    },
    findCategoryData = (categoryData, category) => {
      let index = 0,
        data = null;

      if (Array.isArray(categoryData) && library.isString(category)) {
        while (index < categoryData.length) {
          data = categoryData[index];

          if (isCategoryObjectValid(data) && data.category === category) {
            return index;
          }

          index++;
        }
      }

      return -1;
    },
    setCategoryData = (categoryData, items) => {
      let data = [],
        category = '',
        dataIndex = 0,
        item = null;

      if (Array.isArray(categoryData) && library.isArrayLike(items)) {
        data = library.copyArray(categoryData);

        for (let i = 0; i < items.length; i++) {
          item = items[i];

          if (filter.isItemValid(item)) {
            category = filter.getItemCategory(item);
            dataIndex = findCategoryData(data, category);

            if (dataIndex >= 0) {
              data[dataIndex].itemCount++;
            } else {
              data.push(createNewCategory(category));
            }
          }
        }
      }

      return data;
    },
    filterItems = args => {
      const { btns, selectedBtn, items, numItems, incrementNumItems } = args;
      let category = '',
        categoryCount = 0,
        viewMoreBtn = null;

      if (
        library.isArrayLike(btns) &&
        filter.isBtnValid(selectedBtn) &&
        library.isArrayLike(items)
      ) {
        if (library.isPositiveNumber(numItems)) {
          instructions = library.updateObject({}, instructions, {
            totalNumDisplay: numItems,
          });
        }

        if (library.isPositiveNumber(incrementNumItems)) {
          instructions = library.updateObject({}, instructions, {
            totalNumDisplay: instructions.totalNumDisplay + incrementNumItems,
          });
        }

        category = filter.getSelectedOption(selectedBtn);
        categoryCount = getCategoryCount(instructions.categoryData, category);
        viewMoreBtn = library.getFirstElement(
          instructionArgs.class.viewMoreBtn
        );

        categoryCount !== -1 && instructions.totalNumDisplay < categoryCount
          ? library.showElement(viewMoreBtn)
          : library.hideElement(viewMoreBtn);

        filter.displayItems({
          items,
          btns,
          selectedBtn,
          numItems: instructions.totalNumDisplay,
        });
      } else {
        throw new TypeError(
          library.errorMsg().argsEvaluationFail + ' to filter instructions.'
        );
      }
    },
    setFilter = () => {
      let btns = null,
        btn = null,
        viewMoreBtn = null,
        items = null,
        totalNumItems = 0;

      btns = filter.getBtns(
        library.getFirstElement(instructionArgs.class.btns)
      );
      items = filter.getItems(
        library.getFirstElement(instructionArgs.class.items)
      );

      if (library.isArrayLike(btns) && library.isArrayLike(items)) {
        totalNumItems = items.length;
        viewMoreBtn = library.getFirstElement(
          instructionArgs.class.viewMoreBtn
        );

        for (let i = 0; i < btns.length; i++) {
          btn = btns[i];

          if (library.isElementNode(btn) && document.addEventListener) {
            btn.addEventListener(
              'click',
              filterItems.bind(null, {
                items,
                btns,
                selectedBtn: btn,
                numItems: instructions.initialNumDisplay,
              }),
              false
            );
          }
        }

        if (totalNumItems) {
          instructions = library.updateObject({}, instructions, {
            categoryData: addCategoryData(
              instructions.categoryData,
              createNewCategory('all', totalNumItems)
            ),
          });

          instructions = library.updateObject({}, instructions, {
            categoryData: setCategoryData(instructions.categoryData, items),
          });

          filterItems({
            items,
            btns,
            selectedBtn: library.getSelectedBtn(btns, filter.isBtnSelected),
            numItems: instructions.initialNumDisplay,
          });

          if (library.isElementNode(viewMoreBtn) && document.addEventListener) {
            viewMoreBtn.addEventListener(
              'click',
              function() {
                filterItems({
                  items,
                  btns,
                  selectedBtn: library.getSelectedBtn(
                    btns,
                    filter.isBtnSelected
                  ),
                  incrementNumItems: instructions.numDisplayIncrement,
                });
              },
              false
            );
          }
        }
      }
    };
  return {
    setFilter,
  };
})(document, library, filter);
